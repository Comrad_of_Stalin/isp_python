"""task_tracker_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, url
    2. Add a URL to urlpatterns:  url('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url

from .djangoapp import views
from .djangoapp.url_handlers import (users,
                                     tasks,
                                     error_pages,
                                     tags,
                                     groups,
                                     reminds)
from django.conf import urls

urls.handler404 = error_pages.page_not_found_page


urlpatterns = [
    url(r'^task/add_from_request', views.add_task_from_request),
    url(r'^task/add', tasks.task_addition_page),
    url(r'^task/info/(?P<task_id>\d+)', tasks.task_info_page),
    url(r'^task/fail/(?P<task_id>\d+)', tasks.fail_task),
    url(r'^task/delete/(?P<task_id>\d+)', tasks.delete_task),
    url(r'^task/finish/(?P<task_id>\d+)', tasks.finish_task),
    url(r'^task/edit/(?P<task_id>\d+)', tasks.edit_task_page),
    url(r'^task/edit_task_action/(?P<task_id>\d+)', tasks.edit_task_action),
    url(r'^task/search', tasks.search_task_page),
    url(r'^task/filter', tasks.filter_tasks_async),

    url(r'^user/profile', users.user_profile_page),
    url(r'^user/authentication', users.authentication_page),
    url(r'^user/authenticate', users.authenticate_user),
    url(r'^user/registration', users.registration_page),
    url(r'^user/register', users.register_user),
    url(r'^user/logout', users.log_out_user),

    url(r'^tag/show', tags.users_tags_page),
    url(r'^tag/edit/(?P<tag_id>\d+)', tags.tag_edit_page),
    url(r'^tag/edit_tag_action/(?P<tag_id>\d+)', tags.edit_tag_action),
    url(r'^tag/add_tag_action', tags.add_tag_action),
    url(r'^tag/add', tags.tag_addition_page),
    url(r'^tag/delete/(?P<tag_id>\d+)', tags.delete_tag_action),

    url(r'^group/show/$', groups.users_groups_page),
    url(r'^group/add/$', groups.group_addition_page),
    url(r'^group/edit/(?P<group_id>\d+)', groups.edit_group_page),
    url(r'^group/add_group_action/(?P<user_id>\d+)', groups.add_group_action),
    url(r'^group/edit_group_action/(?P<group_id>\d+)', groups.edit_group_action),
    url(r'^group/delete_group_action/(?P<group_id>\d+)', groups.delete_group_action),

    url(r'^remind/add/$', reminds.remind_addition_page),
    url(r'^remind/add_remind_action', reminds.add_remind_action),
    url(r'^remind/search/$', reminds.remind_search_page),
    url(r'^remind/filter', reminds.filter_reminds_async),
    url(r'^remind/delete_remind_async', reminds.delete_remind_async),

    url(r'show_tasks/', views.show_tasks_page),
    url(r'show_groups/', views.show_groups_page),
    url(r'delete_all_tasks/', views.delete_all_tasks_page),
    url(r'delete_all_users/', views.delete_all_users_page), # /tag/edit_tag_action/

    url(r'^home', users.home_page),
    url('admin/', admin.site.urls),
]


handler404 = error_pages.page_not_found_page
