from django import forms


class TaskAdditionForm(forms.Form):
    task_name = forms.CharField(widget=forms.TextInput)
    parent_task_name = forms.CharField(widget=forms.TextInput)
    message = forms.CharField(widget=forms.TextInput)
    start_time = forms.DateTimeField(widget=forms.DateTimeInput)
    deadline = forms.DateTimeField(widget=forms.DateInput)
    tags = forms.Textarea()


if __name__ == '__main__':
    print(help(forms.Form))
