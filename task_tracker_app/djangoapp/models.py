import uuid
from datetime import datetime, timedelta
from django.db import models
from enumfields import EnumField, Enum
from django.utils import timezone

# Create your models here.


class TaskStatus(Enum):
    NOT_ACTIVE = "NOT_ACTIVE"
    ACTIVE = "ACTIVE"
    FINISHED = "FINISHED"
    FAILED = "FAILED"
    ARCHIVED = "ARCHIVED"


class UserModel(models.Model):
    name = models.TextField(default='DefName', unique=True)
    email = models.EmailField(null=True, unique=True)
    age = models.IntegerField(default=18)
    last_log_time = models.DateTimeField(default=timezone.localtime())
    user_id = models.TextField(default=uuid.uuid1())


class TagModel(models.Model):
    name = models.TextField(default='DefName')
    description = models.TextField(default='Default description')
    owner = models.ForeignKey(UserModel, on_delete=models.SET_NULL, null=True)


class TaskModel(models.Model):
    deadline = models.DateTimeField(default=datetime.now() + timedelta(days=1))
    start_time = models.DateTimeField(default=datetime.now())
    message = models.TextField(default='Default message for task.')
    alias = models.TextField(null=True)
    parent_task_id = models.TextField(null=True)
    root_task_id = models.TextField(default='self.id')
    percent_of_readiness = models.DecimalField(max_digits=11, decimal_places=8, default=0)
    admin_id = models.TextField(null=True)

    owner_id_list = models.ManyToManyField(UserModel, null=True, related_name='tasks')

    status = EnumField(TaskStatus, default=TaskStatus.NOT_ACTIVE)
    priority = models.IntegerField(default=1)
    total_complexity = models.IntegerField(default=0)
    tags = models.ManyToManyField(TagModel, blank=True, related_name='tasks', null=True)


class PlanModel(models.Model):

    apperaring_period_years = models.IntegerField(default=0)
    apperaring_period_month = models.IntegerField(default=0)
    apperaring_period_days = models.IntegerField(default=0)
    apperaring_period_hours = models.IntegerField(default=0)
    apperaring_period_minutes = models.IntegerField(default=0)
    apperaring_period_seconds = models.IntegerField(default=0)

    working_period_years = models.IntegerField(default=0)
    working_period_month = models.IntegerField(default=0)
    working_period_days = models.IntegerField(default=0)
    working_period_hours = models.IntegerField(default=0)
    working_period_minutes = models.IntegerField(default=0)
    working_period_seconds = models.IntegerField(default=0)

    initial_moment = models.DateTimeField(default=datetime.now() + timedelta(days=1))
    task_msg = models.TextField(default='')
    #ee = EnumField().


class GroupModel(models.Model):
    name = models.TextField(default='DefGroupName', unique=True)
    admin = models.ForeignKey(UserModel, on_delete=models.SET_NULL, null=True)
    members = models.ManyToManyField(UserModel, null=True, related_name='groups')
    tasks = models.ManyToManyField(TaskModel, null=True, related_name='groups')


class RemindModel(models.Model):
    remind_time = models.DateTimeField(default=timezone.now())
    message = models.TextField(default='DEFAULT REMIND MESSAGE')
    target_task = models.ForeignKey(TaskModel, on_delete=models.SET_NULL, null=True)
    target_task_alias = models.TextField(null=True)
    owner = models.ForeignKey(UserModel, on_delete=models.SET_NULL, null=True)
