import json

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from dateutil.relativedelta import relativedelta
from .models import *
from .manager import task_tools, tag_tools, filters, user_tools, remind_tools
from .. import settings


# Create your views here.


@login_required
def add_task_page(request):
    msg = 'Some message for task'
    deadline = datetime.now() + timedelta(hours=1)
    name = 'boris'
    task_tools.create_new_task_for_user(
        user_name=name,
        msg=msg,
        deadline=deadline,
        alias='another_task'
    )
    return HttpResponse('Added task with message {0} and deadline {1}'.format(msg, deadline))


def delete_all_tasks_page(request):
    task_tools.delete_all_tasks()
    return HttpResponse('<p>Deleted all tasks.</p>'
                        '<p>Now there is <b>{} tasks</b>.</p>'.format(len(TaskModel.objects.all())))


def delete_all_users_page(request):
    task_tools.delete_all_users()
    return HttpResponse('<p>Deleted all users.</p>'
                        '<p>Now there is <b>{} user</b>.</p>'.format(len(UserModel.objects.all())))


@login_required
def show_tasks_page(request):
    return HttpResponse(task_tools.show_tasks_for_user(user_name=request.user.username))

@login_required
def show_groups_page(request):
    return HttpResponse('<b>{}</b>'.format(GroupModel.objects.all().count()))



@login_required
def add_task_from_request(request):
    task_tools.create_new_task_for_user(
        user_name=request.user.username,
        msg=request.POST['msg'],
        deadline=datetime.strptime(request.POST['deadline'], '%Y-%m-%dT%H:%M'),
        start_time=datetime.strptime(request.POST['start_time'], '%Y-%m-%dT%H:%M'),
        priority=int(request.POST['priority']),
        alias=request.POST['alias'],
        parent_alias=request.POST['parent_alias']
    )
    tag_list = list(dict.fromkeys(request.POST['tags'].split()).keys())

    for tag_name in tag_list:
        tag_tools.add_tag_to_task(
            user_name=request.user.username,
            task_alias=request.POST['alias'],
            tag_name=tag_name
        )

    user = UserModel.objects.get(name=request.user.username)
    task = TaskModel.objects.get(alias=request.POST['alias'], admin_id=user.id)

    return HttpResponseRedirect(settings.TASK_INFO_URL + '/' + str(task.id))


def add_tag_example_page(request):
    tag_tools.add_tag_to_task(task_alias='example_task', tag_name='Example tag', user_name='boris')
    task = TaskModel.objects.get(alias='example_task')
    tag_list = list(map(lambda x: x.name, task.tags.all()))
    print('All tags: {}'.format(TagModel.objects.all()))
    return HttpResponse('Added {0} tag to task example_task. Tag list: {1}'.format('Example tag', tag_list))


@csrf_protect
def task_addition_page(request):
    data = {}
    return render(request, 'task_addition_page.ejs', data)


@csrf_protect
@login_required
def task_info_page(request, task_id):

    task = TaskModel.objects.get(id=task_id)

    assert user_tools.has_task(request.user.username, task)

    user = UserModel.objects.get(name=request.user.username)

    assert int(task.admin_id) == int(user.id), 'User {} doesn\'t have such task.'.format(user.name)

    task_tools.recalculate_readiness(task=task)

    tags = [tag.name for tag in task.tags.all()]
    child_list = TaskModel.objects.filter(parent_task_id=task.id)
    for i in range(len(child_list)):
        child_list[i].tag_list = [tag.name for tag in child_list[i].tags.all()]
    data = {
        'task': task,
        'child_list': child_list,
        'tags': tags
    }
    return render(request, 'task_info_page.ejs', data)


@csrf_protect
def authenticate_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect(settings.HOME_PAGE_URL)
        return HttpResponse('Account was disabled.')
    return HttpResponse('Invalid user name or email has been passed.')


@csrf_protect
def register_user(request):
    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']

    if User.objects.filter(username=username).count() != 0:
        return HttpResponse('User with such login already exists.')

    if User.objects.filter(email=email).count() != 0:
        return HttpResponse('User with such email already exists.')

    user = User.objects.create_user(username, email, password)
    UserModel.objects.create(name=username, email=email)
    user.save()

    login(request, user)

    return redirect(settings.HOME_PAGE_URL)


@csrf_protect
def authentication_page(request):
    data = {}
    return render(request, 'authorization_page.ejs', data)


@csrf_protect
def registration_page(request):
    data = {}
    return render(request, 'registration_page.ejs', data)


@csrf_protect
@login_required
def fail_task(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    task_tools.fail_task(task)
    return redirect(settings.TASK_INFO_URL + '/'+str(task_id))


@csrf_protect
@login_required
def finish_task(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    task_tools.finish_task(task)
    return redirect(settings.TASK_INFO_URL + '/'+str(task_id))


@csrf_protect
@login_required
def delete_task(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    parent_task_id = task.parent_task_id
    task_tools.delete_task(task)
    if parent_task_id is not None:
        return redirect(settings.TASK_INFO_URL+'/'+str(parent_task_id))
    return redirect(settings.TASK_INFO_URL)


@csrf_protect
def home_page(request):
    data = {}
    return render(request, 'home_page.ejs', data)


@csrf_protect
@login_required
def search_task_page(request):
    data = {}
    return render(request, 'search_task_page.ejs', data)


@csrf_exempt
@login_required
def test_ajax(request):
    data = dict(request.POST.items())
    data['min_start_time'] = datetime.strptime(data['min_start_time'], '%Y-%m-%dT%H:%M')
    data['max_start_time'] = datetime.strptime(data['max_start_time'], '%Y-%m-%dT%H:%M')
    data['min_deadline'] = datetime.strptime(data['min_deadline'], '%Y-%m-%dT%H:%M')
    data['max_deadline'] = datetime.strptime(data['max_deadline'], '%Y-%m-%dT%H:%M')
    if 'status_range[]' in data:
        del data['status_range[]']

    task_objects = filters.filter_tasks(
        user_id=UserModel.objects.get(name=request.user.username).id,
        **data
    )

    return HttpResponse(json.dumps(list(({
        'id': str(task.id),
        'alias': task.alias,
        'deadline': task.deadline.strftime('%Y-%m-%dT%H:%M'),
        'status': task.status.value
    } for task in task_objects))), content_type="application/json")


@csrf_protect
@login_required
def edit_task_action(request, task_id):
    user = UserModel.objects.get(name=request.user.username)
    task_tools.edit_task(
        user=user,
        task_id=task_id,
        new_parent_task_alias=request.POST['parent_alias'],
        new_msg=request.POST['msg'],
        new_deadline=datetime.strptime(request.POST['deadline'], '%Y-%m-%dT%H:%M'),
        new_start_time=datetime.strptime(request.POST['start_time'], '%Y-%m-%dT%H:%M'),
        new_priority=int(request.POST['priority'])
    )
    task = TaskModel.objects.get(id=task_id)
    task.tags.all().delete()

    tag_list = list(dict.fromkeys(request.POST['tags'].split()).keys())

    for tag_name in tag_list:
        tag_tools.add_tag_to_task(
            user_name=request.user.username,
            task_alias=request.POST['alias'],
            tag_name=tag_name
        )

    return redirect('/task_info/'+task_id)


@csrf_exempt
@login_required
def edit_task_page(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    data = {
        'id': task.id,
        'msg': task.message,
        'priority': task.priority,
        'alias': task.alias,
        'start_time': task.start_time.strftime('%Y-%m-%dT%H:%M'),
        'deadline': task.deadline.strftime('%Y-%m-%dT%H:%M'),
        'tags': [tag.name for tag in task.tags.all()]
    }

    return render(request, 'task_edit_page.ejs', data)


@csrf_protect
@login_required
def user_profile_page(request):
    user = UserModel.objects.get(name=request.user.username)
    data = {
        'user': user
    }
    return render(request, 'profile_page.ejs', data)
