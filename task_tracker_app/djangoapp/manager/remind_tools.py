from ..models import *
from django.utils import timezone
from dateutil.relativedelta import relativedelta


def add_remind_to_task(task, message='Default message', remind_time=None, user=None):
    if user is None:
        user = UserModel.objects.get(id=task.admin_id)

    if remind_time is None:
        remind_time = timezone.now()

    remind = RemindModel.objects.create(
        remind_time=remind_time,
        message=message,
        target_task=task,
        target_task_alias=task.alias,
        owner=user
    )


def get_n_closest_reminds_for_task(task, n=None):

    if n is None:
        return RemindModel.objects.filter(target_task=task)

    return RemindModel.objects.filter(target_task=task).order_by('-remind_time')[:n]


def get_n_closest_reminds_for_user(user, n=None):

    if n is None:
        return RemindModel.objects.filter(owner=user)

    return RemindModel.objects.filter(owner=user).order_by('-remind_time')[:n]


def drop_passed_reminds_for_user(user):
    RemindModel.objects.filter(
        remind_time__lte=timezone.localtime()).delete()


def get_reminds_future_range(range_period=relativedelta(hours=1)):
    return RemindModel.objects.filter(
        remind_time__lte=timezone.localtime()+range_period,
        remind_time__gte=timezone.localtime()
    )
