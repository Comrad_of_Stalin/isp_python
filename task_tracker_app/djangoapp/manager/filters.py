from datetime import datetime
from ..models import TaskModel, TaskStatus, RemindModel
from django.utils import timezone


MIN_DATETIME = timezone.now().min
MAX_DATETIME = timezone.now().max

MIN_INT = -1e9-7
MAX_INT = 1e9+7


def filter_tasks(
        min_start_time=MIN_DATETIME,
        max_start_time=MAX_DATETIME,
        min_deadline=MIN_DATETIME,
        max_deadline=MAX_DATETIME,
        min_priority=MIN_INT,
        max_priority=MAX_INT,
        min_percent_of_readiness=MIN_INT,
        max_percent_of_readiness=MAX_INT,
        alias_prefix='',
        user_id=None,
        status_range=(
            TaskStatus.FAILED,
            TaskStatus.NOT_ACTIVE,
            TaskStatus.FINISHED,
            TaskStatus.ARCHIVED,
            TaskStatus.ACTIVE
        )):
    if not isinstance(status_range, list):
        status_range = list(status_range)

    min_deadline = min_deadline.replace(tzinfo=timezone.get_current_timezone())
    max_deadline = max_deadline.replace(tzinfo=timezone.get_current_timezone())
    min_start_time = min_start_time.replace(tzinfo=timezone.get_current_timezone())
    max_start_time = max_start_time.replace(tzinfo=timezone.get_current_timezone())
    for status in status_range:
        for task in TaskModel.objects.filter(
                start_time__range=(min_start_time, max_start_time),
                deadline__range=(min_deadline, max_deadline),
                priority__range=(min_priority, max_priority),
                percent_of_readiness__range=(min_percent_of_readiness, max_percent_of_readiness),
                alias__startswith=alias_prefix,
                status=status,
                admin_id=user_id):
            yield task


def filter_reminds(
        min_remind_time=MIN_DATETIME,
        max_remind_time=MAX_DATETIME,
        alias_prefix='',
        message_prefix='',
        user=None):
    min_remind_time = min_remind_time.replace(tzinfo=timezone.get_current_timezone())
    max_remind_time = max_remind_time.replace(tzinfo=timezone.get_current_timezone())
    print(RemindModel.objects.all())
    for remind in RemindModel.objects.filter(
        remind_time__range=(min_remind_time, max_remind_time),
        target_task_alias__startswith=alias_prefix,
        message__startswith=message_prefix,
        owner=user):
        yield remind
