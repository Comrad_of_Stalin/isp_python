from ..models import (
    TaskModel,
    TagModel,
    UserModel
)


def add_tag_to_task(user_name, task_id=None, task_alias=None, tag_name=None, description=''):
    user = UserModel.objects.get(name=user_name)
    if task_id is None:
        task = TaskModel.objects.get(alias=task_alias, admin_id=user.id)
    else:
        task = TaskModel.objects.get(id=task_id)
    if task.tags.filter(name=tag_name, owner=user).count() != 0:
        return
    if TagModel.objects.filter(name=tag_name, owner=user).count() != 0:
        tag = TagModel.objects.get(name=tag_name, owner=user)
    else:
        tag = TagModel.objects.create(name=tag_name, description=description, owner=user)
    task.tags.add(tag)
    task.save()


def remove_tag_from_task(user_name, task_alias, tag_name):
    user = UserModel.objects.get(name=user_name)
    task = TaskModel.objects.get(alias=task_alias, admin_d=user.id)
    if TagModel.objects.filter(name=tag_name, owner=user).count() != 0:
        tag = TagModel.objects.get(name=tag_name, owner=user)
        task.tags.remove(tag)
    task.save()


def get_tasks_with_tag(user_name, tag_name):
    user = UserModel.objects.get(name=user_name)
    tag = TagModel.objects.get(name=tag_name, owner=user)
    tasks = TaskModel.objects.filter(tags=tag)
    return list(tasks)


def add_tag_to_task_list(tag, task_aliases, user):
    for task_alias in task_aliases:
        if TaskModel.objects.filter(alias=task_alias, admin_id=user.id).count != 0:
            add_tag_to_task(user_name=user.name, task_alias=task_alias, tag_name=tag.name)
    tag.save()
