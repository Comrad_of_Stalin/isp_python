from ..models import *
from django.utils import timezone


def add_user(name, email):
    u = UserModel()
    u.name = name
    u.email = email
    u.age = 19
    u.last_log_time = datetime.now()
    u.save()
    return u.id


def add_task_to_user_by_name(user_name, task):
    user = UserModel.objects.get(name=user_name)
    task.owner_id_list.add(user)
    task.save()


def create_new_task_for_user(
        user_name,
        alias,
        parent_alias=None,
        msg='Default msg',
        start_time=datetime.now(),
        deadline=datetime.now()+timedelta(days=1),
        priority=1
):
    user = UserModel.objects.get(name=user_name)

    t = TaskModel()
    t.admin_id = user.id
    t.deadline = deadline
    t.message = msg
    t.alias = alias
    t.start_time = start_time
    t.priority = priority

    if parent_alias:
        parent_task = TaskModel.objects.get(alias=parent_alias, admin_id=user.id)
        t.parent_task_id = parent_task.id
        t.root_task_id = parent_task.root_task_id

    if start_time < datetime.now():
        t.status = TaskStatus.ACTIVE

    t.save()
    add_task_to_user_by_name(user_name, t)


def show_tasks_for_user(user_name):
    user = UserModel.objects.get(name=user_name)
    task_list = list(TaskModel.objects.filter(admin_id=user.id))
    ret_val = "".join([
        '<p>'
        '\nTask deadline: {0}'
        '\nTask message: {1}'
        '\nTask init time: {2}'
        '\nTask id: {3}'
        '\n</p>'.format(
            task.deadline,
            task.message,
            task.start_time,
            task.id
        ) for task in task_list
    ])
    return ret_val


def delete_all_tasks_for_user(user_name):
    user = UserModel.objects.get(name=user_name)
    TaskModel.objects.filter(admin_id=user.id).delete()


def delete_all_users():
    UserModel.objects.all().delete()


def delete_all_tasks():
    TaskModel.objects.all().delete()


def dfs(task, func, condition):
    children = TaskModel.objects.filter(parent_task_id=task.id)
    print('Task alias: {}'.format(task.alias))
    for child in children:
        if condition(child):
            dfs(child, func, condition)
    func(task)


def recalculate_readiness(task):
    if task.status not in [TaskStatus.ACTIVE, TaskStatus.NOT_ACTIVE]:
        return

    def func(task_obj):
        children = TaskModel.objects.filter(parent_task_id=task_obj.id)
        complexities = (float(child.priority) for child in children)
        percents_of_readiness = (float(child.percent_of_readiness) for child in children)
        total_complexity = task_obj.priority
        points_of_readiness = 0
        for x, y in zip(complexities, percents_of_readiness):
            total_complexity += x
            points_of_readiness += x*y
        task_obj.percent_of_readiness = points_of_readiness/total_complexity

        # check failing
        if TaskModel.objects.filter(parent_task_id=task_obj.id, status=TaskStatus.FAILED).count() != 0:
            task_obj.status = TaskStatus.FAILED

        task_obj.save()

    def condition(task_obj):
        return task_obj.status == TaskStatus.ACTIVE or task_obj.status == TaskStatus.NOT_ACTIVE

    dfs(task, func, condition)


def finish_task(task):
    def func(task_obj):
        if task_obj.status in [TaskStatus.FAILED, TaskStatus.FINISHED, TaskStatus.ARCHIVED]:
            return
        task_obj.percent_of_readiness = 100.0
        task_obj.status = TaskStatus.FINISHED
        task_obj.save()

    dfs(task, func, lambda t: True)


def add_task_to_group(task, group):
    def func(task_obj):
        if task_obj.groups.filter(name=group.name).count() != 0:
            return
        task_obj.groups.add(group)
        task_obj.save()

    dfs(task, func, lambda t: True)


def remove_task_from_group(task, group):
    def func(task_obj):
        if task_obj.groups.filter(name=group.name).count() == 0:
            return
        task_obj.groups.remove(group)
        task_obj.save()

    dfs(task, func, lambda t: True)


def fail_task(task):
    def func(task_obj):
        if task_obj.status in [TaskStatus.FAILED, TaskStatus.FINISHED, TaskStatus.ARCHIVED]:
            return
        task_obj.status = TaskStatus.FAILED
        task_obj.save()

    def condition(task_obj):
        return task_obj.status == TaskStatus.ACTIVE or task_obj.status == TaskStatus.NOT_ACTIVE

    dfs(task, func, condition)


def delete_task(task):
    def func(task_obj):
        task_obj.delete()

    dfs(task, func, lambda t: True)


def edit_task(
        user,
        task_id,
        new_msg=None,
        new_start_time=None,
        new_deadline=None,
        new_priority=None,
        new_parent_task_alias=None):

    task = TaskModel.objects.get(id=task_id)
    if new_msg is not None:
        task.message = new_msg
    if new_deadline is not None:
        task.deadline = new_deadline
    if new_start_time is not None:
        task.start_time = new_start_time
    if new_priority is not None:
        task.priority = new_priority

    assert isinstance(task.message, str), 'Message must have type str.'
    assert isinstance(task.start_time, datetime), 'Start time must have type datetime.'
    assert isinstance(task.deadline, datetime), 'Deadline time must have type datetime.'
    assert isinstance(task.priority, int), 'Priority must have type int.'
    assert task.deadline > datetime.now(), 'Can not set deadline to the past.'
    assert task.start_time < task.deadline, 'Can not set deadline before start time of task.'

    if new_parent_task_alias:
        parent_task = TaskModel.objects.get(alias=new_parent_task_alias, admin_id=user.id)
        task.parent_task_id = parent_task.id

    task.save()


def try_to_activate_task(task):
    if task.status in [TaskStatus.FAILED, TaskStatus.FINISHED, TaskStatus.ARCHIVED]:
        return
    if task.status == TaskStatus.NOT_ACTIVE:
        if task.start_time < timezone.now():
            task.status = TaskStatus.ACTIVE
            task.save()
