from ..manager import task_tools, user_tools
from ..models import *


def add_group(groupname, admin, user_names_list, task_aliases_list=()):

    if not isinstance(admin, UserModel):
        raise ValueError('Admin argument must have type UserModel')
    if GroupModel.objects.filter(name=groupname).count() != 0:
        raise ValueError('Group with such name already exists')

    if any(UserModel.objects.filter(name=username).count() == 0 for username in user_names_list):
        raise ValueError('Can not add unexisting user to group.')
    if any((
            TaskModel.objects.filter(
                alias=alias,
                admin_id=admin.id
            ).count() == 0
            for alias in task_aliases_list)):
        raise ValueError('Can not unexisting task to group.')

    user_list = [UserModel.objects.get(name=username) for username in user_names_list]
    task_list = [TaskModel.objects.get(alias=alias) for alias in task_aliases_list]

    group = GroupModel.objects.create(name=groupname, admin=admin)

    group.save()

    group.members.add(*user_list)

    for task in task_list:
        task_tools.add_task_to_group(task, group)

    group.save()


def try_to_add_task_to_group(task, group, user_sender):
    if task.groups.filter(name=group.name).count() == 0 \
            and group.members.filter(name=user_sender.name).count() != 0 \
            and group.tasks.filter(alias=task.alias).count() == 0 \
            and int(task.admin_id) == int(user_sender.id):
        task_tools.add_task_to_group(task=task, group=group)
        return True
    return False


def try_to_remove_task_from_group(task, group, user_sender):
    if task.groups.filter(name=group.name).count() != 0 \
            and group.members.filter(name=user_sender.name).count() != 0 \
            and (int(task.admin_id) == int(user_sender.id) or group.admin.name == user_sender.name):
        task_tools.remove_task_from_group(task=task, group=group)
        return True
    return False


def try_to_add_user_to_group(new_user, group, user_admin):
    if new_user.groups.filter(name=group.name).count() == 0 \
            and group.admin.name == user_admin.name:
        user_tools.add_user_to_group(user=new_user, group=group)
        return True
    return False


def try_to_remove_user_from_group(user_to_remove, group, user_admin):
    if user_to_remove.groups.filter(name=group.name).count() != 0 \
            and group.admin.name == user_admin.name:
        user_tools.remove_user_from_group(user=user_to_remove, group=group)
        return True
    return False
