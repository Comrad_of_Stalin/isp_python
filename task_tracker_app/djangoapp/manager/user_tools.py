from ..models import UserModel


def has_task(username, task):
    user = UserModel.objects.get(name=username)

    user_groups = user.groups.all()
    task_groups = task.groups.all()

    common_groups = user_groups.intersection(task_groups)

    return int(task.admin_id) == int(user.id) or common_groups.count() != 0


def add_user_to_group(user, group):
    if user.groups.filter(name=group.name).count() == 0:
        user.groups.add(group)


def remove_user_from_group(user, group):
    if user.groups.filter(name=group.name).count() != 0:
        user.groups.remove(group)


def get_user_from_request(request):
    return UserModel.objects.get(name=request.user.username)
