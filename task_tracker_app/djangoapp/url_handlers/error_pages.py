from django.shortcuts import render


def page_not_found_page(request):
    data = {}
    print('ERROR PAGE CALLED!')
    return render(request, '404page.ejs', data)
