import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from ..manager import task_tools, user_tools, tag_tools, filters, remind_tools
from ..models import *


def delete_all_tasks_page(request):
    task_tools.delete_all_tasks()
    return HttpResponse('<p>Deleted all tasks.</p>'
                        '<p>Now there is <b>{} tasks</b>.</p>'.format(len(TaskModel.objects.all())))


def delete_all_users_page(request):
    task_tools.delete_all_users()
    return HttpResponse('<p>Deleted all users.</p>'
                        '<p>Now there is <b>{} user</b>.</p>'.format(len(UserModel.objects.all())))


@login_required
def show_tasks_page(request):
    return HttpResponse(task_tools.show_tasks_for_user(user_name=request.user.username))


@csrf_protect
@login_required
def task_addition_page(request):
    data = {}
    return render(request, 'task_addition_page.ejs', data)


@csrf_protect
@login_required
def task_info_page(request, task_id):

    task = TaskModel.objects.get(id=task_id)

    assert user_tools.has_task(request.user.username, task)

    user = user_tools.get_user_from_request(request)

    assert user_tools.has_task(request.user.username, task), \
        'User {} doesn\'t have such task.'.format(user.name)

    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()

    task_tools.recalculate_readiness(task=task)
    task_tools.try_to_activate_task(task=task)

    tags = [tag.name for tag in task.tags.all()]
    child_list = TaskModel.objects.filter(parent_task_id=task.id)
    for i in range(len(child_list)):
        child_list[i].tag_list = [tag.name for tag in child_list[i].tags.all()]
    data = {
        'task': task,
        'child_list': child_list,
        'tags': tags,
        'reminds': reminds
    }
    return render(request, 'task_info_page.ejs', data)


@csrf_protect
@login_required
def search_task_page(request):
    data = {}
    return render(request, 'search_task_page.ejs', data)


@csrf_exempt
@login_required
def edit_task_page(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    user = user_tools.get_user_from_request(request)

    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()

    data = {
        'id': task.id,
        'msg': task.message,
        'priority': task.priority,
        'alias': task.alias,
        'start_time': task.start_time.strftime(settings.DATE_TIME_FORMAT),
        'deadline': task.deadline.strftime(settings.DATE_TIME_FORMAT),
        'tags': [tag.name for tag in task.tags.all()],
        'reminds': reminds
    }

    return render(request, 'task_edit_page.ejs', data)


# Handlers of the support urls


@csrf_exempt
@login_required
def filter_tasks_async(request):
    data = dict(request.POST.items())
    data['min_start_time'] = datetime.strptime(data['min_start_time'], settings.DATE_TIME_FORMAT)
    data['max_start_time'] = datetime.strptime(data['max_start_time'], settings.DATE_TIME_FORMAT)
    data['min_deadline'] = datetime.strptime(data['min_deadline'], settings.DATE_TIME_FORMAT)
    data['max_deadline'] = datetime.strptime(data['max_deadline'], settings.DATE_TIME_FORMAT)

    status_range = None
    if 'status_range[]' in data:
        status_range = request.POST.getlist('status_range[]')
        del data['status_range[]']
    if status_range is not None:
        task_objects = filters.filter_tasks(
            user_id=user_tools.get_user_from_request(request).id,
            status_range=status_range,
            **data
        )
    else:
        task_objects = filters.filter_tasks(
            user_id=user_tools.get_user_from_request(request).id,
            **data
        )

    return HttpResponse(json.dumps(list(({
        'id': str(task.id),
        'alias': task.alias,
        'deadline': task.deadline.strftime('%Y-%m-%dT%H:%M'),
        'status': task.status.value
    } for task in task_objects))), content_type="application/json")


@login_required
def add_task_from_request(request):

    user = user_tools.get_user_from_request(request)

    if TaskModel.objects.filter(alias=request.POST['alias'], admin_id=user.id).count() != 0:
        raise ValueError('Task with such alias already exists')

    assert request.POST['priority'].isdigit()\
           and 1 <= int(request.POST['priority']) <= 10, 'Invalid priority value'

    task_tools.create_new_task_for_user(
        user_name=request.user.username,
        msg=request.POST['msg'],
        deadline=datetime.strptime(request.POST['deadline'], settings.DATE_TIME_FORMAT),
        start_time=datetime.strptime(request.POST['start_time'], settings.DATE_TIME_FORMAT),
        priority=int(request.POST['priority']),
        alias=request.POST['alias'],
        parent_alias=request.POST['parent_alias']
    )
    tag_list = list(dict.fromkeys(request.POST['tags'].split()).keys())

    for tag_name in tag_list:
        tag_tools.add_tag_to_task(
            user_name=request.user.username,
            task_alias=request.POST['alias'],
            tag_name=tag_name
        )

    user = user_tools.get_user_from_request(request)
    task = TaskModel.objects.get(alias=request.POST['alias'], admin_id=user.id)

    return HttpResponseRedirect(settings.TASK_INFO_URL + '/' + str(task.id))


@csrf_protect
@login_required
def fail_task(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    task_tools.fail_task(task)
    return redirect(settings.TASK_INFO_URL + '/'+str(task_id))


@csrf_protect
@login_required
def finish_task(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    task_tools.finish_task(task)
    return redirect(settings.TASK_INFO_URL + '/'+str(task_id))


@csrf_protect
@login_required
def delete_task(request, task_id):
    task = TaskModel.objects.get(id=task_id)
    assert user_tools.has_task(request.user.username, task)

    parent_task_id = task.parent_task_id
    task_tools.delete_task(task)
    if parent_task_id is not None:
        return redirect(settings.TASK_INFO_URL+'/'+str(parent_task_id))
    return redirect(settings.HOME_PAGE_URL)


@csrf_protect
@login_required
def edit_task_action(request, task_id):
    user = user_tools.get_user_from_request(request)
    task_tools.edit_task(
        user=user,
        task_id=task_id,
        new_parent_task_alias=request.POST['parent_alias'],
        new_msg=request.POST['msg'],
        new_deadline=datetime.strptime(request.POST['deadline'], settings.DATE_TIME_FORMAT),
        new_start_time=datetime.strptime(request.POST['start_time'], settings.DATE_TIME_FORMAT),
        new_priority=int(request.POST['priority'])
    )
    task = TaskModel.objects.get(id=task_id)
    task.tags.all().delete()

    tag_list = list(dict.fromkeys(request.POST['tags'].split()).keys())

    for tag_name in tag_list:
        tag_tools.add_tag_to_task(
            user_name=request.user.username,
            task_id=task_id,
            tag_name=tag_name
        )

    return redirect(settings.TASK_INFO_URL+'/'+task_id)
