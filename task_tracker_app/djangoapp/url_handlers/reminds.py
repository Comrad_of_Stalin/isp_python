import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from ..manager import task_tools, user_tools, tag_tools, filters, remind_tools
from ..models import *


@csrf_protect
@login_required
def remind_addition_page(request, task_id=None):
    task = None

    if task_id is not None:
        task = TaskModel.objects.get(id=task_id)

    data = {
        'task': task,
    }
    return render(request, 'remind_addition_page.ejs', data)


@csrf_protect
@login_required
def remind_search_page(request):
    data = {}
    return render(request, 'remind_search_page.ejs', data)


# Handlers of the support urls


@csrf_protect
@login_required
def add_remind_action(request):
    user = user_tools.get_user_from_request(request)
    print(list(request.POST.items()))

    remind_time = datetime.strptime(request.POST.get('remind_time_0'), settings.DATE_TIME_FORMAT)
    message = request.POST.get('message_0', '')
    task = TaskModel.objects.get(alias=request.POST.get('alias_0'), admin_id=user.id)

    remind_tools.add_remind_to_task(
        task=task,
        message=message,
        remind_time=remind_time,
        user=user
    )

    return redirect('/group/show')


@csrf_exempt
@login_required
def filter_reminds_async(request):
    data = dict(request.POST.items())
    data['min_remind_time'] = datetime.strptime(data['min_remind_time'], settings.DATE_TIME_FORMAT)
    data['max_remind_time'] = datetime.strptime(data['max_remind_time'], settings.DATE_TIME_FORMAT)

    remind_objects = filters.filter_reminds(
        user=user_tools.get_user_from_request(request),
        **data
    )

    return HttpResponse(json.dumps(list(({
        'id': str(remind.id),
        'task_id': remind.target_task.id,
        'task_alias': remind.target_task.alias,
        'remind_time': remind.remind_time.strftime(settings.DATE_TIME_FORMAT),
        'message': remind.message,
        'task_deadline': remind.target_task.deadline.strftime(settings.DATE_TIME_FORMAT)
    } for remind in remind_objects))), content_type="application/json")


@csrf_exempt
@login_required
def delete_remind_async(request):
    remind_id = request.POST.get('remind_id', '')
    print(list(request.POST.items()))
    try:
        RemindModel.objects.get(id=remind_id).delete()
        return HttpResponse(json.dumps({'status': 'success'}), content_type='application/json')
    except Exception as ex_obj:
        print(ex_obj)
        return HttpResponse(json.dumps({'status': 'error', 'msg': ex_obj.__str__()}), content_type='application/json')
