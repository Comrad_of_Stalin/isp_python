from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from ..manager import tag_tools, remind_tools
from ..manager import user_tools
from ..models import *


@csrf_protect
@login_required
def users_tags_page(request):

    user = user_tools.get_user_from_request(request)

    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()

    tag_objects = TagModel.objects.filter(owner=user)

    tags = [
        {
            'name': tag.name,
            'id': tag.id,
            'task_amount': TaskModel.objects.filter(tags=tag).count(),
            'description': tag.description
        }
        for tag in tag_objects]

    data = {
        'tags': tags,
        'reminds': reminds
    }

    print(tags)

    return render(request, 'users_tags_page.ejs', data)


@csrf_protect
@login_required
def tag_edit_page(request, tag_id):

    tag = TagModel.objects.get(id=tag_id)

    task_objects = TaskModel.objects.filter(tags=tag)
    task_names = [task.alias for task in task_objects]

    user = user_tools.get_user_from_request(request)
    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()

    data = {
        'tag': tag,
        'tasks': task_names,
        'task_amount': len(task_names),
        'reminds': reminds
    }

    return render(request, 'tag_edit_page.ejs', data)


@csrf_protect
@login_required
def tag_addition_page(request):
    user = user_tools.get_user_from_request(request)
    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()
    data = {
        'reminds': reminds
    }
    return render(request, 'tag_addition_page.ejs', data)


# Handlers of the support urls


@csrf_protect
@login_required
def edit_tag_action(request, tag_id):

    user = user_tools.get_user_from_request(request)

    tag = TagModel.objects.get(id=tag_id)

    tag.description = request.POST['description']

    tag.tasks.clear()

    task_aliases = list(dict.fromkeys(request.POST.get('tasks', '').split()).keys())
    tag_tools.add_tag_to_task_list(
        tag=tag,
        task_aliases=task_aliases,
        user=user
    )

    tag.save()

    return redirect('/tag/show')


@csrf_protect
@login_required
def add_tag_action(request):

    user = user_tools.get_user_from_request(request)

    tagname = request.POST['name']
    description = request.POST['description']
    task_aliases = list(dict.fromkeys(request.POST.get('tasks', '').split()).keys())

    if TagModel.objects.filter(name=tagname, owner=user).count() != 0:
        pass   # message here

    if any((TaskModel.objects.filter(
            alias=task_alias,
            admin_id=user.id).count() == 0 for task_alias in task_aliases)):
        pass    # message here

    tag = TagModel.objects.create(name=tagname, description=description)

    tag_tools.add_tag_to_task_list(
        tag=tag,
        task_aliases=task_aliases,
        user=user
    )

    tag.save()

    return redirect('/tag/show')


@csrf_protect
@login_required
def delete_tag_action(request, tag_id):

    user = user_tools.get_user_from_request(request)

    try:
        tag = TagModel.objects.get(id=tag_id, owner=user)
        tag.delete()
    except:
        pass   # message here

    return redirect('/tag/show')

