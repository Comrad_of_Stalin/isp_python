from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from ..manager import group_tools, remind_tools
from ..models import *


@csrf_protect
@login_required
def group_addition_page(request):

    user = UserModel.objects.get(name=request.user.username)

    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()

    data = {
        'user': user,
        'reminds': reminds
    }

    return render(request, 'group_addition_page.ejs', data)


@csrf_protect
@login_required
def users_groups_page(request):

    user = UserModel.objects.get(name=request.user.username)

    group_objects = GroupModel.objects.filter(members=user)

    groups = []

    for group in group_objects:
        groups.append({
            'id': group.id,
            'name': group.name,
            'task_amount': group.tasks.all().count(),
            'user_amount': group.members.all().count(),
            'admin': group.admin.name
        })

    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()

    data = {
        'groups': groups,
        'reminds': reminds
    }

    return render(request, 'users_groups_page.ejs', data)


@csrf_protect
@login_required
def edit_group_page(request, group_id):

    user = UserModel.objects.get(name=request.user.username)
    group = GroupModel.objects.get(id=group_id)

    members = [member for member in group.members.all()]
    tasks = [task for task in group.tasks.all()]

    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range()

    data = {
        'user': user,
        'group': group,
        'members': members,
        'tasks': tasks,
        'is_admin': group.admin.name == user.name,
        'reminds': reminds
    }

    return render(request, 'group_edit_page.ejs', data)


# Handlers of the support urls


@csrf_protect
@login_required
def add_group_action(request, user_id):

    user = UserModel.objects.get(id=user_id)

    member_names = list(dict.fromkeys(request.POST.get('members', '').split()).keys())
    task_aliases = list(dict.fromkeys(request.POST.get('tasks', '').split()).keys())

    group_tools.add_group(
        groupname=request.POST['name'],
        admin=user,
        user_names_list=member_names,
        task_aliases_list=task_aliases
    )

    return redirect('/group/show')


@csrf_protect
@login_required
def edit_group_action(request, group_id):

    user = UserModel.objects.get(name=request.user.username)
    group = GroupModel.objects.get(id=group_id)

    is_admin = user.name == group.admin.name

    tasks_to_add_names = list(dict.fromkeys(request.POST['added_tasks'].split()).keys())
    tasks_to_remove_names = list(dict.fromkeys(request.POST['removed_tasks'].split()).keys())

    if any((TaskModel.objects.filter(
            alias=task_alias,
            admin_id=user.id).count() == 0 for task_alias in tasks_to_add_names)):
        pass # message

    if any((TaskModel.objects.filter(
            alias=task_alias,
            groups=group).count() == 0 for task_alias in tasks_to_remove_names)):
        pass # message

    tasks_to_add = [TaskModel.objects.get(
        alias=task_alias,
        admin_id=user.id) for task_alias in tasks_to_add_names]
    tasks_to_remove = [TaskModel.objects.get(
        alias=task_alias,
        groups=group) for task_alias in tasks_to_remove_names]

    for task in tasks_to_add:
        group_tools.try_to_add_task_to_group(task, group, user)

    for task in tasks_to_remove:
        group_tools.try_to_remove_task_from_group(task, group, user)

    if is_admin:
        users_to_add_names = list(dict.fromkeys(request.POST.get('added_users', '').split()).keys())
        users_to_remove_names = list(dict.fromkeys(request.POST['removed_users'].split()).keys())

        if any((UserModel.objects.filter(name=username).count() == 0 for username in users_to_add_names)):
            pass # message

        if any((UserModel.objects.filter(name=username).count() == 0 for username in users_to_remove_names)):
            pass # message

        users_to_add = [UserModel.objects.get(name=username) for username in users_to_add_names]
        users_to_remove = [UserModel.objects.get(name=username) for username in users_to_remove_names]

        for user_obj in users_to_add:
            group_tools.try_to_add_user_to_group(
                new_user=user_obj,
                group=group,
                user_admin=user
            )

        for user_obj in users_to_remove:
            group_tools.try_to_remove_user_from_group(
                user_to_remove=user_obj,
                group=group,
                user_admin=user
            )

    group.save()

    return redirect('/group/show')


@csrf_protect
@login_required
def delete_group_action(request, group_id):
    group = GroupModel.objects.get(id=group_id)

    group.delete()

    return redirect('/group/show')
