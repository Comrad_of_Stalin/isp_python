import math
from datetime import datetime

from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils import timezone
from django.utils.timezone import activate
from django.views.decorators.csrf import csrf_protect
from dateutil.relativedelta import relativedelta
from ..manager import user_tools, remind_tools
from ..models import (UserModel,
                      TagModel,
                      TaskModel,
                      TaskStatus,
                      GroupModel)


# Create your views here.


@csrf_protect
def authentication_page(request):
    data = {}
    return render(request, 'authorization_page.ejs', data)


@csrf_protect
def registration_page(request):
    data = {}
    return render(request, 'registration_page.ejs', data)


@csrf_protect
def home_page(request):
    user = user_tools.get_user_from_request(request)
    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range(relativedelta(hours=1))
    data = {
        'reminds': reminds
    }
    return render(request, 'home_page.ejs', data)


@csrf_protect
@login_required
def user_profile_page(request):

    user = user_tools.get_user_from_request(request)
    remind_tools.drop_passed_reminds_for_user(user)
    reminds = remind_tools.get_reminds_future_range(relativedelta(hours=1))

    tags = []

    if TagModel.objects.filter(owner=user).count() != 0:
        tag_objects = TagModel.objects.filter(owner=user)
        tags = [{'name': tag.name, 'id': tag.id} for tag in tag_objects]

    tasks_total = TaskModel.objects.filter(admin_id=user.id).count()
    tasks_active = TaskModel.objects.filter(admin_id=user.id, status=TaskStatus.ACTIVE).count()
    tasks_archived = TaskModel.objects.filter(admin_id=user.id, status=TaskStatus.ARCHIVED).count()
    tasks_finished = TaskModel.objects.filter(admin_id=user.id, status=TaskStatus.FINISHED).count()
    tasks_failed = TaskModel.objects.filter(admin_id=user.id, status=TaskStatus.FAILED).count()
    groups_amount = GroupModel.objects.filter(members=user).count()

    if tasks_total != 0:
        success_stars = '*' * math.floor(5*tasks_finished/tasks_total)
        fail_stars = '*' * (5 - len(success_stars))
    else:
        success_stars = ''
        fail_stars = '*****'

    last_log_time_str_repr = datetime.strftime(
        user.last_log_time.astimezone(timezone.get_current_timezone()),
        settings.DATE_TIME_FORMAT
    )

    data = {
        'user': user,
        'tags': tags,
        'tasks_total': tasks_total,
        'tasks_active': tasks_active,
        'tasks_archived': tasks_archived,
        'tasks_finished': tasks_finished,
        'tasks_failed': tasks_failed,
        'success_stars': success_stars,
        'fail_stars': fail_stars,
        'last_log_time': last_log_time_str_repr,
        'groups_amount': groups_amount,
        'reminds': reminds
    }

    return render(request, 'profile_page.ejs', data)


#  Handlers of support urls


@csrf_protect
@login_required
def log_out_user(request):
    logout(request)
    return redirect(settings.HOME_PAGE_URL)


@csrf_protect
def authenticate_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            user_model = UserModel.objects.get(name=user.username)

            activate(settings.TIME_ZONE)
            user_model.last_log_time = timezone.localtime()

            user_model.save()
            return HttpResponseRedirect(settings.HOME_PAGE_URL)
        return HttpResponse('Account was disabled.')
    return HttpResponse('Invalid user name or email has been passed.')


@csrf_protect
def register_user(request):
    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']

    if User.objects.filter(username=username).count() != 0:
        return HttpResponse('User with such login already exists.')

    if User.objects.filter(email=email).count() != 0:
        return HttpResponse('User with such email already exists.')

    user = User.objects.create_user(username, email, password)
    UserModel.objects.create(name=username, email=email)
    user.save()

    login(request, user)

    return redirect(settings.HOME_PAGE_URL)

