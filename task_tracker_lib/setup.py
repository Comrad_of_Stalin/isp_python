from setuptools import setup, find_packages


if __name__ == '__main__':
    setup(
        name='task_tracker',
        version='2.4',
        packages=find_packages(),
        short_description="Task Tracker!!"
    )

