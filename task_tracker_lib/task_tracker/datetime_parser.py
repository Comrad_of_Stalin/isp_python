"""
This module contains functionss that help to convert different time objects to str or
convert str to time obbject (like datetime, date, relativedelta, etc.)
"""
from datetime import datetime, timedelta

from dateutil import relativedelta as rd
from task_tracker.exceptions import InvalidDeltaFormatException


def is_positive_digit(str_repr):
    """
    Helper function that indicates if str can be converted to natural number
    :param str_repr: string
    :return: True if it can be converted to positive integer else False
    """

    if not str_repr:
        return False
    for i in str_repr:
        if i > '9' or i < '0':
            return False
    return True


def replace_character_with_space(string, char):
    """
    Helper function tha replaces each char symbol in string by space
    :param string: str object
    :param char: char (symbol)
    :return: processed string
    """

    return ' '.join(string.split(char))


def parse_str_to_datetime(datetime_repr):
    """
    Gets datetime object from string. String should have format: %Y-%m-%d %H:%M:%S
    :param datetime_repr: str object
    :return: datetime object
    """

    date_format = "%Y-%m-%d"
    time_format = "%H:%M:%S"
    return datetime.strptime(
        datetime_repr, '{} {}'.format(
            date_format,
            time_format
        )
    )


def parse_str_to_date(date_repr):
    """
    Gets date object from string. String should have format: %Y-%m-%d
    :param date_repr: str object
    :return: date object
    """

    return parse_str_to_datetime(date_repr.strip()+' 0:0:0').date()


def datetime_to_str(datetime_obj):
    """
    Converts datetime object to str in format %Y-%m-%d %H:%M:%S
    :param datetime_obj: datetime object
    :return: str object
    """

    datetime_format = "%Y-%m-%d %H:%M:%S"
    return datetime.strftime(datetime_obj, datetime_format)


def parse_str_to_relativedelta(str_repr):
    """
    Parses string to relativedelta object.
    String must be in following format: '2 days, 1 month and 2 years'.
    Date members should be in following format: number: member_name  (example: 2 days).
    This format is not allowed: days: 2.
    Here are some examples:
    1 year, two months and 30 minutes
    2 days, 1 hour; 20 minutes and 40 seconds
    :param str_repr: str object
    :return: relativedelta object
    """

    str_repr = str_repr.lower()
    str_repr = replace_character_with_space(str_repr, ',')
    str_repr = replace_character_with_space(str_repr, '.')
    str_repr = replace_character_with_space(str_repr, ';')
    arr = str_repr.split()
    keys = []
    values = []
    datetime_format = ['years', 'months', 'days', 'hours', 'minutes', 'seconds', 'weeks']
    prev_taken = ''
    for item in arr:
        if is_positive_digit(item):
            values.append(item)
            if is_positive_digit(prev_taken):
                raise InvalidDeltaFormatException(str_repr)
            prev_taken = item
        elif item in datetime_format:
            keys.append(item)
            if prev_taken in datetime_format:
                raise InvalidDeltaFormatException(str_repr)
            prev_taken = item
        elif item+'s' in datetime_format:
            keys.append(item+'s')
            if prev_taken in datetime_format:
                raise InvalidDeltaFormatException(str_repr)
            prev_taken = item+'s'

    values = list(map(int, values))

    if len(keys) != len(values):
        print(keys)
        print(values)
        raise Exception

    props = dict(zip(keys, values))
    return rd.relativedelta(**props)


def parse_str_to_timedelta(str_repr):
    """
    Parses string to time object.
    WARNING!: Doesn't accepts parameters year and month!
    String must be in following format: '2 days, 1 month and 2 years'.
    Date members should be in following format: number: member_name  (example: 2 days).
    This format is not allowed: days: 2.
    Here are some examples:
    1 year, two months and 30 minutes
    2 days, 1 hour; 20 minutes and 40 seconds
    :param str_repr: str object
    :return: timedelta object
    """

    str_repr = str_repr.lower()
    str_repr = replace_character_with_space(str_repr, ',')
    str_repr = replace_character_with_space(str_repr, '.')
    str_repr = replace_character_with_space(str_repr, ';')
    arr = str_repr.split()
    keys = []
    values = []
    datetime_format = ['days', 'hours', 'minutes', 'seconds', 'weeks']
    prev_taken = ''
    for item in arr:
        if is_positive_digit(item):
            values.append(item)
            if is_positive_digit(prev_taken):
                raise InvalidDeltaFormatException(str_repr)
            prev_taken = item
        elif item in datetime_format:
            keys.append(item)
            if prev_taken in datetime_format:
                raise InvalidDeltaFormatException(str_repr)
            prev_taken = item
        elif item+'s' in datetime_format:
            keys.append(item+'s')
            if prev_taken in datetime_format:
                raise InvalidDeltaFormatException(str_repr)
            prev_taken = item+'s'

    values = list(map(int, values))

    if len(keys) != len(values):
        print(keys)
        print(values)
        raise Exception

    props = dict(zip(keys, values))
    return timedelta(**props)


def get_week_day_by_index(index):
    """
    Returns string representation of day of week with index index.
    Indexation starts from 0.
    :param index:
    :return:
    """

    return ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'][index]
