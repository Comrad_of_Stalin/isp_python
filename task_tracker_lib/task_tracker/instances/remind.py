"""
This module contains class Remind that helps to make notifications for tasks.

Notification is some event that happens before the end of the task and reminds you to do this task.

"""
from task_tracker import datetime_parser
from task_tracker import logger


class Remind:
    """
    This class is like a factory for reminds for tasks
    """
    @staticmethod
    @logger.logging_function
    def create_remind(task_finish_time, time_period, remind_msg, task_id):
        """
        Creates remind for defined task with defined message
        at time moment (task_finish_time - time_period)
        :param task_finish_time: datetime or ts str representation
        :param time_period: timedelta or its str representation
        :param remind_msg: str
        :return: tuple(remind_time: datetime, msg: str)
        """

        if isinstance(task_finish_time, str):
            task_finish_time = datetime_parser.parse_str_to_datetime(task_finish_time)
        if isinstance(time_period, str):
            time_period = datetime_parser.parse_str_to_relativedelta(time_period)
        remind_time = task_finish_time - time_period
        assert remind_time < task_finish_time, 'Can not set remind after task deadline'
        assert isinstance(remind_msg, str), 'Remind message must have type str'
        assert isinstance(task_id, str), 'Task id must have type str'

        return remind_time, remind_msg, task_id
