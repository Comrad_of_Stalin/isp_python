"""
This module contains the main models of task_tracker library.
Each module containns single class, which name equals to the name of module with the capitalized first letter
The models:

* User:
    class that realizes the main logic, i.e. adding tasks, reminds, tags, deleting them and so on
* Task:
    class that contains options for task objects like message, deadline, user_admin, and so on
* Tag:
    class that combines tasks with the common topic
*  TaskPlanner:
    class that sets rules, by whic periodic tasks are created
* Group:
    class that allows sharing tasks between different users
* Remind:
    class that makes notifications about tasks
* BaseIdObject:
    class, that represents object with unique id. Also it contains some useful function.
"""