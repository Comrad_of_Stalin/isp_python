"""
This module contains the Tracker class.
This class is a combines db logic, logger logic and helps to perform
main operations with instances.
"""
from task_tracker import configuration
from task_tracker.instances.user import User
from task_tracker import logger
from task_tracker import database_manager
from task_tracker.instances.user_manager import UserManager


class Tracker:
    """
    This class manages logger logic and database logic.
    Tt's objects init settings and configure pathes where other objects
    can be store/archived.
    Also objects of this class can configure level of logging of the library.
    """
    def __init__(self, path_to_db_dir, path_to_archive_dir):
        """
        Initializes tracker object with settings from config file.
        :param path_to_config_dir: Path to the directory where the config file locates.
        :param config_file_name: The name of config file.
        """
        self.config_storage = None
        self.path_to_db = path_to_db_dir
        self.path_to_archive = path_to_archive_dir
        self.manager = UserManager(self.path_to_db, self.path_to_archive)

    def get_user(self, username):
        """
        Loads/Creates user with name username.
        :param username: name of user (str)
        :return: User object
        """
        self.user = User(name=username)
        return self.user

    def set_logging_level(self, level):
        """
        Sets level of logging.
        :param level: level of logging None/DEBUG/INFO
        :return: None
        """
        database_manager.initialize_log_dir(
            log_dir=self.config_storage.get_log_dir(),
            config_storage=self.config_storage
        )
        logger.set_logging_level(level=level, config_storage=self.config_storage)

    def init_new_configuration(self, path_to_config_dir, config_file_name):
        """
        Initializes new configuration settings of object.
        :param path_to_config_dir: Path to the directory where the config file locates.
        :param config_file_name: The name of config file.
        :return: None
        """
        self.config_storage = configuration.ConfigStore(
            path_to_config_dir=path_to_config_dir,
            config_file_name=config_file_name
        )
