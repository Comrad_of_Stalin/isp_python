"""
This module contains Group class that represents
the common workspace about tasks for many users.
Each user in group has access (he can finish or fail tasks)
to the tasks that are shared to this group.
I still continue thinking about the architecture of this class.
"""
from task_tracker import database_manager
from task_tracker import logger
from task_tracker import tools
from task_tracker.instances.base_id_object import BaseIdObject


class Group(BaseIdObject):
    """
    Represents the common workspace about the tasks
    """
    @logger.logging_function
    def __init__(self,
                 group_name,
                 admin_id,
                 member_list=(),
                 task_list=(),
                 path_to_db_dir=None,
                 path_to_archive_dir=None,
                 save=False):
        """
        Creates new group with given list of users and admin
        :param group_name: name of group (str)
        :param admin_id: admin id (str)
        :param member_list: members of new group (list or tuple of id-s)
        :param task_list: shared tasks (list of task id-s)
        :param save: boolean. Indicates to push changes into DB
        """

        BaseIdObject.__init__(self,
                              group_name,
                              path_to_db_dir=path_to_db_dir,
                              path_to_archive_dir=path_to_archive_dir)
        assert isinstance(group_name, str), 'Name must be str'
        assert isinstance(admin_id, str), 'Admin id must be str'
        self.name = group_name
        self.members = list(member_list)
        if admin_id not in member_list:
            self.members.append(admin_id)

        self.task_list = list(task_list)
        self.admin_id = admin_id

        for task_id in self.task_list:
            assert isinstance(task_id, str), 'Task id in task list must have type str.'
            assert tools.object_is_storing(
                _id=task_id,
                path_to_db_dir=self.path_to_db_dir), 'Can\'t set unexisting task for group'
            assert task_id.startswith('Task_'), 'Wrong task id'

        for user_id in self.members:
            assert isinstance(user_id, str), 'User id in task list must have type str.'
            assert tools.object_is_storing(
                _id=user_id,
                path_to_db_dir=self.path_to_db_dir), 'Can\'t add unexisting user to group'
            assert user_id.startswith('User_'), 'Wrong user id'

        if path_to_db_dir:
            if database_manager.object_exists_in_db(obj_id=self.id, path_to_db_dir=path_to_db_dir):
                self.init_from_dict(
                    database_manager.get_object_from_db_(
                        obj_id=self.id,
                        path_to_db_dir=self.path_to_db_dir
                    ).__dict__)

        for user_id in self.members:
            self.add_user(user_id, path_to_db_dir=path_to_db_dir, save=save)

        for task_id in self.task_list:
            self.add_task(task_id, path_to_db_dir=path_to_db_dir, save=save)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def has_member(self, user_id):
        """
        Tells us if this group has such member
        :param user_id: person id
        :return: True or False
        """

        return user_id in self.members

    @logger.logging_function
    def add_user(self, user_id, path_to_db_dir=None, save=False):
        """
        Adds user to group
        :param user_id: str
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        assert isinstance(user_id, str), 'User_id must have type str'

        usr_obj = tools.get_object_by_id(user_id, path_to_db_dir=path_to_db_dir)

        assert usr_obj is not None, 'No such user exist'
        if usr_obj.id not in self.members:
            self.members.append(usr_obj.id)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def del_user(self, user_id, path_to_db_dir=None, save=False):
        """
        Deletes user from group
        :param user_id: str
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        assert isinstance(user_id, str), 'User_id must have type str'
        assert user_id != self.admin_id, 'Can not kick admin out of group'

        usr_obj = tools.get_object_by_id(user_id, path_to_db_dir=path_to_db_dir)
        assert usr_obj is not None, 'No such user exist'
        if usr_obj.id in self.members:
            self.members.remove(user_id)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def has_task(self, task):
        """
        Returns True if task with group has task
        :param task: task id or tuple(finish_time, id)
        :return: True if has, else False
        """

        task_id = None
        if isinstance(task, tuple):
            task_id = task[0]
        elif isinstance(task, str):
            task_id = task

        return task_id in self.task_list

    @logger.logging_function
    def add_task(self, task_id, path_to_db_dir=None, save=False):
        """
        Shares new task for this group
        :param task_id: str
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        assert isinstance(task_id, str), 'Task_id must have type str'
        task_obj = tools.get_object_by_id(task_id, path_to_db_dir=path_to_db_dir)
        assert task_obj is not None, 'No such task exist'
        assert task_obj.user_admin_id in self.members, \
            'User owner of task {} is not in group'.format(task_obj.user_admin_id)
        list_of_task_id = task_obj.add_group_to_all_subtasks(
            self.name,
            path_to_db_dir=path_to_db_dir,
            save=True
        )
        for _task_id in list_of_task_id:
            if _task_id not in self.task_list:
                self.task_list.append(_task_id)
        if save:
            task_obj.save(path_to_db_dir=path_to_db_dir)
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def remove_task(self, task_id, user_sender_id, path_to_db_dir=None, save=False):
        """
        Removes task from available tasks fr this group
        :param task_id: str
        :param user_sender_id: users admin of task id (str)
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        assert isinstance(task_id, str), 'Task_id must have type str'
        task_obj = tools.get_object_by_id(task_id, path_to_db_dir=path_to_db_dir)
        assert task_obj is not None, 'No such task exist'
        assert task_obj.user_admin_id == user_sender_id, \
            'User {} is not an owner of this task'.format(task_obj.user_admin_id)
        assert task_obj.id in self.task_list, 'No such task in group'
        self.task_list.remove(task_obj.id)
        task_obj.del_group(self.name, path_to_db_dir=path_to_db_dir)
        if save:
            task_obj.save(path_to_db_dir=path_to_db_dir)
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def clean_group_tasks(self, path_to_db_dir=None, save=False):
        """
        Removes unexisting tasks from group tasks
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        del_items = []
        for task_id in self.task_list:
            if not database_manager.object_exists_in_db(task_id):
                del_items.append(task_id)
        for item in del_items:
            self.task_list.remove(item)
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @database_manager.saving_changes
    @logger.logging_function
    def save(self, path_to_db_dir=None):
        """
        Saves changes to DB
        :return: None
        """

        if path_to_db_dir is not None:
            self.path_to_db_dir = path_to_db_dir

    @database_manager.removing_object
    @logger.logging_function
    def delete(self, path_to_db_dir=None):
        """
        Removes group
        :return: None
        """

        if path_to_db_dir is not None:
            self.path_to_db_dir = path_to_db_dir
