"""
This module contains Task class that is used to store information about tasks, i.e. their appearing time,
finish time(deadline), message, user_admin, groups that have access to this task.
"""
import datetime
import logging

from task_tracker import database_manager
from task_tracker import datetime_parser
from task_tracker import logger
from task_tracker import tools
from task_tracker.exceptions import WrongDataException
from task_tracker.instances import base_id_object


class Task(base_id_object.BaseIdObject):
    """
    This class contains the main data for task,
    including time of creation, user owner,
    message, start time, finish time (deadline),
    percent of readiness, list of id-s of it's child tasks,
    current status, groups and so on.

    The objects of this class contain all the vital data that user needs to know about this task.

    """

    COMPLETED = 'COMPLETED'
    FAILED = 'FAILED'
    ARCHIVED = 'ARCHIVED'
    ACTIVE = 'ACTIVE'
    SCIPPED = 'SCIPPED'
    NOT_ACTIVE = 'NOT_ACTIVE'

    @logger.logging_function
    def __init__(
            self,
            finish_time=datetime.datetime(year=3000, month=1, day=1),
            msg='Default msg',
            users_owners_list=[],
            user_admin_id=None,
            priority=1,
            start_time=datetime.datetime.now(),
            parent_task_id=None,
            path_to_db_dir=None,
            path_to_archive_dir=None,
            save=False):
        """
        Creates Task object. If it already exists, retrieves it from DB
        :param finish_time: datetime or its str representation in format Y-M-D H:M:S
        :param msg: Message (str)
        :param users_owners_list: useless
        :param user_admin_id: user admin (the one, who has absolute rights on this task) (str)
        :param priority: positive number
        :param start_time: datetime or its str representation in format Y-M-D H:M:S
        :param parent_task_id: used to make this task a subtask
                of parent_task, if no, set this option to None
        :param save: boolean. Indicates t save object to db
        """

        base_id_object.BaseIdObject.__init__(self)

        if not isinstance(finish_time, datetime.datetime):
            finish_time = datetime_parser.parse_str_to_datetime(finish_time)
        self.finish_time = finish_time
        if not isinstance(start_time, datetime.datetime):
            start_time = datetime_parser.parse_str_to_datetime(start_time)
        self.start_time = start_time
        self.msg = msg
        self.users_owners_list = users_owners_list
        self.priority = priority
        self.remind_list = []
        self.percent_ready = 0
        self.parent_task_id = parent_task_id
        self.child_task_list = []
        self.total_complexity = self.priority
        self.is_ready = False
        self.user_admin_id = user_admin_id
        self.tag_list = []
        self.group_id = None
        self.groups = []
        self.user_successor = None

        if self.start_time > datetime.datetime.now():
            self.status = Task.NOT_ACTIVE
        else:
            self.status = Task.ACTIVE
        self.last_time_modified = datetime.datetime.now()

        if self.user_admin_id is not None and self.user_admin_id not in self.users_owners_list:
            self.users_owners_list.append(self.user_admin_id)
        if path_to_db_dir is not None:
            if database_manager.object_exists_in_db(obj_id=self.id, path_to_db_dir=path_to_db_dir):
                self.init_from_dict(tools.get_object_by_id(
                    _id=self.id,
                    path_to_db_dir=self.path_to_db_dir
                ).__dict__)

        assert isinstance(self.msg, str), 'msg must have type string'
        assert isinstance(self.priority, int), 'priority must have type int'
        assert 1 <= priority <= 10, 'Priority must be int in range 1..10'
        assert isinstance(self.user_admin_id, str) or self.user_admin_id is None, \
            'user_admin_id have type string'
        assert self.start_time < self.finish_time, 'You can\'t set start time after deadline!'
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def activate_task(self, path_to_db_dir=None, save=False):
        """
        Activates task if its time to do it
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        self.status = Task.ACTIVE
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def edit_task(
            self,
            new_finish_time=None,
            new_msg=None,
            new_priority=None,
            path_to_db_dir=None,
            save=False):
        """
        Sets new properties for task
        :param new_finish_time: new deadline (datetime)
        :param new_msg: new message (str)
        :param new_priority: new priority (int)
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        if new_finish_time is None and new_msg is None:
            logging.info('You must set new time or new message while editing task.')
        if isinstance(new_finish_time, str):
            new_finish_time = datetime_parser.parse_str_to_datetime(new_finish_time)
            assert isinstance(new_finish_time, datetime.datetime), \
                'Wrong data was passed for new deadline'
            assert new_finish_time > datetime.datetime.now(), \
                'You can\'t set finish time to the past'

        if new_msg is not None:
            assert isinstance(new_msg, str), 'Message must have type str'
        if new_finish_time is not None:
            self.finish_time = new_finish_time
        if new_priority is not None:
            assert isinstance(new_priority, int), \
                'Wrong data was passed for new priority'
            assert 1 <= new_priority <= 10, 'Priority must be int in range 1..10'
            self.priority = new_priority
        if new_msg is not None:
            self.msg = new_msg

        self.last_time_modified = datetime.datetime.now()

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def set_parent_task(self, parent_task, path_to_db_dir=None, save=False):
        """
        Sets parent task for this task
        :param parent_task: Task object or id (str)
        :param save:
        :return: None
        """

        if isinstance(parent_task, Task):
            self.parent_task_id = parent_task.id
        elif isinstance(parent_task, str):
            self.parent_task_id = parent_task
        elif parent_task is None:
            self.parent_task_id = parent_task
        else:
            raise WrongDataException('parent_task argument must be a Task or str or None')
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def clean_child_list(self, save=False, path_to_db_dir=None, path_to_archive_dir=None):
        """
        Removes empty children
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        del_items = []
        for item in self.child_task_list:
            _id = item['id']
            if not tools.object_is_storing(_id=_id, path_to_db_dir=path_to_db_dir) and not \
                    tools.object_is_in_archive(_id=_id, path_to_archive_dir=path_to_archive_dir):
                del_items.append(item)

        for item in del_items:
            self.child_task_list.remove(item)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def add_child_task(self, child_task, path_to_db_dir=None, path_to_archive_dir=None, save=False):
        """
        Adds new subtask to this task
        :param child_task: Task object or id (str)
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        self._add_new_child_to_list(child_task, path_to_db_dir=path_to_db_dir, save=save)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

        root_task = self._find_root_task(path_to_db_dir=path_to_db_dir)
        root_task.recalculate_readiness(
            save=save,
            path_to_db_dir=path_to_db_dir,
            path_to_archive_dir=path_to_archive_dir
        )

    @logger.logging_function
    def deattach_child_task(self, child_task, path_to_db_dir=None, path_to_archive_dir=None, save=False):
        """
        Removes child from self children list and recalculates self readiness.
        :param child_task: Task object or id (str)
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        self._remove_child_from_list(child_task, path_to_db_dir=path_to_db_dir, save=True)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

        root_task = self._find_root_task(path_to_db_dir=path_to_db_dir)
        root_task.recalculate_readiness(
            save=save,
            path_to_db_dir=path_to_db_dir,
            path_to_archive_dir=path_to_archive_dir
        )

    @logger.logging_function
    def _add_new_child_to_list(self, child_task, path_to_db_dir=None, save=False):
        """
        Adds new child to list of children without doing any additional calculations
        :param child_task: Task object or id (str)
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        if isinstance(child_task, Task):
            item = {'complexity': child_task.total_complexity, 'id': child_task.id}

            if item in self.child_task_list:
                return

            self.child_task_list.append(item)
            self.total_complexity += child_task.total_complexity

            child_task.set_parent_task(self.id, save=save)

        elif isinstance(child_task, str):
            child_task = tools.get_object_by_id(
                _id=child_task,
                path_to_db_dir=path_to_db_dir
            )
            item = {'complexity': child_task.total_complexity, 'id': child_task.id}

            if item in self.child_task_list:
                return

            self.child_task_list.append(item)
            self.total_complexity += child_task.total_complexity

            child_task.set_parent_task(self.id, save=save)
        else:
            raise WrongDataException('child_task argument must be a Task or str')

    @logger.logging_function
    def _remove_child_from_list(self, child_task, path_to_db_dir=None, save=False):
        """
        Adds new child to list of children without doing any additional calculations
        :param child_task: Task object or id (str)
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        item = None
        if isinstance(child_task, Task):
            item = {'complexity': child_task.total_complexity, 'id': child_task.id}
        elif isinstance(child_task, str):
            child_task = tools.get_object_by_id(
                _id=child_task,
                path_to_db_dir=path_to_db_dir
            )
            item = {'complexity': child_task.total_complexity, 'id': child_task.id}
        else:
            raise WrongDataException('child_task argument must be a Task or str')

        if item not in self.child_task_list:
            return

        self.child_task_list.remove(item)
        self.total_complexity -= child_task.total_complexity

        child_task.set_parent_task(None, path_to_db_dir=path_to_db_dir, save=save)

    @logger.logging_function
    def recalculate_readiness(self, save=False, path_to_db_dir=None, path_to_archive_dir=None):
        """
        Recalculates percent of readiness for this task using simple DFS
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        self.total_complexity = self.priority
        points_of_readiness = 0

        self.clean_child_list(
            save=save,
            path_to_db_dir=path_to_db_dir,
            path_to_archive_dir=path_to_archive_dir
        )

        for child_data in self.child_task_list:
            task_complexity = child_data['complexity']
            task_id = child_data['id']

            child_task = tools.get_object_by_id(
                _id=task_id,
                path_to_db_dir=path_to_db_dir
            )

            if child_task is None or child_task.status in [Task.COMPLETED, Task.SCIPPED]:
                self.total_complexity += task_complexity
                points_of_readiness += task_complexity
            else:
                if child_task.is_ready:
                    self.total_complexity += child_task.total_complexity
                    points_of_readiness += child_task.total_complexity
                else:
                    child_task.recalculate_readiness(
                        path_to_db_dir=path_to_db_dir,
                        path_to_archive_dir=path_to_archive_dir,
                        save=save
                    )
                    self.total_complexity += child_task.total_complexity
                    points_of_readiness += child_task.total_complexity*child_task.percent_ready/100
                    child_data['complexity'] = child_task.total_complexity
        self.percent_ready = points_of_readiness/self.total_complexity*100

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def get_percent_of_readiness(self, save=False, path_to_db_dir=None, path_to_archive_dir=None):
        """
        Returns recalculated percent of readiness for this task
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        self.recalculate_readiness(
            save=save,
            path_to_db_dir=path_to_db_dir,
            path_to_archive_dir=path_to_archive_dir
        )
        return self.percent_ready

    @logger.logging_function
    def _finish_all_subtasks(
            self,
            save=False,
            archive=False,
            path_to_db_dir=None,
            path_to_archive_dir=None):
        """
        Finshes all subtasks in the subtree
        :param save: boolean. Indicates to save object to db
        :param archive: if this option is set to True, all subtasks will be moved to the archive
        :return: None
        """

        if self.status != Task.ACTIVE:
            if not archive:
                self.delete()
            else:
                self.archive()
            return
        self.is_ready = True
        self.status = Task.COMPLETED
        self.percent_ready = 100

        for child_data in self.child_task_list:
            task_id = child_data['id']

            child_task = tools.get_object_by_id(
                _id=task_id,
                path_to_db_dir=path_to_db_dir
            )

            if child_task is not None:
                child_task._finish_all_subtasks(
                    save=save,
                    archive=archive,
                    path_to_db_dir=path_to_db_dir,
                    path_to_archive_dir=path_to_archive_dir
                )

        if not archive:
            self.delete(path_to_db_dir=path_to_db_dir)
        else:
            self.archive(path_to_db_dir=path_to_db_dir, path_to_archive_dir=path_to_archive_dir)

    @logger.logging_function
    def finish_task(
            self,
            user_successor=None,
            save=False,
            archive=False,
            path_to_db_dir=None,
            path_to_archive_dir=None):
        """
        Finishes task and all its subtasks and recalculates percent of readiness of the root task
        :param user_successor: user, that finished this task
        :param save: boolean. Indicates to save object to db
        :param archive: boolean. If this option is set to True,
        all finished tasks would be archived, else the will be deleted
        :return:
        """

        if not hasattr(self, 'user_successor'):
            self.user_successor = None
        self.user_successor = user_successor

        self._finish_all_subtasks(
            save=save,
            archive=archive,
            path_to_db_dir=path_to_db_dir,
            path_to_archive_dir=path_to_archive_dir
        )

        root_task = self._find_root_task(path_to_db_dir=path_to_db_dir)
        root_task.recalculate_readiness(
            save=save,
            path_to_db_dir=path_to_db_dir,
            path_to_archive_dir=path_to_archive_dir
        )

    @logger.logging_function
    def _find_root_task(self, path_to_db_dir=None):
        """
        Returns the root task
        :return: Root task (Task object)
        """

        current_task = self
        while current_task.parent_task_id is not None:
            current_task = tools.get_object_by_id(
                _id=current_task.parent_task_id,
                path_to_db_dir=path_to_db_dir
            )
        return current_task

    @logger.logging_function
    def add_user_owner(self, user_id, save=False):
        """
        Adds user that cn watch this task, but I think its weird ad I'll delete it later
        :param user_id: str
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        if user_id not in self.users_owners_list:
            self.users_owners_list.append(user_id)

    @logger.logging_function
    def del_user_owner(self, user_id, save=False):
        """
        Deletes user froom owners list
        :param user_id: str
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        if user_id in self.users_owners_list:
            self.users_owners_list.remove(user_id)
        if user_id == self.user_admin_id:
            self.user_admin_id = None

    @logger.logging_function
    def add_tag(self, tag_name, path_to_db_dir=None, save=False):
        """
        Adds tag to task
        :param tag_name: str
        :param save: :param save: boolean. Indicates to save object to db
        :return: None
        """

        assert isinstance(tag_name, str), 'Tag name must have type str'
        if not hasattr(self, 'tag_list'):
            self.tag_list = []
        self.tag_list.append(tag_name)
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def del_tag(self, tag_name, path_to_db_dir=None, save=False):
        """
        Removes tag from this task
        :param tag_name: str
        :param save: :param save: boolean. Indicates to save object to db
        :return: None
        """

        assert isinstance(tag_name, str), 'Tag name must have type str'
        if tag_name in self.tag_list:
            self.tag_list.remove(tag_name)
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def list_tags(self):
        """
        Simple getter that returns tag list
        :return: tag list
        """

        return self.tag_list

    @logger.logging_function
    def _fail_all_subtasks(
            self,
            path_to_db_dir=None,
            path_to_archive_dir=None,
            archive=False,
            save=False):
        """
        Fails all the subtasks of this task. This is the helper method
        :param archive: boolean. If this option is set to True,
        failed tasks would be moved to archive, else thy will be simply deleted
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        if self.status == Task.ACTIVE:
            self.status = Task.FAILED
        self.is_ready = True

        for child_data in self.child_task_list:
            _id = child_data['id']
            if tools.object_is_storing(_id=_id, path_to_db_dir=path_to_db_dir):
                child_task = tools.get_object_by_id(_id=_id, path_to_db_dir=path_to_db_dir)
                if child_task is None:
                    continue
                child_task._fail_all_subtasks(
                    path_to_db_dir=path_to_db_dir,
                    path_to_archive_dir=path_to_archive_dir,
                    save=save,
                    archive=archive
                )

        if save:
            if archive:
                self.archive(
                    path_to_db_dir=path_to_db_dir,
                    path_to_archive_dir=path_to_archive_dir
                )
            else:
                self.delete()

    @logger.logging_function
    def fail_task(self, path_to_db_dir=None, path_to_archive_dir=None, archive=False, save=False):
        """
        Fails task and its subtasks and optionaly moves them to archive
        :param archive: boolean. If this option is set to True,
        failed tasks would be moved to archive, else thy will be simply deleted
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        root_task = self._find_root_task(path_to_db_dir=path_to_db_dir)
        root_task._fail_all_subtasks(
            path_to_archive_dir=path_to_archive_dir,
            path_to_db_dir=path_to_db_dir,
            archive=archive,
            save=save
        )

    @logger.logging_function
    def get_subtree_list(self, init_list, path_to_db_dir=None):
        """
        Returns all the distinct tasks that are in task subtree
        :param init_list: collector container
        :return: None
        """

        init_list.append((self.finish_time, self.id))

        for _time, _id in self.child_task_list:
            t_obj = tools.get_object_by_id(_id=_id, path_to_db_dir=path_to_db_dir)
            if t_obj is not None:
                t_obj.get_subtree_list(init_list)

        return init_list

    @logger.logging_function
    def add_remind(self, *args, **kwargs):
        """
        Adds remind for this task
        :param args:
        :param kwargs:
        :return: None
        """

        for user_id in self.users_owners_list:
            u = tools.get_object_by_id(_id=user_id, path_to_db_dir=self.path_to_db_dir)
            if u is not None:
                u.add_remind(*args, **kwargs)

    @database_manager.removing_object
    @logger.logging_function
    def delete(self, path_to_db_dir=None):
        """
        Deletes this task from DB
        :return: None
        """

        if self.status == Task.ACTIVE:
            self.status = Task.SCIPPED

        for child_data in self.child_task_list:
            _id = child_data['id']
            if tools.object_is_storing(_id=_id, path_to_db_dir=path_to_db_dir):
                child_task = tools.get_object_by_id(_id=_id, path_to_db_dir=path_to_db_dir)
                if child_task is not None:
                    child_task.delete(path_to_db_dir=path_to_db_dir)

    @database_manager.saving_changes
    @logger.logging_function
    def save(self, path_to_db_dir=None):
        """
        Saves this task to DB
        :return: None
        """
        self.path_to_db_dir = path_to_db_dir
        if not self.users_owners_list or not self.user_admin_id:
            self.prevent_from_saving_in_db()
            self.delete(path_to_db_dir=path_to_db_dir)

    @database_manager.archiving_object
    @logger.logging_function
    def archive(self, path_to_db_dir=None, path_to_archive_dir=None):
        """
        Moves this task and all its subtasks to the archive
        :return: None
        """

        if path_to_db_dir is None:
            path_to_db_dir = self.path_to_db_dir

        if path_to_archive_dir is None:
            path_to_archive_dir = self.path_to_archive_dir

        if self.status == Task.ACTIVE:
            self.status = Task.ARCHIVED

        u = tools.get_object_by_id(_id=self.user_admin_id, path_to_db_dir=path_to_db_dir)
        u.del_task_from_task_list((self.finish_time, self.id), path_to_db_dir=path_to_db_dir)

        if not (self.finish_time, self.id) in u.archived_tasks:
            u.archived_tasks.append((self.finish_time, self.id))
            u.save(path_to_db_dir=path_to_db_dir)

        self.path_to_db_dir = path_to_db_dir
        self.path_to_archive_dir = path_to_archive_dir

        for child_data in self.child_task_list:
            _id = child_data['id']
            if tools.object_is_storing(_id=_id, path_to_db_dir=path_to_db_dir):
                child_task = tools.get_object_by_id(_id=_id, path_to_db_dir=path_to_db_dir)
                child_task.archive(
                    path_to_db_dir=path_to_db_dir,
                    path_to_archive_dir=path_to_archive_dir
                )

    @logger.logging_function
    def add_group(self, group_name, path_to_db_dir=None, save=False):
        """
        Adds group that can change this task
        :param group_name: str
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        if not hasattr(self, 'groups'):
            self.groups = []
        group_id = 'Group_{}'.format(group_name)
        if group_id not in self.groups:
            self.groups.append(group_id)
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def del_group(self, group_name, path_to_db_dir=None, save=False):
        """
        Deletes group with group_name from groups for this task
        :param group_name: str
        :param save: boolean. Indicates to save object to db
        :return: None
        """

        if not hasattr(self, 'groups'):
            self.groups = []

        group_id = group_name if not group_name.startswith('Group_') else 'Group_{}'.format(group_name)
        if group_id in self.groups:
            self.groups.remove(group_id)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def add_group_to_all_subtasks(self, group_name, path_to_db_dir=None, save=False):
        """
        Walks the subtasks and adds group with name group_name, to each subtask
        :param group_name: name of group
        :param save: boolean. Indicates to push changes into db.
        :return: None
        """

        def pre(task_obj, *args, **kwargs):
            if not kwargs['group_name'] in task_obj.groups:
                task_obj.add_group(group_name, path_to_db_dir=path_to_db_dir, save=save)
            return [task_obj.id]

        def post(task_obj, *args, **kwargs):
            ret = []
            for l in kwargs['postcalc_child_results']:
                ret += l
            return ret + kwargs['precalc_value']

        fir, sec = self.attach_func_to_each_subtask(
            pre_func=pre,
            post_func=post,
            args_to_pre_func=(),
            kwargs_to_pre_func={'group_name': group_name},
            args_to_post_func=('use_precalc_value', 'use_child_results',),
            kwargs_to_post_func={'group_name': group_name}
        )
        return sec

    @logger.logging_function
    def attach_func_to_each_subtask(
            self,
            pre_func,
            post_func,
            args_to_pre_func,
            kwargs_to_pre_func,
            args_to_post_func,
            kwargs_to_post_func,
            save=False
    ):
        """
        Applies two defined functions to all the subtasks of the current task, using DFS algorithm.
        :param pre_func: function (callable object) that will be called at the beginning from each
        task in subtree, before it starts calling from it's children.
        :param post_func: function that calls after the function main function is called from children
        (ususally used to process precomputed values from children)
        :param args_to_pre_func: positional arguments for pre_func
        :param kwargs_to_pre_func: keyword arguments for pre_func
        :param args_to_post_func: positional arguments for post func
        :param kwargs_to_post_func: keyword arguments for post func
        :param save: boolean. Indicates to push changes into db.
        :return: two returned value (from first and second functions)
        """

        precalc_val = pre_func(self, *args_to_pre_func, **kwargs_to_pre_func)
        self.clean_child_list(save=save)
        precalc_child_results = []
        postcalc_child_results = []

        for child_data in self.child_task_list:
            _id = child_data['id']
            child_vals = tools.get_object_by_id(
                _id=_id,
                path_to_db_dir=self.path_to_db_dir
            ).attach_func_to_each_subtask(
                pre_func=pre_func,
                post_func=post_func,
                args_to_pre_func=args_to_pre_func,
                kwargs_to_pre_func=kwargs_to_pre_func,
                args_to_post_func=args_to_post_func,
                kwargs_to_post_func=kwargs_to_post_func,
                save=save
            )
            precalc_child_results.append(child_vals[0])
            postcalc_child_results.append(child_vals[1])

        additional_args = {}

        if 'use_precalc_value' in args_to_post_func:
            additional_args['precalc_value'] = precalc_val

        if 'use_child_results' in args_to_post_func:
            additional_args['precalc_child_results'] = precalc_child_results
            additional_args['postcalc_child_results'] = postcalc_child_results

        postcalc_val = post_func(self, *args_to_post_func, **kwargs_to_post_func, **additional_args)

        return precalc_val, postcalc_val
