"""
This module contains Tag class which allows to group
tasks by common topics or some other criterias
Each tag belongs to one user.
There can also exist two users with same tags, i.e. for example:
    user A has tag FOO and user B cn also have tag FOO
But this tags aren't the same.
"""
from task_tracker import database_manager
from task_tracker import logger
from task_tracker import tools
from task_tracker.instances import base_id_object
from task_tracker.instances.task import Task


class Tag(base_id_object.BaseIdObject):
    """
    Tag object represents a set of tasks with common topic.
    There can not be two tags with equal names in one user. But there can be tags
    wit similar names but each of them must belong to different user.

    One task can have multiple tags.

    Tag also can have multiply tasks, as it was defined above.

    To see information about any method, type: help(method)
    """
    @logger.logging_function
    def __init__(self,
                 tag_name,
                 user_owner_id,
                 description='Default description',
                 path_to_db_dir=None,
                 path_to_archive_dir=None):
        """
        Creates tag. If it already exists, retrieves it from DB
        :param tag_name: str
        :param user_owner_id: id of tag owner (str)
        :param description: str
        :param save: boolean. Indicates t save object to db
        """

        assert user_owner_id.startswith('User_'), 'Wrong user_id was passed'
        user_owner_name = user_owner_id[5:]
        assert isinstance(tag_name, str), 'Tag name must have type str'
        assert isinstance(user_owner_name, str), 'User name must have type str'
        base_id_object.BaseIdObject.__init__(self,
                                             user_owner_name+'_'+tag_name,
                                             path_to_db_dir=path_to_db_dir,
                                             path_to_archive_dir=path_to_archive_dir)
        self.name = tag_name
        self.tasks = {}
        self.user_owner = user_owner_name
        self.description = description

        if self.path_to_db_dir is not None:
            if database_manager.object_exists_in_db(obj_id=self.id, path_to_db_dir=self.path_to_db_dir):
                self.init_from_dict(
                    database_manager.get_object_from_db_(
                        obj_id=self.id,
                        path_to_db_dir=self.path_to_db_dir
                    ).__dict__)

    @logger.logging_function
    def add_tag_to_task(self, task, path_to_db_dir=None, save=False):
        """
        Adds tag to task and adds task to task id list of this tag
        :param task: id (str) or Task object
        :param save: boolean. Indicates t save object to db
        :return: None
        """

        if isinstance(task, str):
            task = tools.get_object_by_id(task, path_to_db_dir=path_to_db_dir)
        assert isinstance(task, Task), 'Wrong data was passed instead of task object or id'

        task.add_tag(self.name, path_to_db_dir=path_to_db_dir, save=save)
        self.tasks[task.id] = task.finish_time

    @logger.logging_function
    def remove_tag_from_task(self, task, path_to_db_dir=None, save=False):
        """
        Removes tagname from task.tag_list and similarly removes task.id from tag.tasks
        :param task: id (str) or Task object
        :param save: boolean. Indicates t save object to db
        :return: None
        """
        if isinstance(task, str):
            task = tools.get_object_by_id(task, path_to_db_dir=path_to_db_dir)
        assert isinstance(task, Task), 'Wrong data was passed instead of task object or id'

        task.del_tag(self.name, path_to_db_dir=path_to_db_dir, save=save)

        if task.id in self.tasks:
            del self.tasks[task.id]
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @database_manager.saving_changes
    @logger.logging_function
    def save(self, path_to_db_dir=None):
        self.path_to_db_dir = path_to_db_dir
        """
        Saves object to db
        :return: None
        """

    @database_manager.removing_object
    @logger.logging_function
    def remove(self, path_to_db_dir=None):
        if path_to_db_dir is not None:
            self.path_to_db_dir = path_to_db_dir
        """
        Deletes object from db
        :param save: boolean. Indicates t save object to db
        :return: None
        """

    @logger.logging_function
    def change_description(self, new_description, path_to_db_dir=None, save=False):
        """
        Edits Tag description
        :param new_description: str
        :param save: boolean. Indicates t save object to db
        :return: None
        """
        if not hasattr(self, 'description'):
            self.description = 'Default description'
        assert isinstance(new_description, str), 'Description must have type str'
        self.description = new_description
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def clean_task_list(self, path_to_db_dir=None, save=False):
        """
        Removes unexisting tasks from task list
        :param save: boolean. Indicates to push changes into DB.
        :return:
        """

        del_items = []
        for task_id in self.tasks:
            if not tools.object_is_storing(task_id, path_to_db_dir=path_to_db_dir):
                del_items.append(task_id)

        for item in del_items:
            del self.tasks[item]
        if save:
            self.save(path_to_db_dir=path_to_db_dir)
