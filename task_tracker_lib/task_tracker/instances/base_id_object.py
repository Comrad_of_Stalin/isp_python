"""
This class represents the base database and is object.
It contains several useful methods.
Each descendant of this class would have unique id, based on its unique qualities.
"""
import uuid

from task_tracker import configuration
from task_tracker import database_manager
from task_tracker import logger


class BaseIdObject:
    """
    Base class for the main instances of this project.
    """

    @logger.logging_function
    def __init__(self, id_key=None, path_to_db_dir=None, path_to_archive_dir=None):
        """
        Creates object with unique id and stores
        a place where this object should be stored.
        :param id_key: parameter that is used to create id
        :param path_to_db_dir: parameter to the directory where the object should be saved.
        :param path_to_db_dir: parameter to the directory where the object should be archived.
        :return None
        """

        if id_key is None:
            id_key = uuid.uuid1()
        if isinstance(id_key, str):
            if id_key.startswith(self.__class__.__name__+'_'):
                id_key = id_key[len(self.__class__.__name__)+1:]

        self.path_to_db_dir = path_to_db_dir
        self.path_to_archive_dir = path_to_archive_dir

        self.id = self.__class__.__name__ + '_' + str(id_key)
        self.__can_be_saved = True
        self.__can_be_archived = False

    @logger.logging_function
    def configure_storage_place(self, path_to_db_dir=None, path_to_archive_dir=None):
        """
        Sets new storage place (db directory or archive directory) for this object
        :param path_to_db_dir: path to db directory (str)
        :param path_to_archive_dir: path to archive directory (str)
        :return: None
        """

        if path_to_db_dir is not None:
            self.path_to_db_dir = path_to_db_dir
        if path_to_archive_dir is not None:
            self.path_to_archive_dir = path_to_archive_dir

    @logger.logging_function
    def prevent_from_saving_in_db(self):
        """
        Prevent saving this object in db
        :return: None
        """

        self.__can_be_saved = False

    @logger.logging_function
    def prevent_from_saving_in_archive(self):
        """
        Prevent saving this object in archive
        :return: None
        """

        self.__can_be_saved = False

    @logger.logging_function
    def allow_to_archive(self):
        """
        Allows saving this object in archive
        :return: None
        """

        self.__can_be_archived = True

    @logger.logging_function
    def allow_saving_in_db(self):
        """
        Allows saving this object in db. May be useless
        :return: None
        """

        self.__can_be_saved = False

    @logger.logging_function
    def can_be_saved(self):
        """
        Tells that object can besaved in DB
        :return: None
        """

        return self.__can_be_saved

    @logger.logging_function
    def can_be_archived(self):
        """
        Tells that object can be archived
        :return: None
        """

        return self.__can_be_archived

    @logger.logging_function
    def copy(self, other):
        """
        Copy the properties of another object
        :param other: another instance
        :return: None
        """

        if other is None:
            return
        for prop, val in other.__dict__.items():
            setattr(self, prop, val)

    @logger.logging_function
    def init_from_dict(self, dict_data):
        """Initialize properties from dict"""

        if dict_data is None:
            return
        for prop, val in dict_data.items():
            setattr(self, prop, val)

    @logger.logging_function
    def update_self_state(self):
        """
        Updates current state of object, loading the latest version of it.
        :return: Latest version of object or None if object doesn't exist any more.
        """

        self.init_from_dict(
            database_manager.get_object_from_db_(
                obj_id=self.id,
                path_to_db_dir=self.path_to_db_dir
            ).__dict__)
