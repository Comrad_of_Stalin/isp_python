"""
This module contains User class that implements the main logic of my project.

User instance has id and several data fields that help to perform
operations with other instances.
"""
import datetime
import heapq

from task_tracker import database_manager
from task_tracker import logger
from task_tracker import tools
from task_tracker.exceptions import (AccessToUnExistingObjectException)
from task_tracker.instances import base_id_object
from task_tracker.instances.task import Task


class User(base_id_object.BaseIdObject):
    """
    This class represents user logic and contains many data fields,
    but less methods (most of them are dedicated to clean data fields, or to
    indicate if user object has such option).
    """

    @logger.logging_function
    def __init__(self, name):
        """
        Creates user with name name without linking it with the db.
        :param name: name of user. each user mut have unique name.
        """

        base_id_object.BaseIdObject.__init__(self,
                                             name)
        self.name = name
        self.task_queue = []
        self.archived_tasks = []
        self.planner_list = []
        self.remind_list = []
        self.aliases = {}
        self.tags = {}
        self.groups = []
        self.last_time_logged_in = datetime.datetime.now()
        self.time_since_last_log = datetime.timedelta(seconds=0)

    # Task logic

    @logger.logging_function
    def is_admin_of_task(self, task, path_to_db_dir=None):
        """
        Checks if the user has such task
        :param task: (unique task identifier) Task object or str or tuple(datetime, id)
        :return: boolean. True if the user has such task else False
        """

        if isinstance(task, str):
            task_data = tools.get_object_by_id(_id=task, path_to_db_dir=path_to_db_dir)
            if not hasattr(task_data, 'user_admin_id') or task_data['user_admin_id'] != self.id:
                return False
            return True
        if isinstance(task, tuple):
            task_obj = tools.get_object_by_id(_id=task[1], path_to_db_dir=path_to_db_dir)
            if not hasattr(task_obj, 'user_admin_id') or task_obj.user_admin_id != self.id:
                return False
            return True
        if isinstance(task, Task):
            return task.user_admin_id == self.id
        return False

    @logger.logging_function
    def has_failed_tasks(self):
        """
        Method that returns True if the user has failed tasks. Used to update task list
        :return: True if user has failed tasks
        """

        for _time, _id in self.task_queue:
            if _time < datetime.datetime.now():
                return True
        return False

    @logger.logging_function
    def del_task_from_task_list(self, task, path_to_db_dir=None):
        """
        Deletes task from users task list but not from the DB (if it exists in DB)
        :param path_to_db_dir: path to directory where user should look for objects
        :param task: identifier of task. May have type str (id), tuple (datetime, id) or Task object
        :return: None
        """

        _id = None
        if isinstance(task, str):
            _id = task
            task = tools.get_object_by_id(_id=task, path_to_db_dir=path_to_db_dir)
        elif isinstance(task, Task):
            _id = task.id
        elif isinstance(task, tuple):
            _id = task[1]
            task = tools.get_object_by_id(_id=task[1], path_to_db_dir=path_to_db_dir)

        index = 0
        for item in self.task_queue:
            if item[1] == _id:
                break
            index += 1

        if index < len(self.task_queue):
            self.task_queue.__delitem__(index)

        if task is not None:
            task = (task.finish_time, task.id)
        else:
            return

        if task in self.task_queue:
            self.task_queue.remove(task)
            heapq.heapify(self.task_queue)
            _id = task[1]
            if tools.object_is_storing(_id=_id, path_to_db_dir=path_to_db_dir):
                task_obj = tools.get_object_by_id(_id=_id, path_to_db_dir=path_to_db_dir)
                for child in task_obj.child_task_list:
                    self.del_task_from_task_list(child['id'])

    @logger.logging_function
    def del_task_from_archive_list(self, task, path_to_archive_dir=None):
        """
        Deletes task from users list of archived tasks but not from the DB (if it exists in DB)
        :param task: identifier of task. May have type str (id), tuple (datetime, id) or Task object
        :return: None
        """

        task_obj = None
        if isinstance(task, str):
            task_obj = tools.get_object_from_archive(
                _id=task,
                path_to_archive_dir=path_to_archive_dir
            )
            task_obj = (task_obj.finish_time, task_obj.id)
        elif isinstance(task, Task):
            task_obj = (task.finish_time, task.id)
        assert isinstance(task, tuple) and len(task) == 2, 'Something went wrong!'
        if task_obj is None:
            raise AccessToUnExistingObjectException(str(task))

        if task_obj in self.task_queue:
            self.archived_tasks.remove(task)
            heapq.heapify(self.task_queue)

    @logger.logging_function
    def clean_task_list(self, path_to_db_dir=None, save=False):
        """
        Cleans task list from failed tasks
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        if path_to_db_dir is None:
            path_to_db_dir = self.path_to_db_dir

        del_items = []
        for _time, _id in self.task_queue:
            if not tools.object_is_storing(_id=_id, path_to_db_dir=path_to_db_dir):
                del_items.append((_time, _id))

        for item in del_items:
            self.task_queue.remove(item)

        heapq.heapify(self.task_queue)

        if save:
            self.save()

    @logger.logging_function
    def clean_archived_task_list(self, path_to_archive_dir=None):
        """
        Cleans task list from failed tasks
        :param path_to_archive_dir: path to directory where
        user should look for archived objects
        :return: None
        """

        if path_to_archive_dir is None:
            path_to_archive_dir = self.path_to_archive_dir

        del_items = []
        for item in self.archived_tasks:
            _id = item[1]
            if not tools.object_is_in_archive(_id, path_to_archive_dir=path_to_archive_dir):
                del_items.append(item)

        for item in del_items:
            self.archived_tasks.remove(item)

        heapq.heapify(self.archived_tasks)

    @logger.logging_function
    def has_task(self, task, path_to_db_dir=None):
        """
        Indicates if user has task with such identifier
        :param path_to_db_dir: path to directory where user should look for objects
        :param task: identifier of task. May have type str (id), tuple (datetime, id) or Task object
        :return: True if uer has such task
        """

        task_id = None

        if isinstance(task, str):
            task_id = task
            for _time, _id in self.task_queue:
                if _id == task:
                    return True

        if isinstance(task, tuple):
            task_id = task[1]
            if task in self.aliases:
                task = self.aliases[task]
            if task in self.task_queue:
                return True

        if isinstance(task, Task):
            task_id = task.id
            if (task.finish_time, task.id) in self.task_queue:
                return True

        if task_id is None:
            return False

        task_obj = tools.get_object_by_id(
            _id=task_id,
            path_to_db_dir=path_to_db_dir
        )
        if task_obj is not None:
            for task_group in task_obj.groups:
                for user_group in self.groups:
                    if task_group == user_group:
                        return True
        return False

    @logger.logging_function
    def has_tag(self, tagname):
        """
        Returns True if user has such tag attached to any of it\'s archived or active tasks
        :param tagname: name of tag
        :return: True if user has such tag attached to any of
        it\'s archived or active tasks else False
        """

        if not hasattr(self, 'tags'):
            self.tags = {}
        assert isinstance(tagname, str), 'Tag name must have type str!'
        return tagname in self.tags

    @logger.logging_function
    def del_group_from_list(self, group_name, path_to_db_dir=None, save=False):
        """
        Deletes group with defined name from list of users groups
        :param path_to_db_dir: path to directory where user should look for objects
        :param group_name: name of target group
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        if not hasattr(self, 'groups'):
            self.groups = []

        if not group_name.startswith('Group_'):
            group_id = 'Group_{}'.format(group_name)
        else:
            group_id = group_name

        if group_id in self.groups:
            self.groups.remove(group_id)

        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def has_group(self, group_name):
        """
        Returns true if the user has such group
        :param group_name: id or name of group (str)
        :return: True if the user has such group else false
        """

        assert isinstance(group_name, str), 'Group id or name must have type str.'
        group_id = group_name
        if not group_name.startswith('Group_'):
            group_id = 'Group_{}'.format(group_name)
        return group_id in self.groups

    @database_manager.removing_object
    @logger.logging_function
    def delete(self, path_to_db_dir=None):
        """
        Deletes user with deleting all its groups and tasks
        :return: None
        """

        for _time, _id in self.task_queue:
            if database_manager.object_exists_in_db(_id):
                task_obj = tools.get_object_by_id(
                    _id=_id,
                    path_to_db_dir=path_to_db_dir
                )
                task_obj.delete(path_to_db_dir=path_to_db_dir)

    @database_manager.saving_changes
    @logger.logging_function
    def save(self, path_to_db_dir=None):
        """
        Pushes object into DB
        :return: None
        """
        self.path_to_db_dir = path_to_db_dir
        for (_time, _id) in self.task_queue:
            task_obj = tools.get_object_by_id(_id=_id, path_to_db_dir=path_to_db_dir)
            if isinstance(task_obj, Task):
                task_obj.save(path_to_db_dir=path_to_db_dir)

