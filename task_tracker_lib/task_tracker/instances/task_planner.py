"""
This module contains class TaskPlanner that can produce objects
that set the rules for creating periodic tasks.
"""
from datetime import datetime

from dateutil.relativedelta import relativedelta
from task_tracker import database_manager
from task_tracker import datetime_parser
from task_tracker import logger
from task_tracker.instances import base_id_object


class TaskPlanner(base_id_object.BaseIdObject):
    """
    The main function of its instanses is to set the rles for creating new tasks.
    They produce tasks with defined period and simple arguments.
    For example: if you want to create periodic task Jogging
    that would be appearing every day you should use this class
    """
    @logger.logging_function
    def __init__(
            self,
            initial_moment=datetime.now(),
            period_time=relativedelta(days=1),
            task_msg='Default Periodic task MSG!',
            working_period=relativedelta(days=1),
            task_priority=1,
            path_to_db_dir=None,
            path_to_archive_dir=None,
            save=False):

        """
        Creates planner that sets the rules for periodic tasks
        :param initial_moment: The moment when it starts working (datetime)
        :param period_time: The period of time by which the task will be created (timedelta)
        :param task_msg: message for task (str)
        :param working_period: The period by which the task will be available (timedelta)
        :param task_priority: priority of task (int)
        :param save: boolean. Indicates to push changes into DB
        """

        if isinstance(initial_moment, str):
            initial_moment = datetime_parser.parse_str_to_datetime(initial_moment)
        assert isinstance(initial_moment, datetime), 'Invalid data was passed for argument task'
        assert isinstance(period_time, relativedelta), 'Time period must have type relativedelta'
        assert isinstance(working_period, relativedelta), 'Working period must have relativedelta'
        assert isinstance(task_msg, str), 'Task message must have tpe str'
        base_id_object.BaseIdObject.__init__(self,
                                             path_to_db_dir=path_to_db_dir,
                                             path_to_archive_dir=path_to_archive_dir)
        self.initial_moment = initial_moment
        self.period_time = period_time
        self.task_msg = task_msg
        self.working_period = working_period
        self.task_priority = task_priority
        self.init_tuple = (
            self.initial_moment,
            self.period_time,
            self.task_msg,
            self.working_period,
            self.task_priority,
            self.id
        )
        if save:
            self.save(path_to_db_dir=path_to_db_dir)

    @logger.logging_function
    def attach_to_new_user(self, user_id, save=False):
        """
        Adds this planner to new user (may be useless)
        :param user_id: id of target user
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        assert isinstance(user_id, str), 'User id must have type str'
        assert database_manager.object_exists_in_db(user_id), \
            'User_id in must belong to existing user'
        if save:
            self.save()

    @staticmethod
    @logger.logging_function
    def get_next_creation_time(init_moment, appearing_period, working_period, last_time_created):
        """
        Supporting method that we need to get the next moment the task will be created.
        Returns expected time since when the task will be activated.
        :param init_moment: datetime
        :param appearing_period: timedelta
        :param working_period: timedelta
        :param last_time_created: datetime
        :return: datetime object
        """

        next_moment = init_moment
        while next_moment < datetime.now():
            next_moment += appearing_period
        return next_moment

    @staticmethod
    @logger.logging_function
    def get_possible_creation_time(init_moment, appearing_period, working_period):
        """
        Returns the possible moment when the task can be created.
        The moment should be chosen so, that there will be a chance to finish task.
        If there is no such moment returns None.
        :param init_moment: the initalization time of plan
        :param appearing_period: the period by which the task is creating (relativedelta object)
        :param working_period: the period of time, by which the created task is active
        :return: he possible moment of time or None
        """

        creation_moment = init_moment
        while creation_moment + appearing_period < datetime.now():
            creation_moment += appearing_period
        return creation_moment if creation_moment + working_period > datetime.now() else None

    @staticmethod
    @logger.logging_function
    def get_planner_object(obj_id, path_to_db_dir):
        """
        Probably useless, but I will delete it later
        :param path_to_db_dir: shos where to look for the planner object
        :param obj_id: pass
        :return: pass
        """

        return database_manager.get_object_from_db_(obj_id, path_to_db_dir=path_to_db_dir)

    @database_manager.saving_changes
    @logger.logging_function
    def save(self, path_to_db_dir=None):
        self.path_to_db_dir = path_to_db_dir
        """
        Saves object into db
        :return: None
        """

    @database_manager.removing_object
    @logger.logging_function
    def remove(self, path_to_db_dir=None):
        self.path_to_db_dir = path_to_db_dir
        """
        Deletes object from DB
        :return: None
        """
