"""
This module contains UserManager class that implements the main logic of my project.

UserManager is the main instance, which operartes all other instances,
i.e. that manages tasks, adds tags to tasks, makes reminds for tasks and creates groups.

UserManager object is strongly synchronized with DB. It has it's own pathes to storage files,
to which it saves or loads objects.
That means that object will saved after completion of any action.

Most of the methods contain the keyword save parameter,
which means that the object will be pushed into db after completion of this method.
If save parameter is set to False, nothing would be pushed into db.

To create object of this instance just type: usr_mgr = UserManger(path_to_db_dir='pathtodb',
                                                            path_to_archive_dir='pathtoarchive')
UserManger is the main class of this project, so all the program logic goes through this class.

There're lots of methods in this class, so if you don't understand something,
type help(method) to see more information.

"""
import datetime
import heapq

from task_tracker import database_manager
from task_tracker import datetime_parser
from task_tracker import logger
from task_tracker import tools
from task_tracker.exceptions import (AccessToUnExistingObjectException,
                                     WrongDataException,
                                     PermissionDeniedException)
from task_tracker.instances import base_id_object
from task_tracker.instances.group import Group
from task_tracker.instances.remind import Remind
from task_tracker.instances.tag import Tag
from task_tracker.instances.task import Task
from task_tracker.instances.user import User
from task_tracker.instances.task_planner import TaskPlanner
from dateutil.relativedelta import relativedelta


class UserManager(base_id_object.BaseIdObject):
    """
    This class implements the main logic of this project.
    It performs the main actions with users, tasks, tags, groups.
    It connects all the logic together.
    """

    @logger.logging_function
    def __init__(self, path_to_db_dir=None, path_to_archive_dir=None):
        """
        Creates manager object, linking it with storage directories.
        :param path_to_db_dir: path to directory where this file will be saved.
        :param path_to_archive_dir: path to directory where this file can be archived.
        """

        base_id_object.BaseIdObject.__init__(self,
                                             path_to_db_dir=path_to_db_dir,
                                             path_to_archive_dir=path_to_archive_dir)

    @logger.logging_function
    def add_alias(self, user, alias, value, save=False):
        """
        Adds alias of any task the user had set.
        :param user: user object to which alias will be added
        :param alias: str
        :param value: str
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        assert isinstance(alias, str), 'Alias must have type str.'
        assert isinstance(value, str), 'Value must have type str.'
        assert database_manager.object_exists_in_db(value, path_to_db_dir=self.path_to_db_dir), \
            'Adding alias to unexisting task is not permitted.'

        user_obj.aliases[alias] = value

        if save:
            user_obj.save(path_to_db_dir=self.path_to_db_dir)

    @logger.logging_function
    def del_alias(self, user, val, is_val=False, save=False):
        """
        Deletes alias
        :param user: user object from which alias will be deleted
        :param val: alias (str)
        :param is_val: if this parameter is passed than we search
        for this value in aliases and delete it with it\'s key
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        assert isinstance(val, str), 'Alias must have type str.'
        if is_val:
            list_keys = []
            for key in user_obj.aliases:
                if user_obj.aliases[key] == val:
                    list_keys.append(key)
            for key in list_keys:
                del user_obj.aliases[key]
        else:
            del user_obj.aliases[val]

        if save:
            user_obj.save(path_to_db_dir=self.path_to_db_dir)

    # Task logic

    @logger.logging_function
    def get_user_object_from_db(self, user_identifier):
        """
        It's a commmon method that returns task object (instance of Task) from given identifier.
        Task identifier can be id, tuple (finish_time, id) or task object
        (in that case task identifier is returned)
        :param user_identifier: str or User object
        :return: task object
        """

        return_value = None
        if isinstance(user_identifier, User):
            return_value = user_identifier
        if isinstance(user_identifier, str):
            if not user_identifier.startswith('User_'):
                user_identifier = 'User_' + user_identifier
            return_value = tools.get_object_by_id(
                _id=user_identifier,
                path_to_db_dir=self.path_to_db_dir
            )
        return return_value

    @logger.logging_function
    def get_task_object_from_db(self, task_identifier):
        """
        It's a commmon method that returns task object (instance of Task) from given identifier.
        Task identifier can be id, tuple (finish_time, id) or task object
        (in that case task identifier is returned)
        :param task_identifier: str, tuple(finish_time, id) or task object
        :return: task object
        """

        return_value = None
        if isinstance(task_identifier, tuple):
            return_value = tools.get_object_by_id(
                _id=task_identifier[1],
                path_to_db_dir=self.path_to_db_dir
            )
        if isinstance(task_identifier, Task):
            return_value = task_identifier
        if isinstance(task_identifier, str):
            return_value = tools.get_object_by_id(
                _id=task_identifier,
                path_to_db_dir=self.path_to_db_dir
            )

        return return_value

    @logger.logging_function
    def get_task_object_from_archive(self, task_identifier):
        """
        It's a commmon method that returns task object (instance of Task) from given identifier.
        Task identifier can be id, tuple (finish_time, id) or task object
        (in that case task identifier is returned)
        :param task_identifier: str, tuple(finish_time, id) or task object
        :return: task object
        """

        return_value = None
        if isinstance(task_identifier, tuple):
            return_value = tools.get_object_from_archive(
                _id=task_identifier[1],
                path_to_archive_dir=self.path_to_archive_dir
            )
        if isinstance(task_identifier, Task):
            return_value = tools.get_object_from_archive(
                _id=task_identifier.id,
                path_to_archive_dir=self.path_to_archive_dir
            )
        if isinstance(task_identifier, str):
            return_value = tools.get_object_from_archive(
                _id=task_identifier,
                path_to_archive_dir=self.path_to_archive_dir
            )

        return return_value

    @logger.logging_function
    def add_task(self, user, task, save=False):
        """
        Adds task to this user
        :param user: user object to which task will be added
        :param task: Task object (Task) or id (str)
        :param save: save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        task_obj = self.get_task_object_from_db(task)

        if user_obj.has_task(task_obj, path_to_db_dir=self.path_to_db_dir):
            raise WrongDataException('User {0} already has such task'.format(user_obj.id))

        heapq.heappush(user_obj.task_queue, (task_obj.finish_time, task_obj.id))
        task_obj.add_user_owner(user_obj.id)
        task_obj.user_admin_id = user_obj.id

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=task_obj.id,
                obj=task_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def edit_task(
            self,
            user_id,
            task_id,
            new_finish_time=None,
            new_msg=None,
            new_priority=None,
            save=False):
        """
        Edits task with id taskid.
        :param user_id: user object who's task will be edited
        :param task_id: id of task object
        :param new_finish_time: finish_time (deadline) may be datetime
        object or str representation of it
        :param new_msg: message (str)
        :param new_priority: new priority (int)
        :param save: boolean. Indicates to push changes to db
        :return: None
        """
        user_obj = self.get_user_object_from_db(user_id)

        task_obj = self.get_task_object_from_db(task_id)

        if not user_obj.has_task(task_obj):
            raise WrongDataException('User {0} has not got such task'.format(user_obj.id))

        user_obj.del_task_from_task_list(task=task_obj)
        task_obj.edit_task(
            new_finish_time=new_finish_time,
            new_msg=new_msg,
            new_priority=new_priority
        )
        heapq.heappush(user_obj.task_queue, (task_obj.finish_time, task_obj.id))

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=task_obj.id,
                obj=task_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def create_new_task(
            self,
            user,
            finish_time=datetime.datetime.now()+datetime.timedelta(days=1),
            msg='Default message for task.',
            start_time=datetime.datetime.now(),
            users_owners_list=[],
            priority=1,
            save=False
    ):
        """
        Creates new task and attaches it to current user
        :param user: user object to which task will be added
        :param finish_time: (deadline) datetime or str representation of datetime
        :param msg: message
        :param start_time: the time this task is going to appear (has similar type with finish_time)
        :param users_owners_list: contains list of users that have access to this task
        :param priority: priority of this task
        :param save: boolean. Indicates to push changes to db
        :return: id of new task
        """

        user_obj = self.get_user_object_from_db(user)

        for item in users_owners_list:
            if not isinstance(item, str):
                raise WrongDataException('Wrong id list for task {}'.format(user_obj.id))

        if user_obj.id not in users_owners_list:
            users_owners_list.append(user_obj.id)

        users_list = users_owners_list[:]
        task_obj = Task(
            finish_time=finish_time,
            msg=msg,
            users_owners_list=users_list,
            user_admin_id=user_obj.id,
            priority=priority,
            start_time=start_time
        )
        self.add_task(user_obj, task_obj, save=save)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=task_obj.id,
                obj=task_obj,
                path_to_db_dir=self.path_to_db_dir
            )

        return None if task_obj is None else task_obj.id

    @logger.logging_function
    def add_subtask_to_task(self, user, parent_task, son_task, save=False):
        """
        Adds existing task (son_task) as a subtask of existing task (parent_task)
        :param user: user object who's task will be taken
        :param parent_task: id of parent task
        :param son_task: id of son task
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        parent_task_obj = self.get_task_object_from_db(parent_task)
        son_task_obj = self.get_task_object_from_db(son_task)

        if not user_obj.is_admin_of_task(son_task_obj):
            raise PermissionDeniedException(user_obj.id, son_task_obj.id)

        if not user_obj.is_admin_of_task(parent_task_obj):
            raise PermissionDeniedException(user_obj.id, parent_task_obj.id)

        parent_task_obj.child_task_list.append(
            {
                'id': son_task_obj.id,
                'complexity': son_task_obj.total_complexity
            }
        )
        son_task_obj.parent_task_id = parent_task_obj.id

        root_task = parent_task_obj._find_root_task(path_to_db_dir=self.path_to_db_dir)

        if not (son_task_obj.finish_time, son_task_obj.id) in user_obj.task_queue:
            heapq.heappush(user_obj.task_queue, (son_task_obj.finish_time, son_task_obj.id))

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=parent_task_obj.id,
                obj=parent_task_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=son_task_obj.id,
                obj=son_task_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            root_task.recalculate_readiness(
                path_to_db_dir=self.path_to_db_dir,
                path_to_archive_dir=self.path_to_archive_dir,
                save=save
            )

    @logger.logging_function
    def deattach_subtask_from_task(self, user, parent_task, son_task, save=False):
        """
        Adds existing task (son_task) as a subtask of existing task (parent_task)
        :param user: user object who's task will be taken
        :param parent_task: id of parent task
        :param son_task: id of son task
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        parent_task_obj = self.get_task_object_from_db(parent_task)
        son_task_obj = self.get_task_object_from_db(son_task)

        if not user_obj.is_admin_of_task(son_task_obj):
            raise PermissionDeniedException(user_obj.id, son_task_obj.id)

        if not user_obj.is_admin_of_task(parent_task_obj):
            raise PermissionDeniedException(user_obj.id, parent_task_obj.id)

        parent_task_obj.deattach_child_task(son_task_obj, path_to_db_dir=self.path_to_db_dir, save=save)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=parent_task_obj.id,
                obj=parent_task_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=son_task_obj.id,
                obj=son_task_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def add_new_subtask_to_task(
            self,
            user,
            parent_task,
            finish_time,
            msg,
            start_time=datetime.datetime.now(),
            priority=1,
            save=False,
            *args,
            **kwargs
    ):
        """
        Creates new task and attaches it to existing parent_task as a subtask
        :param user: user object or id (action performer)
        :param parent_task: id of parent task
        :param finish_time: (deadline) datetime or str representation of datetime
        :param msg: message
        :param start_time: the time this task is going to appear (has similar type with finish_time)
        :param priority: priority of this task
        :param save: boolean. Indicates to push changes to db
        :param args:
        :param kwargs:
        :return: id of newly created subtask
        """

        user_obj = self.get_user_object_from_db(user)
        parent_task_obj = self.get_task_object_from_db(parent_task)

        if not user_obj.is_admin_of_task(parent_task_obj):
            raise PermissionDeniedException(user_obj.id, parent_task_obj.id)

        son_task_obj = Task(
            finish_time=finish_time,
            msg=msg,
            start_time=start_time,
            user_admin_id=user_obj.id,
            priority=priority,
            *args,
            **kwargs
        )

        self.add_subtask_to_task(
            user=user_obj,
            parent_task=parent_task_obj,
            son_task=son_task_obj,
            save=save
        )

        return son_task_obj.id

    @logger.logging_function
    def finish_task(self, user, task, save=False, archive=False):
        """
        Finishes task and optionally removes it to archive
        :param user: user object or id (action performer)
        :param task: identifier of task - str (id) or tuple(datetime, id)
        :param save: boolean. Indicates to push changes to db
        :param archive: boolean. If this option is True task will
        be moved to archive else it will be dropped without saving
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        task_obj = self.get_task_object_from_db(task)

        if not user_obj.has_task(task_obj.id, path_to_db_dir=self.path_to_db_dir):
            raise PermissionDeniedException(user_obj.id, task_obj.id)

        if task_obj is not None:
            user_obj.del_task_from_task_list(task_obj)
            task_obj.finish_task(
                user_successor=user_obj.name,
                save=save,
                archive=archive,
                path_to_db_dir=self.path_to_db_dir,
                path_to_archive_dir=self.path_to_archive_dir
            )

    @logger.logging_function
    def del_task(self, user, task, save=False):
        """
        Deletes task without archiving
        :param user: user object or id (action performer)
        :param task: identifier of task - has type str (id) or tuple (datetime, id)
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        task_obj = self.get_task_object_from_db(task)

        user_obj.del_task_from_task_list(task, path_to_db_dir=self.path_to_db_dir)

        if task_obj is not None:
            if (task_obj.finish_time, task_obj.id) in user_obj.task_queue:
                user_obj.task_queue.remove((task_obj.finish_time, task_obj.id))
                heapq.heapify(user_obj.task_queue)

            if task_obj.id in user_obj.aliases.values():
                self.del_alias(user_obj, task_obj.id, is_val=True)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            if task_obj is not None:
                database_manager.push_object_to_db_(
                    obj_id=task_obj.id,
                    obj=task_obj,
                    path_to_db_dir=self.path_to_db_dir
                )

            if task_obj is not None:
                root_task = task_obj._find_root_task(path_to_db_dir=self.path_to_db_dir)

                task_obj.delete(path_to_db_dir=self.path_to_db_dir)

                root_task.recalculate_readiness(
                    path_to_db_dir=self.path_to_db_dir,
                    path_to_archive_dir=self.path_to_archive_dir,
                    save=save
                )

    @logger.logging_function
    def del_archived_task(self, user, taskid):
        """
        Deletes task from archive
        :param user: user object or id (action performer)
        :param taskid: identifier of task. May have type str (id),
        tuple (datetime, id) or Task object
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        if isinstance(taskid, tuple):
            taskid = taskid[1]

        item_to_remove = None

        for i in range(len(user_obj.archived_tasks)):
            item = user_obj.archived_tasks[i]
            _id = item[1]
            if _id == taskid:
                item_to_remove = item

        if item_to_remove is None:
            return

        user_obj.archived_tasks.remove(item_to_remove)
        tools.del_object_from_archive(taskid, path_to_archive_dir=self.path_to_archive_dir)
        database_manager.push_object_to_db_(
            obj_id=user_obj.id,
            obj=user_obj,
            path_to_db_dir=self.path_to_db_dir
        )

    @logger.logging_function
    def archive_task(self, user, task):
        """
        Moves task task into archive and marks it as archived.
        Doesn't work if user doesn't have such task
        :param user: user object or id (action performer)
        :param task: identifier of task. May have type str (id), tuple (datetime, id) or Task object
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        task_obj = self.get_task_object_from_db(task)

        if not (task_obj.finish_time, task_obj.id) in user_obj.archived_tasks:
            user_obj.task_queue.remove((task_obj.finish_time, task_obj.id))
            user_obj.archived_tasks.append((task_obj.finish_time, task_obj.id))
            heapq.heapify(user_obj.task_queue)

        if task_obj is not None:
            task_obj.archive(
                path_to_db_dir=self.path_to_db_dir,
                path_to_archive_dir=self.path_to_archive_dir
            )

        database_manager.push_object_to_db_(
            obj_id=user_obj.id,
            obj=user_obj,
            path_to_db_dir=self.path_to_db_dir
        )

    @logger.logging_function
    def fail_task(self, user, task, archive=False, save=False):
        """
        Fails task with identifier task
        :param user: user object or id (action performer)
        :param task: identifier of task. May have type str (id),
        tuple (datetime, id) or Task object
        :param save: boolean. Indicates to push changes to db
        :param archive: boolean. If this option is True task will be
        moved to archive else it will be dropped without saving
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        task_obj = self.get_task_object_from_db(task)

        main_task = task_obj._find_root_task(path_to_db_dir=self.path_to_db_dir)
        user_obj.del_task_from_task_list(main_task, path_to_db_dir=self.path_to_db_dir)

        connected_tasks = main_task.get_subtree_list([], path_to_db_dir=self.path_to_db_dir)

        task_obj.fail_task(
            path_to_db_dir=self.path_to_db_dir,
            path_to_archive_dir=self.path_to_archive_dir,
            archive=archive,
            save=save
        )

        for item in connected_tasks:
            if item in user_obj.task_queue:
                user_obj.task_queue.remove(item)
            if archive:
                if item not in user_obj.archived_tasks:
                    user_obj.archived_tasks.append(item)

        user_obj.clean_task_list(path_to_db_dir=self.path_to_db_dir)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def delete_all_tasks(self, user, save=False):
        """
        Deletes all task tha user had set
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        while user_obj.task_queue:
            self.del_task(user=user_obj, task=user_obj.task_queue[-1], save=save)
            user_obj = self.get_user_object_from_db(user)

    @logger.logging_function
    def delete_all_archived_tasks(self, user):
        """
        Deletes all tasks of current user that are stored in the archive.
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        user_obj.clean_archived_task_list(path_to_archive_dir=self.path_to_archive_dir)
        for _time, _id in user_obj.archived_tasks[:]:
            self.del_archived_task(user=user_obj, taskid=_id)

    @logger.logging_function
    def archive_all_tasks(self, user):
        """
        Moves all current active users tasks to the archive
        :param user: user object or id (action performer)
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        while user_obj.task_queue:
            self.archive_task(user=user_obj, task=user_obj.task_queue[-1])
            user_obj = self.get_user_object_from_db(user)

    @logger.logging_function
    def get_tasks_in_time_range(self, user, a, b):
        """
        Returns list of tasks that end up in particular period of time
        :param user: user object or id (action performer)
        :param a: lower bound of period. May have type datetime or str representation of datetime
        :param b: upper bound of period. May have type datetime or str representation of datetime
        :return: list of Task objects
        """

        user_obj = self.get_user_object_from_db(user)

        if isinstance(a, str):
            a = datetime_parser.parse_str_to_datetime(a)
        if isinstance(b, str):
            b = datetime_parser.parse_str_to_datetime(b)
        arr = []
        for i in range(len(user_obj.task_queue)):
            if a <= user_obj.task_queue[i][0] <= b:
                arr.append(user_obj.task_queue[i])
        arr.sort()
        return [tools.get_object_by_id(
            _id=_id,
            path_to_db_dir=self.path_to_db_dir) for _time, _id in arr]

    @logger.logging_function
    def get_n_nearest_tasks(self, user):
        """
        Returns n closest tasks
        :param user: user object or id (action performer)
        :return: n closest tasks
        """

        user_obj = self.get_user_object_from_db(user)
        heapq.heapify(user_obj.task_queue)
        return heapq.nlargest(len(user_obj.task_queue), user_obj.task_queue)

    @logger.logging_function
    def drop_passed_tasks(self, user, save=False, archive=False):
        """
        Deletes tasks with passed finish_time and moves it
        into the archive (if archive option is set to True)
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes to DB
        :param archive: boolean. If this option is True task will
        be moved to archive else it will be dropped without saving
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        while user_obj.has_failed_tasks():
            for _time, _id in user_obj.task_queue:
                if _time < datetime.datetime.now():
                    self.fail_task(user_obj, _id, archive=archive, save=save)
                    break

    @logger.logging_function
    def update_task_list(self, user, save=False, archive=False):
        """
        Updates users list of tasks by performing cleaning
        task list and achive list from unexisting tasks,
        creating planned tasks and dropping failed tasks
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes to DB
        :param archive: boolean. If this option is True failed tasks
        will be moved to archive else they will be dropped without saving
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        user_obj.clean_task_list(path_to_db_dir=self.path_to_db_dir)
        self.drop_passed_tasks(user_obj, save=save, archive=archive)
        self.create_planned_tasks(user=user, save=save)
        user_obj.clean_archived_task_list(path_to_archive_dir=self.path_to_archive_dir)

        for _time, _id in user_obj.task_queue:
            task_obj = tools.get_object_by_id(_id=_id, path_to_db_dir=self.path_to_db_dir)
            if task_obj.start_time < datetime.datetime.now():
                task_obj.activate_task()
                database_manager.push_object_to_db_(
                    obj_id=task_obj.id,
                    obj=task_obj,
                    path_to_db_dir=self.path_to_db_dir
                )

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def update_task(self, user, task_id, save=False, archive=False):
        """
        Updates single task.
        :param user: user object or id (action performer)
        :param task_id: id of task (str)
        :param save: boolean. Indicates t push changes to DB
        :param archive: if this option is set to True,
        failed or finished task will e moved to archive
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        assert isinstance(task_id, str), 'Task id must be str'
        if not tools.object_is_storing(_id=task_id, path_to_db_dir=self.path_to_db_dir):
            raise WrongDataException('Task object doesn\'t exist')
        if not user_obj.has_task(task=task_id):
            raise PermissionDeniedException(user_obj.id, task_id)

        task_obj = tools.get_object_by_id(_id=task_id, path_to_db_dir=self.path_to_db_dir)
        if task_obj.finish_time < datetime.datetime.now():
            self.fail_task(user, task_id, archive=archive, save=save)
        elif task_obj.start_time < datetime.datetime.now():
            task_obj.activate_task(path_to_db_dir=self.path_to_db_dir)
            self.save_object(task_obj)

    # Tag logic

    @logger.logging_function
    def add_tag_to_task(self, user, task, tag, save=False):  # TODO: Make user tag list
        """
        Adds tag with tagname to task
        :param user: user object or id (action performer)
        :param task: identifier of task. May have type str (id), tuple (datetime, id) or Task object
        :param tag: tagname (must have type str)
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        task_obj = self.get_task_object_from_db(task)

        if isinstance(tag, str):
            tag = Tag(tag, user_obj.id)
            if tag.name not in user_obj.tags:
                user_obj.tags[tag.name] = (1, tag.id)
            else:
                user_obj.tags[tag.name] = (user_obj.tags[tag.name][0]+1, tag.id)

        assert isinstance(tag, Tag), \
            'Wrong data was passed for argument tag instead of Tag or tag_name.'

        tag.add_tag_to_task(task_obj, path_to_db_dir=self.path_to_db_dir, save=save)

        if save:
            self.save_object(tag)
            self.save_object(user_obj)

    @logger.logging_function
    def get_tag_object(self, user, tagname):
        """
        Returns Tag object with name tagname. Each Tag has it\'s user owner
        :param user: user object or id (action performer)
        :param tagname: str
        :return: Tag object
        """

        user_obj = self.get_user_object_from_db(user)

        assert isinstance(tagname, str), 'Tag name must have type str'

        if tagname not in user_obj.tags:
            raise WrongDataException('User {0} hasn\'t got such tag.'.format(user_obj.name, tagname))
        return Tag(tagname,
                   user_obj.id,
                   path_to_db_dir=self.path_to_db_dir,
                   path_to_archive_dir=self.path_to_archive_dir)

    @logger.logging_function
    def del_tag_from_task(self, user, task, tag, save=False):
        """
        Removes tag from single task
        :param user: user object or id (action performer)
        :param task: task identifier. May have type str (id) tuple (datetime, id) or task object
        :param tag:
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)
        task_obj = self.get_task_object_from_db(task)

        if isinstance(tag, str):
            tag = self.get_tag_object(user=user_obj, tagname=tag)
        assert isinstance(tag, Tag), \
            'Wrong data was passed for argument tag instead of Tag or tag_name'

        tag.remove_tag_from_task(task_obj, path_to_db_dir=self.path_to_db_dir)

        user_obj.tags[tag.name] = (user_obj.tags[tag.name][0]-1, tag.id)
        if not user_obj.tags[tag.name][0]:
            del user_obj.tags[tag.name]
            tag.remove(path_to_db_dir=self.path_to_db_dir)
        if save:
            self.save_object(user_obj)
            self.save_object(task_obj)
            if database_manager.object_exists_in_db(obj_id=tag.id, path_to_db_dir=self.path_to_db_dir):
                self.save_object(tag)

    @logger.logging_function
    def edit_tag_description(self, user, tagname, new_tag_description, save=False):
        """
        Edit tag description option. Optionally saves it into db.
        :param user: user object or id (action performer)
        :param tagname: name of tag
        :param new_tag_description: new description
        :param save: boolean. Indicates to push changes into DB.
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        tag = Tag(tagname,
                  user_obj.id,
                  path_to_db_dir=self.path_to_db_dir,
                  path_to_archive_dir=self.path_to_archive_dir)
        tag.change_description(new_tag_description)

        if save:
            database_manager.push_object_to_db_(
                obj_id=tag.id,
                obj=tag,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def remove_tag(self, user, tag_name, save=False):
        """
        Removes tag and removes tag from all the tasks that contain it
        :param user: user object or id (action performer)
        :param tag_name: str object
        :param save: boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        if not user_obj.has_tag(tag_name):
            raise WrongDataException('User {} doesn\'t have such tag!'.format(user_obj.name))

        tag = Tag(tag_name=tag_name,
                  user_owner_id=user_obj.id,
                  path_to_db_dir=self.path_to_db_dir,
                  path_to_archive_dir=self.path_to_archive_dir)

        for _time, _id in user_obj.task_queue:
            task_obj = tools.get_object_by_id(_id=_id, path_to_db_dir=self.path_to_db_dir)
            task_obj.del_tag(tag.name, path_to_db_dir=self.path_to_db_dir, save=save)
            if save:
                database_manager.push_object_to_db_(
                    obj_id=task_obj.id,
                    obj=task_obj,
                    path_to_db_dir=self.path_to_db_dir
                )

        del user_obj.tags[tag.name]

        if save:
            tag.remove(path_to_db_dir=self.path_to_db_dir)
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def remove_all_tags(self, user, save=False):
        """
        Removes all tags that user has set before.
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes int db.
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        for tagname in user_obj.tags.copy():
            self.remove_tag(user, tagname, save=save)

    # Plan logic

    @logger.logging_function
    def add_periodic_task(self, user, planner, save=False):
        """
        Adds task planner to users planner_list
        :param user: user object or id (action performer)
        :param planner: TaskPlanner object or id of TaskPlanner object
        :param save:boolean. Indicates to push changes to db
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        if isinstance(planner, str):
            planner = TaskPlanner.get_planner_object(planner, path_to_db_dir=self.path_to_db_dir)
        assert isinstance(planner, TaskPlanner), 'Planner argument must be id or TaskPlanner object'
        user_obj.planner_list.append(
            planner.init_tuple+(datetime.datetime.now()-datetime.timedelta(seconds=100000000),)
        )
        if save:
            self.save_object(user_obj)
            self.save_object(planner)

    @logger.logging_function
    def create_periodic_task(
            self,
            user,
            msg,
            initial_moment=datetime.datetime.now(),
            working_period=relativedelta(days=1),
            appearing_period=relativedelta(days=1),
            priority=1,
            save=False
    ):
        """
        Creates new TaskPlanner object and adds it to users planner_list.
        This object will be creating new Task object each time the appearing_period is passed
        :param user: user object or id (action performer)
        :param msg: message of periodic task
        :param initial_moment: time moment when the planer will start working
        :param working_period: Period that the task will be available
        :param appearing_period: The period by which the task will be created
        :param priority: priority of task
        :param save: boolean. Indicates to push changes to db
        :return: Id of task planner
        """

        planner = TaskPlanner(
            initial_moment=initial_moment,
            period_time=appearing_period,
            task_msg=msg,
            working_period=working_period,
            task_priority=priority,
            save=save,
            path_to_db_dir=self.path_to_db_dir,
            path_to_archive_dir=self.path_to_archive_dir
        )
        self.add_periodic_task(user, planner, save=save)
        return planner.id

    @logger.logging_function
    def delete_periodic_task(
            self,
            user,
            periodic_task_id,
            save=False
    ):
        """
        Deletes periodic task with id periodic_task_id
        :param user: user object or id (action performer)
        :param periodic_task_id: str
        :param save: boolean. Indicates to push changes to DB
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        for item in user_obj.planner_list:
            if item[-2] == periodic_task_id:
                user_obj.planner_list.remove(item)
                planner = tools.get_object_by_id(
                    _id=periodic_task_id,
                    path_to_db_dir=self.path_to_db_dir
                )
                if planner is not None:
                    database_manager.del_object_in_db_(
                        obj_id=planner.id,
                        path_to_db_dir=self.path_to_db_dir
                    )
                break
        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def create_planned_tasks(self, user, save=False):
        """
        Creates tasks that are in planner list
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes to DB
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        for i in range(len(user_obj.planner_list)):
            init_tuple = user_obj.planner_list[i]

            (
                initial_moment,
                appearing_period,
                msg,
                working_period,
                priority, _id,
                last_time_created
            ) = init_tuple

            if initial_moment > datetime.datetime.now():
                continue

            creation_moment = TaskPlanner.get_possible_creation_time(
                init_moment=initial_moment,
                appearing_period=appearing_period,
                working_period=working_period
            )

            if creation_moment is not None:
                self.create_new_task(
                    user=user_obj,
                    start_time=creation_moment,
                    finish_time=creation_moment + working_period,
                    msg=msg,
                    users_owners_list=[self.id],
                    priority=priority,
                    save=save
                )
                user_obj.planner_list[i] = init_tuple[:-1] + (datetime.datetime.now(),)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def delete_all_periodic_tasks(self, user, save=False):
        """
        Deletes all plans that produce tasks.
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes into db.
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        while user_obj.planner_list:
            database_manager.del_object_in_db_(
                obj_id=user_obj.planner_list[-1][-2],
                path_to_db_dir=self.path_to_db_dir
            )
            del user_obj.planner_list[-1]

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def get_planner_by_id(self, user, planner_id):
        """
        returns TaskPlanner object by it's id.
        :param user: user object or id (action performer)
        Since there is no need to store planners in db, we construct the object manually.
        :param planner_id: id of planner object
        :return: TaskPlanner object
        """

        user_obj = self.get_user_object_from_db(user)

        assert isinstance(planner_id, str), 'Planer id must have type str'
        for item in user.planner_list:
            if item[-2] == planner_id:
                return item
        raise WrongDataException('Wrong planner id was passed!')

    # Remind logic

    @logger.logging_function
    def add_remind(self, user, *args, **kwargs):
        """
        Adds remind to users remind list
        :param user: user object or id (action performer)
        :param args: positional arguments for remind
        :param kwargs: keyword arguments for remind
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        save = 'save' in kwargs.keys()

        if save:
            del kwargs['save']

        remind_tuple = Remind.create_remind(*args, **kwargs)
        heapq.heappush(user_obj.remind_list, remind_tuple)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def get_closest_remind(self, user):
        """
        Returns closest by time remind to any of users tasks.
        If no reminds left than it returns None.
        :param user: user object or id (action performer)
        :return: tuple(datetime, str) or None
        """

        user_obj = self.get_user_object_from_db(user)

        if not user_obj.remind_list:
            return None
        return user_obj.remind_list[0]

    @logger.logging_function
    def del_closest_remind(self, user, save=False):
        """
        Returns closest remind if such exists and deletes it from users remind list
        :param user: user object or id (action performer)
        :return: tuple(datetime, str) or None
        :param save: boolean. Indicates to push changes to DB
        """

        user_obj = self.get_user_object_from_db(user)

        item = None
        if user_obj.remind_list:
            item = heapq.heappop(user_obj.remind_list)
        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )
        return item

    @logger.logging_function
    def drop_reminds_before_moment(self, user, moment=datetime.datetime.now(), save=False):
        """
        Deletes all reminds before defined moment.
        :param user: user object or id (action performer)
        :param moment: time moment (datetime object)
        :param save: save: boolean. Indicates to push changes into db.
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        while user_obj.remind_list and user_obj.remind_list[0][0] < moment:
            heapq.heappop(user_obj.remind_list)
        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def get_reminds_for_task(self, user, task_id):
        """
        Returns list of reminds for task with id task_id
        :param user: user object or id (action performer)
        :param task_id: id of task (str)
        :return: list of tuple(datetime, msg, task_id)
        """

        user_obj = self.get_user_object_from_db(user)

        assert isinstance(task_id, str), 'Task id must have type str'
        assert user_obj.has_task(task_id, path_to_db_dir=self.path_to_db_dir), \
            'User {0} doesn\'t have task {1}'.format(user_obj.id, task_id)

        remind_list = []
        for item in user_obj.remind_list:
            if item[-1] == task_id:
                remind_list.append(item)
        return remind_list

    # Group logic

    @logger.logging_function
    def add_group(self, user, group_name, users_list, task_list, save=False):
        """
        Creates new group, where this user will participate as admin
        :param user: user object or id (action performer)
        :param group_name: name of group (str)
        :param users_list: members of group (list or tuple of id-s)
        :param task_list: tasks for this group (list or tuple of tasks)
        :param save: boolean. Indicates to push changes into DB
        :return: newly created group id
        """

        user_obj = self.get_user_object_from_db(user)

        if user_obj.has_group(group_name):
            raise WrongDataException('Group with such name already exists.')

        users_list = list(users_list)
        task_list = list(task_list)

        for i in range(len(task_list)):
            if task_list[i] in user_obj.aliases:
                task_list[i] = user_obj.aliases[task_list[i]]
            task_id = task_list[i]
            assert isinstance(task_id, str), 'Task id in task list must have type str.'
            assert tools.object_is_storing(
                _id=task_id,
                path_to_db_dir=self.path_to_db_dir), 'Can\'t set unexisting task for group'
            if not user_obj.has_task(task_id, path_to_db_dir=self.path_to_db_dir):
                raise PermissionDeniedException(user_obj.id, task_id)

        if user_obj.id not in users_list:
            users_list.append(user_obj.id)

        for user_id in users_list:
            assert isinstance(user_id, str), 'User id in task list must have type str.'
            assert tools.object_is_storing(
                _id=user_id,
                path_to_db_dir=self.path_to_db_dir), 'Can\'t add unexisting user to group'
            assert user_id.startswith('User_'), 'Wrong user id'

        group_id = 'Group_{}'.format(group_name)
        if database_manager.object_exists_in_db(group_id, path_to_db_dir=self.path_to_db_dir):
            raise WrongDataException('Group with such name already exists!')

        group_obj = Group(
            group_name=group_name,
            admin_id=user_obj.id,
            member_list=users_list,
            task_list=task_list,
            save=save,
            path_to_db_dir=self.path_to_db_dir,
            path_to_archive_dir=self.path_to_archive_dir
        )

        for task_id in task_list:
            task_obj = self.get_task_object_from_db(task_id)
            if group_id not in task_obj.groups:
                task_obj.groups.append(group_id)
                task_obj.save(path_to_db_dir=self.path_to_db_dir)

        for user_id in users_list:
            user_obj = self.get_task_object_from_db(user_id)
            if group_id not in user_obj.groups:
                user_obj.groups.append(group_id)
                user_obj.save(path_to_db_dir=self.path_to_db_dir)

        return group_obj.id

    @logger.logging_function
    def clean_group_list(self, user, save=False):
        """
        Removes unexisting groups
        :param user: user object or id (action performer)
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        del_items = []
        for item in user_obj.groups:
            if not tools.object_is_storing(_id=item, path_to_db_dir=self.path_to_db_dir):
                del_items.append(item)

        for item in del_items:
            user_obj.groups.remove(item)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def del_group(self, user, group_name):
        """
        Deletes group. User must be an admin of this group.
        :param user: user object or id (action performer)
        :param group_name: name of target group
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        group_id = 'Group_{}'.format(group_name)
        if group_id not in user_obj.groups:
            raise WrongDataException('User doesn\'t have such group!')

        group_obj = tools.get_object_by_id(_id=group_id, path_to_db_dir=self.path_to_db_dir)
        assert group_obj is not None, 'No such group'
        assert group_obj.admin_id == user_obj.id, 'You have no rights to delete group'

        for user_id in group_obj.members:
            usr = self.get_user_object_from_db(user_id)
            if usr is not None:
                usr.groups.remove(group_id)
                database_manager.push_object_to_db_(usr.id, usr, path_to_db_dir=self.path_to_db_dir)

        for task_id in group_obj.task_list:
            task_obj = tools.get_object_by_id(task_id, path_to_db_dir=self.path_to_db_dir)
            task_obj.del_group(group_obj.id, path_to_db_dir=self.path_to_db_dir, save=True)

        group_obj.delete(path_to_db_dir=self.path_to_db_dir)

    @logger.logging_function
    def quit_from_group(self, user, group_name, save=False):
        """
        Removes current user from group.
        :param user: user object or id (action performer)
        :param group_name: name of target roup
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        group_id = 'Group_{}'.format(group_name)

        if group_id not in user_obj.groups:
            raise WrongDataException('User doesn\'t have such group!')

        grouo_obj = tools.get_object_by_id(_id=group_id, path_to_db_dir=self.path_to_db_dir)
        grouo_obj.del_user(user_obj.id, path_to_db_dir=self.path_to_db_dir, save=save)

        user_obj.groups.remove(group_id)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def kick_user_from_group(self, user, user_id, group_name, save=False):
        """
        Force deletion of user from group. This option is available for admin
        :param user: user object or id (action performer)
        :param user_id: str
        :param group_name: name of target group
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        if not group_name.startswith('Group_'):
            group_id = 'Group_{}'.format(group_name)
        else:
            group_id = group_name

        if group_id not in user_obj.groups:
            raise WrongDataException('User doesn\'t have such group!')

        group_obj = tools.get_object_by_id(_id=group_id, path_to_db_dir=self.path_to_db_dir)
        if group_obj is None:
            raise AccessToUnExistingObjectException(group_name)
        assert user_obj.id == group_obj.admin_id, 'User has no rights to do this'

        usr = tools.get_object_by_id(_id=user_id, path_to_db_dir=self.path_to_db_dir)

        assert usr is not None, 'No such user'
        self.quit_from_group(user=usr, group_name=group_name, save=save)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def invite_to_group(self, user, user_id, group_name, save=False):
        """
        Invites user with id user_id to current group
        :param user: user object or id (action performer)
        :param user_id: id of user whom you want to invite (str)
        :param group_name: name of target group
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        group_id = 'Group_{}'.format(group_name)
        if group_id not in user_obj.groups:
            raise WrongDataException('User doesn\'t have such group!')

        group_obj = tools.get_object_by_id(_id=group_id, path_to_db_dir=self.path_to_db_dir)
        usr = tools.get_object_by_id(_id=user_id, path_to_db_dir=self.path_to_db_dir)
        if group_obj.has_member(user_id) or user_obj.id == user_id:
            return

        group_obj.add_user(user_id, path_to_db_dir=self.path_to_db_dir, save=save)
        if group_obj.id not in usr.groups:
            usr.groups.append(group_obj.id)

        if save:
            database_manager.push_object_to_db_(
                obj_id=usr.id,
                obj=usr,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def share_task_with_group(self, user, task_id, group_name, save=False):
        """
        Shares task with id task_id between the members of the defined group
        :param user: user object or id (action performer)
        :param task_id: str
        :param group_name: name of target group
        :param save: boolean. Indicates to push changes into DB
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        group_id = 'Group_{}'.format(group_name)

        group_obj = tools.get_object_by_id(_id=group_id, path_to_db_dir=self.path_to_db_dir)
        assert group_obj is not None, 'User doesn\'t have such group'

        task_obj = tools.get_object_by_id(_id=task_id, path_to_db_dir=self.path_to_db_dir)
        assert task_obj is not None and (task_obj.finish_time, task_obj.id) in user_obj.task_queue, \
            'User doesn\'t have such task'

        group_obj.add_task(task_obj.id, path_to_db_dir=self.path_to_db_dir, save=save)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def del_task_from_group(self, user, task_id, group_name, save=False):
        """
        Deletes task with id task_id from group group_name.
        It works only if user is an admin of task.
        :param user: user object or id (action performer)
        :param task_id: id of task (str)
        :param group_name: name of group
        :param save: boolean. Indicates to push changes into db.
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        group_id = 'Group_{}'.format(group_name)
        group_obj = tools.get_object_by_id(_id=group_id, path_to_db_dir=self.path_to_db_dir)
        assert group_obj is not None, 'User doesn\'t have such group'

        task_obj = self.get_task_object_from_db(task_id)

        assert (task_obj.finish_time, task_obj.id) in user_obj.task_queue, \
            'User doesn\'t have such task'

        group_obj.remove_task(task_id, user_obj.id, path_to_db_dir=self.path_to_db_dir, save=save)

        if save:
            database_manager.push_object_to_db_(
                obj_id=user_obj.id,
                obj=user_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    @logger.logging_function
    def get_group(self, user, group_name):
        """
        Returs group by group_name
        :param user: user object or id (action performer)
        :param group_name: id or name of group
        :return: Group object
        """

        user_obj = self.get_user_object_from_db(user)

        group_id = group_name
        if not group_name.startswith('Group_'):
            group_id = 'Group_{}'.format(group_name)

        if not tools.object_is_storing(_id=group_id, path_to_db_dir=self.path_to_db_dir):
            raise WrongDataException('Group {} doesn\'t exist'.format(group_name))
        if not user_obj.has_group(group_name):
            raise PermissionDeniedException(user_obj.id, group_name)

        return Group(group_name,
                     user_obj.id,
                     path_to_db_dir=self.path_to_db_dir,
                     path_to_archive_dir=self.path_to_archive_dir)

    @logger.logging_function
    def share_task_to_group(self, user, task_id, group_name, save=False):
        """
        Makes task available to all members in th group.
        :param user: user object or id (action performer)
        :param task_id: id of task (str)
        :param group_name: name of group (str)
        :param save: boolean. Indicates to push changes into db.
        :return: None
        """

        user_obj = self.get_user_object_from_db(user)

        group_id = 'Group_{}'.format(group_name)
        group_obj = tools.get_object_by_id(_id=group_id, path_to_db_dir=self.path_to_db_dir)
        assert group_obj is not None, 'User doesn\'t have such group'

        task_obj = tools.get_object_by_id(_id=task_id, path_to_db_dir=self.path_to_db_dir)
        assert task_obj is not None and (task_obj.finish_time, task_obj.id) in user_obj.task_queue, \
            'User doesn\'t have such task'

        if group_id not in task_obj.groups:
            task_obj.groups.append(group_id)

        if task_id not in group_obj.task_list:
            group_obj.task_list.append(task_obj.id)

        if save:
            database_manager.push_object_to_db_(
                obj_id=task_obj.id,
                obj=task_obj,
                path_to_db_dir=self.path_to_db_dir
            )
            database_manager.push_object_to_db_(
                obj_id=group_obj.id,
                obj=group_obj,
                path_to_db_dir=self.path_to_db_dir
            )

    def save_object(self, obj):
        obj.save(path_to_db_dir=self.path_to_db_dir)
