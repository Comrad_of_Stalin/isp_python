"""
This file contains exception classes that are used in project
"""
import datetime


class WrongDataException(Exception):

    def __init__(self, txt):
        self.txt = txt

    def __str__(self):
        return self.txt


class InvalidTimeFormatException(Exception):

    def __init__(self, time_obj):
        Exception.__init__(self, time_obj)
        if isinstance(time_obj, datetime.datetime):
            self.txt = 'Invalid time moment: {}'.format(time_obj)
        else:
            self.txt = 'Invalid time format: {}'.format(str(time_obj))

    def __str__(self):
        return self.txt


class InvalidDeltaFormatException(Exception):
    def __init__(self, time_delta_repr):
        Exception.__init__(self, time_delta_repr)
        self.txt = '{} can not be interpreted as a time delta object!'.format(time_delta_repr)

    def __str__(self):
        return self.txt


class DatabaseException(Exception):

    def __init__(self, obj_id):
        self.txt = 'Object with id {} doesn\'t exist in db!'.format(obj_id)

    def __str__(self):
        return self.txt


class PermissionDeniedException(Exception):

    def __init__(self, accessor_id, target_id):
        Exception.__init__(self, (accessor_id, target_id,))
        self.txt = 'Object with id {0} has no rights ' \
                   'to access object with id {1}'.format(accessor_id, target_id)

    def __str__(self):
        return self.txt


class AccessToUnExistingObjectException(Exception):
    def __init__(self, obj_id):
        Exception.__init__(self, obj_id)
        self.txt = 'Object with id {} doesnt exist in the storage!'.format(obj_id)

    def __str__(self):
        return self.txt


class WrongArgumentException(Exception):
    def __init__(self, arg, func_name):
        Exception.__init__(self, arg, func_name)
        self.txt = '{0} is not a valid argument for the function {1}.'.format(arg, func_name)
