"""
This module contains static Logger class.
"""
import datetime
import logging
import os
from functools import wraps

import click
from task_tracker import configuration

LIB_LOGGER_NAME = 'DEFAULT_LOGGER'


def get_logger(config_storage=None, logger_name='task_tracker'):
    """
    This function returns specially prepared logger that will have defined name
    :param config_storage: ConfigStore object that contains configuration settings
    :param logger_name: name of returned logger
    :return: logger (instance of logging.Logger)
    """

    if config_storage is None:
        config_storage = configuration.ConfigStore()

    log_file_path = os.path.join(
        config_storage.get_log_dir(),
        '{}.log'.format(datetime.date.today())
    )

    logger = logging.getLogger(logger_name)

    logger.addHandler(logging.FileHandler(log_file_path))

    return logger


def init_logging(level=logging.NOTSET, config_storage=None):
    """
    Sets the logging level
    :param level: level of logging, as was defined above it can be None, INFO or DEBUG
    :param config_storage: ConfigStore object that contains configuration settings
    :return: None
    """

    logger = get_logger(config_storage=config_storage, logger_name=LIB_LOGGER_NAME)

    logger.setLevel(level=level)

    if level == logging.DEBUG:
        logger.addHandler(logging.StreamHandler())


def log_error(logger, ex_obj, func):
    """
    Using for logging exception
    :param logger: used for logging messages (instance of logging.Logger)
    :param ex_obj: exception object
    :param func: function in which exception occured
    :return: None
    """
    logging_level = logger.getEffectiveLevel()
    if logging_level not in [logging.INFO, logging.DEBUG]:
        return

    stream_handler = logging.StreamHandler()
    logger.addHandler(stream_handler)

    logger.log(msg='Error exception occupied in '
                   'function {0} in module {1}. '
                   'Exception type: {2}.'.format(
                       func.__name__,
                       func.__module__,
                       type(ex_obj)
                   ),
               level=logging_level
              )

    if logging_level == logging.DEBUG:
        logger.error(ex_obj, stack_info=True)
    else:
        logger.error(ex_obj)

    logger.removeHandler(stream_handler)

    logging.disable(logging.ERROR)


def log_execution_message(logger, func, *args):
    """
    Using in debug mode
    :param logger: used for logging messages (instance of logging.Logger)
    :param func: function to debug
    :param args: optional
    :return: None
    """

    logger.log(
        msg='Called {0} with arguments: {1} '
            '\n'.format(
                func.__name__,
                args
            ),
        level=logger.getEffectiveLevel()
    )


def log_any_msg(logger, msg):
    """
    Prints log in file and on screen
    :param logger: used for logging messages (instance of logging.Logger)
    :param msg: message
    :return: None
    """

    logging_level = logger.getEffectiveLevel()
    if logging_level == logging.NOTSET:
        return
    logger.log(msg=msg, level=logging_level)
    if logging_level == logging.INFO:
        logging_level = 'INFO'
    elif logging_level == logging.DEBUG:
        logging_level = 'DEBUG'
    click.echo('[Level {}] '.format(logging_level)+msg)


def log_creation(logger, obj, *args):
    """
    Using for logging __init__ method
    :param logger: used for logging messages (instance of logging.Logger)
    :param obj: self parameter
    :param args: args to init
    :param kwargs: kwargs to init
    :return: None
    """

    logging_level = logger.getEffectiveLevel()
    if logging_level == logging.DEBUG:
        logger.log(
            msg='Created {0} of instance {1} with arguments: {2}.\n'
            .format(
                obj.id,
                type(obj),
                args
            ),
            level=logging_level
        )


def info_execution(func, logger):
    """
    Decorator hat is used for INFO mode. It logs only on depth 1.
    :param logger: used for logging messages (instance of logging.Logger)
    :param func: method or func
    :return: decorated func
    """

    @wraps(func)
    def wrapped(*args, **kwargs):
        """
        Wrapping function that covers func, logging the execution of it with level INFO
        :param args: positional arguments to func.
        :param kwargs: keyword arguments to func.
        :return: has same reurn value as the func has
        """
        try:
            log_execution_message(logger, func, args, kwargs)
            return func(*args, **kwargs)
        except Exception as ex_obj:
            log_error(logger, ex_obj, func)
            raise

    return wrapped


def logging_execution(func, logger):
    """
    Decorator for logging methods
    :param logger: used for logging messages (instance of logging.Logger)
    :param func: method
    :return: decorated method
    """

    @wraps(func)
    def wrapped(*args, **kwargs):
        """
        Wrapping function that covers func, logging the execution of it with level DEBUG
        :param args: positional arguments to func.
        :param kwargs: keyword arguments to func.
        :return: has same reurn value as the func has
        """
        try:
            log_execution_message(logger, func, args, kwargs)

            ret = func(*args, **kwargs)
        except Exception as ex_obj:
            log_error(logger, ex_obj, func)
            raise
        return ret
    return wrapped


def logging_function(func):
    """
    Common decorator for logging
    :param func: function
    :return: decorated function
    """

    @wraps(func)
    def wrapped(*args, **kwargs):
        """
        Wrapping function that chooses the level of logging, depending on the provided settings
        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: the same value as the func returns
        """

        logger = logging.getLogger(LIB_LOGGER_NAME)
        logging_level = logger.getEffectiveLevel()

        wrapped_func = func

        if logging_level == logging.INFO:
            wrapped_func = info_execution(func, logger)
        elif logging_level == logging.DEBUG:
            wrapped_func = logging_execution(func, logger)
        try:
            ret_val = wrapped_func(*args, **kwargs)
            return ret_val
        except Exception as ex_obj:
            log_error(logger, ex_obj, func)
            raise

    return wrapped


def set_logging_level(level, config_storage=None):
    """
    Initializes logging level for class MyLogger
    :param level: str or None
    :param config_storage: ConfigStore object that contains configuration settings
    :return: None
    """

    if level is None:
        init_logging(logging.NOTSET, config_storage=config_storage)
    elif level == 'INFO':
        init_logging(logging.INFO, config_storage=config_storage)
    elif level == 'DEBUG':
        init_logging(logging.DEBUG, config_storage=config_storage)
    else:
        init_logging(logging.NOTSET, config_storage=config_storage)
        raise ValueError('Invalid logging level has been passed!')
    logger = logging.getLogger(LIB_LOGGER_NAME)
    log_any_msg(logger, 'The logging level is set to {0}\n'.format(level))
