"""
This module works with configuration files.
It contains two main classes.
The first one is DefaultConfig which objects contain default properties for the lib.
The second one is ConfigStore that connects to config.ini file before each action.
When the library is used, the default folder ~/.task_tracker_conf is created,
where the most common files lie.
"""
import configparser
import getpass
import json
import os
from sys import platform

from task_tracker.exceptions import WrongDataException


class Globals:
    """
    Class Globals contain several constants,
    that we need to operate with config file.
    """
    DEFAULT_OPTIONS_SECTION = 'DEFAULT_OPTIONS'
    CURRENT_OPTIONS_SECTION = 'CURRENT_OPTIONS'
    user_defined_directories = ('db_dir', 'archive_dir', 'log_dir')
    default_config_options = {
        'port': '50007',
        'archive_dir': 'archive_files',
        'db_dir': 'db_files',
        'log_dir': 'log',
        'host': 'localhost',
        'current_user_id': '',
        'config_dir': '.task_tracker_conf',
        'config_file': 'config.ini',
        'main_db_file': 'main_db.json'
    }


class DefaultConfig:
    """
    This class contains base default settings, defined by the program.
    But if there are already default settings in config file, it, doesn't change them
    """

    def __init__(self, path_to_config_dir=None, config_file_name=None):
        """
        Initializes object with default properties:
        port: 50007
        host: localhost
        archive dir: ~/.task_tracker_conf/archive_files
        db dir: ~/.task_tracker_conf/db_files
        logs dir: ~/.task_tracker_conf/log
        current_user_id: None
        configuration file name: config.ini
        main database file (where the information about users paths lies): main_db.json
        """

        self._default_properties = Globals.default_config_options

        if path_to_config_dir is not None:
            self._default_properties['config_dir'] = path_to_config_dir

        if path_to_config_dir is not None:
            self._default_properties['config_file'] = config_file_name

        self.set_config_directory()

        if not os.path.exists(self._default_properties['config_dir']):
            raise ValueError('Invalid config path has been passed.')

        self._default_properties['db_dir'] = os.path.join(
            self.get_property('config_dir'),
            self.get_property('db_dir')
        )
        self._default_properties['archive_dir'] = os.path.join(
            self.get_property('config_dir'),
            self.get_property('archive_dir')
        )
        self._default_properties['main_db_file'] = os.path.join(
            self.get_property('config_dir'),
            self.get_property('main_db_file')
        )
        self._default_properties['config_file'] = os.path.join(
            self.get_property('config_dir'),
            self.get_property('config_file')
        )
        self._default_properties['log_dir'] = os.path.join(
            self.get_property('config_dir'),
            self.get_property('log_dir')
        )

    def set_config_directory(self):
        """
        Chooses path to default directory, depending on a platform
        :return: None
        """

        if platform == 'linux' or platform == 'linux2':
            varname = 'HOME'
        elif platform == 'win32' or platform == 'win64':
            varname = 'HOMEPATH'
        elif platform == 'darwin':
            varname = 'HOME'
        path_to_config_dir = os.path.join(
            os.environ[varname],
            self._default_properties['config_dir']
        )
        if not self._default_properties['config_dir'].startswith(os.environ[varname]):
            self._default_properties['config_dir'] = path_to_config_dir

    def get_all_properties(self):
        """
        Returns all default properties. define by the program
        :return: dict
        """

        return self._default_properties

    def get_property(self, prop_name):
        """
        Returns value, to the key. value for the key is defined by the program
        :param prop_name: str
        :return: property value
        """

        return self._default_properties[prop_name]

    def get_config_dir(self):
        """
        Returns path to default configuration directory
        :return: path to default configuration directory
        """

        return self.get_property('config_dir')

    def get_default_db_dir(self):
        """
        Returns path to default database directory
        :return: path to default database directory
        """

        return self.get_property('db_dir')

    def get_default_archive_dir(self):
        """
        Returns path to default archive directory
        :return: path to default archive directory
        """

        return self.get_property('archive_dir')

    def get_default_log_dir(self):
        """
        Returns path to default log directory
        :return: path to default log directory
        """

        return self.get_property('log_dir')


def check_if_file_in_config_dir_exists(file_name, def_conf_obj=None):
    """
    Performs check for existenment of file with filename in default configuration directory
    :param file_name: str
    :param def_conf_obj: default config object that contains vital properties (DefaultConfig)
    :return: boolean. True if exists else False
    """

    if def_conf_obj is None:
        def_conf_obj = DefaultConfig()
    return os.path.exists(
        os.path.join(
            def_conf_obj.get_config_dir(),
            def_conf_obj.get_property(file_name)
        )
    )


def create_file_in_config_dir(file_name, def_conf_obj=None):
    """
    Creates file with name file_name in default configuration directory
    :param file_name: str
    :param def_conf_obj: default config object that contains vital properties (DefaultConfig)
    :return: None
    """
    if def_conf_obj is None:
        def_conf_obj = DefaultConfig()
    full_path = os.path.join(def_conf_obj.get_config_dir(), def_conf_obj.get_property(file_name))
    if not os.path.exists(os.path.dirname(full_path)):
        os.makedirs(os.path.dirname(full_path))
    os.mknod(full_path)


def init_config_dir(path_to_config_dir=None, def_conf_obj=None):
    """
    Initializes two main files in config derectory: main_db and config file. Others are optional
    :param path_to_config_dir: path to the directory where config file locates.
    :param def_conf_obj: default config object that contains vital properties (DefaultConfig)
    :return: None
    """

    if not check_if_file_in_config_dir_exists('config_file', def_conf_obj=def_conf_obj):
        create_file_in_config_dir('config_file', def_conf_obj=def_conf_obj)
    if not check_if_file_in_config_dir_exists('main_db_file', def_conf_obj=def_conf_obj):
        create_file_in_config_dir('main_db_file', def_conf_obj=def_conf_obj)


def set_parser_options(parser, section, file_to_write, **properties):
    """
    Takes options from dict properties and writes it to the section section
    :param parser: parser object
    :param section: section (example: DEFAULT_OPTIONS)
    :param file_to_write: file, where the options will be written
    :param properties: property dict
    :return: None
    """

    for key in properties:
        parser.set(section, key, properties[key])
    parser.write(
        open(
            file_to_write,
            'w'
        )
    )


def init_main_db_file(path_to_config_dir=None, def_conf_obj=None):
    """
    Initializes main_db file with default values,
    if there is no such file, or the json in file is not valid.
    Else passes
    :return: None
    """

    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    if not check_if_file_in_config_dir_exists('main_db_file', def_conf_obj=def_conf_obj):
        create_file_in_config_dir('main_db_file', def_conf_obj=def_conf_obj)
    try:
        with open(def_conf_obj.get_property('main_db_file')) as file:
            json.load(file)
    except:
        with open(def_conf_obj.get_property('main_db_file'), 'w') as file:
            data = {
                '': {
                    'db_dir': def_conf_obj.get_default_db_dir(),
                    'archive_dir': def_conf_obj.get_default_archive_dir(),
                    'log_dir': def_conf_obj.get_default_log_dir()
                }
            }
            json.dump(data, file)


def initialize_current_section(props, def_conf_obj=None, path_to_config_dir=None):
    """
    Initializes current section with properties provided by props argument
    :param props: dict of properties
    :param def_conf_obj: DefaultConfig object or None
    :param path_to_config_dir: path to the directory where config file locates.
    :return: None
    """

    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    parser = configparser.ConfigParser()
    parser.read(
        def_conf_obj.get_property('config_file')
    )
    if Globals.CURRENT_OPTIONS_SECTION not in parser.sections():
        parser.add_section(Globals.CURRENT_OPTIONS_SECTION)
    set_parser_options(
        parser=parser,
        section='CURRENT_OPTIONS',
        file_to_write=def_conf_obj.get_property('config_file'),
        **props
    )


def initialize_default_section_file(path_to_config_dir, def_conf_obj=None):
    """
    Fills the section DEFAULT_OPTIONS with default values, defined by the program
    :param path_to_config_dir: path to the directory where config file locates.
    :param def_conf_obj: DefaultConfig object or None
    :return: None
    """
    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    parser = configparser.ConfigParser()
    parser.read(
        def_conf_obj.get_property('config_file')
    )
    if Globals.DEFAULT_OPTIONS_SECTION not in parser.sections():
        parser.add_section(Globals.DEFAULT_OPTIONS_SECTION)
    set_parser_options(
        parser=parser,
        section=Globals.DEFAULT_OPTIONS_SECTION,
        file_to_write=def_conf_obj.get_property('config_file'),
        **def_conf_obj.get_all_properties()
    )


def set_default_current_properties(def_conf_obj=None, path_to_config_dir=None):
    """
    Sets current properties equal to default properties
    :param def_conf_obj: DefaultConfig object or None
    :param path_to_config_dir: path to the directory where config file locates.
    :return: None
    """

    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    initialize_current_section(def_conf_obj.get_all_properties(), def_conf_obj)


def check_validness_of_current_properties(def_conf_obj=None, path_to_config_dir=None):
    """
    Checks if the CURRENT_OPTIONS section is correct
    (must exist all the required fields that are in the default options)
    :param path_to_config_dir: path to the directory where config file locates.
    :param def_conf_obj: DefaultConfig object or None
    :return: None
    """

    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    parser = configparser.ConfigParser()
    parser.read(
        def_conf_obj.get_property('config_file')
    )
    if Globals.CURRENT_OPTIONS_SECTION not in parser.sections():
        set_default_current_properties(def_conf_obj)
    else:
        for key in def_conf_obj.get_all_properties():
            if key not in parser.options(Globals.CURRENT_OPTIONS_SECTION):
                set_default_current_properties(def_conf_obj)
                break


def get_current_properties(keys=[], path_to_config_dir=None, def_conf_obj=None):
    """
    Returns current properties from configuration file.
    If no keys are passed, returns all properties
    :param keys: list of keys of properties.
    :param path_to_config_dir: path to the directory where config file locates.
    :param def_conf_obj: DefaultConfig object or None.
    :return: dict of properties.
    """

    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    parser = configparser.ConfigParser()
    parser.read(
        def_conf_obj.get_property('config_file')
    )
    if not keys:
        return dict(parser[Globals.CURRENT_OPTIONS_SECTION].items())
    return dict((key, parser[Globals.CURRENT_OPTIONS_SECTION][key]) for key in keys)


def get_users_dir(dir_name, user_id, def_conf_obj=None, path_to_config_dir=None):
    """
    Gets the directory for db/archive/logs that was defined by the user with id user_id
    :param dir_name: str
    :param user_id: str
    :param def_conf_obj: DefaultConfig object or None.
    :param path_to_config_dir: path to the directory where config file locates.
    :return: path to the required directory.
    """

    if dir_name not in Globals.user_defined_directories:
        raise WrongDataException('Invalid key for directory name was passed. '
                                 '{0} doesn\'t match any of the following names: {1}'
                                 .format(dir_name, Globals.user_defined_directories))
    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    with open(def_conf_obj.get_property('main_db_file')) as file:
        data = json.load(file)
        if user_id not in data:
            data[user_id] = {
                'db_dir': def_conf_obj.get_default_db_dir(),
                'archive_dir': def_conf_obj.get_default_archive_dir(),
                'log_dir': def_conf_obj.get_default_log_dir()
            }
        if dir_name not in data[user_id]:
            data[user_id][dir_name] = def_conf_obj.get_property(dir_name)
        return data[user_id][dir_name]


def set_users_dir(user_id, dir_name, value=None, def_conf_obj=None, path_to_config_dir=None):
    """
    Sets new path for the property with name dir_name, due to the user configuration.
    :param user_id: ID of user
    :param dir_name: property name
    :param value: new path
    :param def_conf_obj: def_conf_obj: DefaultConfig object or None
    :param path_to_config_dir: path to the directory where config file locates.
    :return: None
    """

    if def_conf_obj is None:
        def_conf_obj = DefaultConfig(path_to_config_dir=path_to_config_dir)
    if dir_name not in Globals.user_defined_directories:
        raise WrongDataException('Invalid key for directory name was passed. '
                                 '{0} doesn\'t match any of the following names: {1}'
                                 .format(dir_name, Globals.user_defined_directories))
    with open(def_conf_obj.get_property('main_db_file')) as in_file:
        data = json.load(in_file)
        if user_id not in data:
            data[user_id] = {
                    'db_dir': def_conf_obj.get_default_db_dir(),
                    'archive_dir': def_conf_obj.get_default_archive_dir(),
                    'log_dir': def_conf_obj.get_default_log_dir()
                }
        if value is not None:
            data[user_id][dir_name] = value
        with open(def_conf_obj.get_property('main_db_file'), 'w') as out_file:
            json.dump(data, out_file)


class ConfigStore:
    """
    This class represents the connection with main configuration file.
    """

    def __init__(self, path_to_config_dir=None, config_file_name=None):
        """
        Initializes object of connection.
        Before that it initializes default configuration directory.
        """
        self.path_to_config_dir = path_to_config_dir
        self.config_file_name = config_file_name
        self.def_conf_obj = DefaultConfig(
            path_to_config_dir=path_to_config_dir,
            config_file_name=config_file_name
        )
        init_config_dir(
            path_to_config_dir=path_to_config_dir,
            def_conf_obj=self.def_conf_obj
        )
        init_main_db_file(
            path_to_config_dir=path_to_config_dir,
            def_conf_obj=self.def_conf_obj
        )
        initialize_default_section_file(
            path_to_config_dir=path_to_config_dir,
            def_conf_obj=self.def_conf_obj
        )
        check_validness_of_current_properties(
            def_conf_obj=self.def_conf_obj,
            path_to_config_dir=path_to_config_dir
        )
        self._current_config_properties = get_current_properties(
            path_to_config_dir=path_to_config_dir,
            def_conf_obj=self.def_conf_obj
        )

    def set_properties(self, props):
        """
        Sets current properties to section CURRENT_OPTIONS
        :param props: props dict
        :return: None
        """

        initialize_current_section(props, def_conf_obj=self.def_conf_obj)
        for prop_name in Globals.user_defined_directories:
            if prop_name in props:
                set_users_dir(
                    self.get_property('current_user_id'),
                    prop_name,
                    props[prop_name],
                    self.def_conf_obj,
                    path_to_config_dir=self.path_to_config_dir
                )
        for key, val in props.items():
            self._current_config_properties[key] = val

    def get_property(self, prop_name):
        """
        Returns current option, or default option for option_name property_name
        :param prop_name:
        :return:
        """

        if prop_name in Globals.user_defined_directories:
            return get_users_dir(
                prop_name,
                self.get_property('current_user_id'),
                self.def_conf_obj
            )
        if prop_name in self._current_config_properties:
            return self._current_config_properties[prop_name]
        elif prop_name in self.def_conf_obj.get_all_properties():
            return self.def_conf_obj.get_property(prop_name)
        else:
            raise WrongDataException('No such option in config!')

    def get_properties(self, keys=[]):
        """
        Returns current properties from configuration file.
        If no keys are passed, returns all properties
        :param keys: list of keys of properties
        :return: dict of properties
        """

        if not keys:
            return self._current_config_properties
        return dict((key, self.get_property(key)) for key in keys)

    def change_current_user(self, user_id):
        """
        Changes current user to user with id user_id
        :param user_id: id of new current user
        :return: None
        """

        self.set_properties({'current_user_id': user_id})
        for dir_name in ['db_dir', 'archive_dir', 'log_dir']:
            value = get_users_dir(dir_name, user_id, self.def_conf_obj)
            self.set_properties({dir_name: value})

    def get_current_user_id(self):
        """
        Returns id of current user
        :return: ID (str)
        """

        return self.get_property('current_user_id')

    def get_config_dir(self):
        """
        Returns main configuration directory
        :return: path to directory
        """

        return self.get_property('config_dir')

    def get_db_dir(self):
        """
        Returns current database directory
        :return: path to directory
        """

        return self.get_property('db_dir')

    def get_archive_dir(self):
        """
        Returns current archive directory
        :return: path to directory
        """

        return self.get_property('archive_dir')

    def get_log_dir(self):
        """
        Returns logs directory directory
        :return: path to directory
        """

        return self.get_property('log_dir')

