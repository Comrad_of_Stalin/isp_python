"""
This project requires python3 to be installed in your PC.
This library allows user to set tasks in local computer.
Also it is possible to make a notification checker about tasks.
Before you start using the library, call the init_db method from tools.
It sets default db file and configuration to the folder, specified by the system.
To see more about this function type help(task_tracker.tools.init_db) in python3 interpreter.

The project consists of two main parts:
    * The tool classes (database_manager, datetime_parser, exceptions, logger, tools, configuration).
    * The instances package

The tool classes do low-level logic of the project.
They pull and push objects into db, parse data and time delta objects, log execution of the project
functions, configure the project. Their main purpose is to make a nice
background for working with main instances, that lie in the instances package.

The instances package contains main models of the library. Each file in this package contains only 1 class,
that does it's own part of logic.
The files and classes in them are listed below:

    * module user.py:
        class User - represents user logic and implements all main logic of the library

    * module task.py:
        class Task - model, that represents something that you want to do in a distinct period of time

    * module group.py:
        class Group - set of users that can share tasks between each other

    * module tag.py:
        class Tag - model that can be attached to tasks and groups tasks by topic

    * module task_planner.py:
        class Plan - Creates a periodic plan, that will be creating tasks with defined properties

    * module remind.py:
        class Remind - notification for task

    * module base_id_object.py:
        class BaseIdObject - base class for most of the models. Helps to create unique id for the object.


Type  help(<module>) to see more information about the content of module <module>.

Have a nice time, using this library.
"""