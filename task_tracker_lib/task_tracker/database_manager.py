"""
Simple db manager logic.
By default we save different type objects with different prefixes.
For example if an object has type Foo we save it as db['Foo_<obj_repr>']
Type       Prefix
Task       Task_
Day        Day_
User       User_
and so on ...
"""
import copy
import json
import os
from functools import wraps

import jsonpickle
from task_tracker import configuration


def initialize_log_dir(log_dir=None, config_storage=None):
    """
    Initializes db dir using users personal data
    :param log_dir: If this parameter is specified than it will be set as db directory
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: None
    """
    if config_storage is None:
        config_storage = configuration.ConfigStore()
    if log_dir is None:
        log_dir = config_storage.get_log_dir()
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    config_storage.set_properties({'log_dir': log_dir})


def initialize_db_dir(db_dir=None, config_storage=None):
    """
    Initializes db dir using users personal data
    :param db_dir: If this parameter is specified than it will be set as db directory
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: None
    """

    if config_storage is None:
        config_storage = configuration.ConfigStore()
    if db_dir is None:
        db_dir = config_storage.get_db_dir()
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)
    data_dir_list = ['User', 'Task', 'Remind', 'Day', 'Group']
    for dir_name in data_dir_list:
        pth = os.path.join(db_dir, dir_name)
        if not os.path.exists(pth):
            os.mkdir(pth)
    config_storage.set_properties({'db_dir': db_dir})


def initialize_archive_dir(archive_dir=None, config_storage=None):
    """
    Initializes archive dir using users personal data
    :param archive_dir: If this parameter is specified than it will be set as db directory
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: None
    """

    if config_storage is None:
        config_storage = configuration.ConfigStore()
    if archive_dir is None:
        archive_dir = config_storage.get_archive_dir()
    if not os.path.exists(archive_dir):
        os.makedirs(archive_dir)
    data_dir_list = ['User', 'Task', 'Remind', 'Day', 'Group']
    for dir_name in data_dir_list:
        pth = os.path.join(archive_dir, dir_name)
        if not os.path.exists(pth):
            os.mkdir(pth)
    config_storage.set_properties({'archive_dir': archive_dir})


def init_default_settings():
    """
    Initializes default settings for db
    :return: None
    """

    def_conf_object = configuration.DefaultConfig()
    initialize_db_dir(def_conf_object.get_default_db_dir())
    initialize_archive_dir(def_conf_object.get_default_archive_dir())
    initialize_log_dir(def_conf_object.get_default_log_dir())


def _get_storage_file(obj_id, config_storage=None, path_to_db_dir=None):
    """
    Returns path to storage file where the object should store
    :param obj_id: object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: path to the storage file
    """
    if config_storage is not None:
        db_dir = config_storage.get_db_dir()
    elif path_to_db_dir is not None:
        db_dir = path_to_db_dir
    else:
        config_store = configuration.ConfigStore()
        db_dir = config_store.get_db_dir()
    arr = [db_dir] + obj_id.split('_')
    file_name = obj_id
    dir_name = os.path.join(*arr[:-1])
    if not file_name.endswith('.json'):
        file_name += '.json'
    return dir_name, file_name


def _get_archive_file(obj_id, config_storage=None, path_to_archive_dir=None):
    """
    Returns path to archive file where the archived object should store
    :param obj_id: object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_archive_dir: path ot the directory where archive files locate.
    :return: path to the archive file
    """

    if config_storage is not None:
        archive_dir = config_storage.get_archive_dir()
    elif path_to_archive_dir is not None:
        archive_dir = path_to_archive_dir
    else:
        config_store = configuration.ConfigStore()
        archive_dir = config_store.get_archive_dir()
    arr = obj_id.split('_')
    file_name = obj_id
    file_dir = arr[0]
    dir_name = os.path.join(archive_dir, file_dir)
    if not file_name.endswith('.json'):
        file_name += '.json'
    return dir_name, file_name


def get_object_from_db_(obj_id, config_storage=None, path_to_db_dir=None):
    """
    Returns object that is stored in db. If there is not such object None is returned.
    :param obj_id: object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: object or None
    """

    dir_name, file_name = _get_storage_file(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )
    if not os.path.exists(os.path.join(dir_name, file_name)):
        return None
    else:
        with open(os.path.join(dir_name, file_name)) as db_file:
            data = json.load(db_file)
            obj = jsonpickle.decode(data)
            return obj


def get_object_from_archive_(obj_id, config_storage=None, path_to_archive_dir=None):
    """
    Returns object that is stored in archive. If there is not such object None is returned.
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_archive_dir: path ot the directory where archive files locate.
    :param obj_id: object id
    :return: object or None
    """

    dir_name, file_name = _get_archive_file(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_archive_dir=path_to_archive_dir
    )
    if not os.path.exists(os.path.join(dir_name, file_name)):
        return None
    else:
        with open(os.path.join(dir_name, file_name)) as db_file:
            data = json.load(db_file)
            obj = jsonpickle.decode(data)
            return obj


def object_exists_in_archive(obj_id, config_storage=None, path_to_archive_dir=None):
    """
    Returns True if object is stored in archive.
    :param obj_id: object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_archive_dir: path ot the directory where archive files locate.
    :return: True if there is such object in db
    """

    dir_name, file_name = _get_archive_file(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_archive_dir=path_to_archive_dir
    )
    obj = get_object_from_archive_(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_archive_dir=path_to_archive_dir
    )
    return os.path.exists(os.path.join(dir_name, file_name)) and obj is not None


def push_object_to_db_(obj_id, obj, config_storage=None, path_to_db_dir=None):
    """
    Adds object with id obj_id to db. If there already exists such object, method rewrites it.
    :param obj_id: object id
    :param obj: object
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: None
    """

    dir_name, file_name = _get_storage_file(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    with open(os.path.join(dir_name, file_name), 'w') as db_file:
        data = jsonpickle.encode(obj)
        json.dump(data, db_file, indent=4)


def move_object_to_achive(obj_id, obj):
    """
    Moves object to archive
    :param obj_id:
    :param obj: object (any instance)
    :return: None
    """

    db_dir, archive_dir = obj.path_to_db_dir, obj.path_to_archive_dir
    del_object_in_db_(obj_id, path_to_db_dir=db_dir)
    dir_name, file_name = _get_archive_file(obj_id, path_to_archive_dir=archive_dir)
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    with open(os.path.join(dir_name, file_name), 'w') as db_file:
        data = jsonpickle.encode(obj)
        json.dump(data, db_file, indent=4)


def del_object_in_db_(obj_id, config_storage=None, path_to_db_dir=None):
    """
    Deletes object which is now storing in the db
    :param obj_id: object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: None
    """

    dir_name, file_name = _get_storage_file(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )
    if os.path.exists(os.path.join(dir_name, file_name)):
        os.remove(os.path.join(dir_name, file_name))


def del_object_in_archive_(obj_id, config_storage=None, path_to_archive_dir=None):
    """
    Deletes object which is now storing in the archive
    :param obj_id: object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_archive_dir: path ot the directory where archive files locate.
    :return: None
    """

    dir_name, file_name = _get_archive_file(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_archive_dir=path_to_archive_dir
    )
    if os.path.exists(os.path.join(dir_name, file_name)):
        os.remove(os.path.join(dir_name, file_name))


def saving_changes(func):
    """
    Decorates method, saving object after finishing method action
    :param func: method to decorate
    :return: decorated method
    """

    @wraps(func)
    def wrapped(self_obj, *args, **kwargs):
        """
        Just a wrapping function that saves object after doing main action
        :param self_obj: link to object
        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: same value as the the wrapped method
        """

        ret = func(self_obj, *args, **kwargs)
        if self_obj.can_be_saved():
            push_object_to_db_(
                obj_id=self_obj.id,
                obj=self_obj,
                path_to_db_dir=self_obj.path_to_db_dir
            )
        return ret
    return wrapped


def removing_object(func):
    """
    Decorates method, removing object from db after finishing method action
    :param func: method to decorate
    :return: decorated method
    """
    @wraps(func)
    def wrapped(self_obj, *args, **kwargs):
        """
        Just a wrapping function that deletes object after doing main action
        :param self_obj: link to object
        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: same value as the the wrapped method
        """
        ret = func(self_obj, *args, **kwargs)
        self_obj.prevent_from_saving_in_db()
        del_object_in_db_(
            obj_id=self_obj.id,
            path_to_db_dir=self_obj.path_to_db_dir
        )
        return ret
    return wrapped


def archiving_object(func):
    """
    Decorates method, moving object from db to archive after finishing method action
    :param func: method to decorate
    :return: decorated method
    """
    @wraps(func)
    def wrapped(self_obj, *args, **kwargs):
        """
        Just a wrapping function that moves object to archive after doing main action
        :param self_obj: link to object
        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: same value as the the wrapped method
        """
        ret = func(self_obj, *args, **kwargs)
        self_obj.prevent_from_saving_in_db()
        self_obj.allow_to_archive()
        saved_obj = copy.deepcopy(self_obj)
        try:
            move_object_to_achive(self_obj.id, self_obj)
        except Exception:
            saved_obj.allow_saving_in_db()
            saved_obj.prevent_from_saving_in_archive()
            raise
        return ret
    return wrapped


def get_config_values(
        keys=[],
        config_storage=None,
        path_to_config_dir=None,
        config_file_name=None):
    """
    Gets dict of configuration properties
    :param keys: list of property names
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_config_dir: path ot the directory where config file locates.
    :param config_file_name: the name of config file.
    :return: dict of configuration properties
    """

    if config_storage is None:
        return configuration.ConfigStore(
            path_to_config_dir=path_to_config_dir,
            config_file_name=config_file_name
        ).get_properties(keys)
    return config_storage.get_properties(keys)


def set_config_values(
        properties={},
        config_storage=None,
        path_to_config_dir=None,
        config_file_name=None):
    """
    Sets new configuration properties to main config file
    :param properties: properties (dict)
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_config_dir: path ot the directory where config file locates.
    :param config_file_name: the name of config file.
    :return: None
    """

    if config_storage is None:
        configuration.ConfigStore(
            path_to_config_dir=path_to_config_dir,
            config_file_name=config_file_name
        ).set_properties(properties)
    config_storage.set_properties(properties)


def object_exists_in_db(obj_id, config_storage=None, path_to_db_dir=None):
    """
    Returns True if object is in db now
    :param obj_id: object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: True if object is stored in db, else False
    """

    dir_name, file_name = _get_storage_file(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )
    obj = get_object_from_db_(
        obj_id=obj_id,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )
    return os.path.exists(os.path.join(dir_name, file_name)) and obj is not None


def get_all_objects_from_db(cls, path_to_db_dir=None, config_storage=None):
    """
    Returns list of objects of particular instance, that are in db now
    :param cls: instance (class)
    :param path_to_db_dir: path ot the directory where DB files locate.
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: list of its stored objects
    """

    if cls is None:
        return []
    dir_name, file_name = _get_storage_file(cls.__name__+'_fictiveId',
                                            path_to_db_dir=path_to_db_dir,
                                            config_storage=config_storage)
    ret = []
    for file_name in os.listdir(dir_name):
        with open(os.path.join(dir_name, file_name)) as db_file:
            obj = jsonpickle.decode(json.load(db_file))
            ret.append(obj)
    return ret
