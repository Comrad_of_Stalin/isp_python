"""
This file contains useful functions for project.
The are mostly the coverage above the DBManager and TempStore classes, but they are really helpful.
"""
from task_tracker import configuration
from task_tracker import database_manager


def get_object_by_id(_id, config_storage=None, path_to_db_dir=None):
    """
    Attempts to find last version of object in memory, or in DB. If failes returns None
    :param _id: required object id (str)
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate. (str)
    :return: None
    """

    if database_manager.object_exists_in_db(
            obj_id=_id,
            config_storage=config_storage,
            path_to_db_dir=path_to_db_dir):
        return database_manager.get_object_from_db_(
            obj_id=_id,
            config_storage=config_storage,
            path_to_db_dir=path_to_db_dir
        )
    return None


def get_object_from_archive(_id, config_storage=None, path_to_archive_dir=None):
    """
    Gets object from archive
    :param _id: object id
    :param path_to_archive_dir: path ot the directory where archive files locate.
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: object
    """

    if database_manager.object_exists_in_archive(
            obj_id=_id,
            config_storage=config_storage,
            path_to_archive_dir=path_to_archive_dir):
        return database_manager.get_object_from_archive_(
            obj_id=_id,
            config_storage=config_storage,
            path_to_archive_dir=path_to_archive_dir
        )
    return None


def object_is_storing(_id, config_storage=None, path_to_db_dir=None):
    """
    Returns True if object is storing in DB or in RAM
    :param _id: required object id
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: True if object is in DB or in RAM, else False
    """

    return database_manager.object_exists_in_db(
        obj_id=_id,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )


def object_is_in_archive(_id, config_storage=None, path_to_archive_dir=None):
    """
    Returns True
    :param _id: required object id
    :param path_to_archive_dir: path ot the directory where archive files locate.
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: True if object is storing in archive, else returns False
    """

    return database_manager.object_exists_in_archive(
        obj_id=_id,
        config_storage=config_storage,
        path_to_archive_dir=path_to_archive_dir
    )


def list_instance_objects(cls, config_storage=None, path_to_db_dir=None):
    """
    Returns list of object of the given instance that are in DB now
    :param cls: Class
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: list of objects
    """

    return database_manager.get_all_objects_from_db(
        cls=cls,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )


def get_config_value(key, config_storage=None):
    """
    Returns config value, specified by key
    :param key: str
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: config value
    """

    return database_manager.get_config_values([key], config_storage=config_storage)[key]


def get_config_values(config_storage=None):
    """
    Returns all config values
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: all values from section 'CURRENT _OPTIONS' in config.ini
    """

    return database_manager.get_config_values(config_storage=config_storage)


def set_config_values(props, config_storage=None):
    """
    sets the given Config properties
    :param props: properties (dict)
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: None
    """

    return database_manager.set_config_values(props, config_storage=config_storage)


def del_object_from_archive(_id, config_storage=None, path_to_archive_dir=None):
    """
    Deletes object which is now storing in the archive
    :param _id: id of archived object
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_archive_dir: path ot the directory where archive files locate.
    :return: None
    """

    database_manager.del_object_in_archive_(
        obj_id=_id,
        config_storage=config_storage,
        path_to_archive_dir=path_to_archive_dir
    )


def get_object_from_db(_id, config_storage=None, path_to_db_dir=None):
    """
    Returns object that
    :param _id: id of object
    :param config_storage: ConfigStore object that contain configuration settings.
    :param path_to_db_dir: path ot the directory where DB files locate.
    :return: object
    """

    return database_manager.get_object_from_db_(
        obj_id=_id,
        config_storage=config_storage,
        path_to_db_dir=path_to_db_dir
    )


def initialize_database_settings(path_to_db=None, path_to_archive=None, path_to_logs=None):
    """
    Initializes db and archive directory for current_user
    :param path_to_db: path ot the directory where DB files locate.
    :param path_to_archive: path ot the directory where archive files locate.
    :param path_to_logs: path ot the directory where log files locate.
    :return: None
    """

    database_manager.initialize_db_dir(path_to_db)
    database_manager.initialize_archive_dir(path_to_archive)
    database_manager.initialize_log_dir(path_to_logs)


def init_db(config_storage=None):
    """
    Initializes db with default settings or with settings, that are being configured by user
    :param config_storage: ConfigStore object that contain configuration settings.
    :return: None
    """

    if config_storage is None:
        config_storage = configuration.ConfigStore()
    initialize_database_settings(
        path_to_db=config_storage.get_db_dir(),
        path_to_archive=config_storage.get_archive_dir(),
        path_to_logs=config_storage.get_log_dir()
    )


def get_current_user_id(config_storage=None):
    """
    Returns current user id from config
    :return: current user id
    """

    return database_manager.get_config_values(
        ['current_user_id'],
        config_storage=config_storage
    )['current_user_id']
