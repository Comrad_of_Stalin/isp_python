"""
The console application allows to set tasks and different properties for tasks.

To start the application type:

        task_tracker

In the terminal.

Then you'll see the help page for this app.

There are 7 main sub commands:

    * user
    * task
    * tag
    * group
    * remind
    * config

Type task_tracker <COMMAND> to see more.

After you do so, you'll see the help page for the sub command COMMAND.

There, you'll see all available options for the chosen sub command.

Finally, type task_tracker <COMMAND> <SECOND_COMMAND> to get the run program.

Example:        task_tracker user current   ---  will output the information about current user

You can add tag to task, or you can change some fields of your task.

You can share task between users in group.

If you are scared that there is a chance that you'll forget about task,

there is an option of setting reminds for tasks.

WARNING:
    when you start working with the app, there will create directory with the name .task_tracker_conf
    in your home directory. Don't worry it's main a configuration directory.
    You can delete it, then you'll lose all your data, but the program will still work then.

"""