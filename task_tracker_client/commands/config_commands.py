import click
from app_tools.validation_tools import check_authentication, auth_user
from task_tracker import tools


@click.group()
@click.pass_context
def config(ctx):
    """
    Configurates global settings like db paths, etc.
    setpath - configures path for db/archive/logs
    """


@config.command()
@click.option('--dbdir', help='Directory where you want to store your personal db files.')
@click.option('--archivedir', help='Directory where you want to store your personal archive files.')
@click.option('--logdir', help='Directory where you want to store your log files.')
@click.option('--level', '-l', default='INFO', help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def setpath(tracker, dbdir=None, archivedir=None, logdir=None, level=None):
    """
    This command helps to configure new path to db/archive/logs.
    Pathes are independent for all each user.
    WARNING: if you change your config directory,
    you may lose some f your data, which is being stored in another place.
    """

    tracker.set_logging_level(level)
    u = auth_user(config_storage=tracker.config_storage)
    check_authentication(u)

    if dbdir:
        click.confirm('Do you really want to change DB directory? '
                      '\nYou may lose some of your personal data.', abort=True)
        tools.set_config_values({'db_dir': dbdir}, config_storage=tracker.config_storage)
        click.echo('Changed db directory to: {}'.format(tools.get_config_value('db_dir')))
    if archivedir:
        click.confirm('Do you really want to change archive directory? '
                      '\nYou may lose some of your personal data.', abort=True)
        tools.set_config_values({'archive_dir': archivedir}, config_storage=tracker.config_storage)
        click.echo('Changed archive directory to: {}'.format(tools.get_config_value('archive_dir')))
    if logdir:
        click.confirm('Do you really want to change log directory? '
                      '\nYou may lose some of your personal data.', abort=True)
        tools.set_config_values({'log_dir': logdir}, config_storage=tracker.config_storage)
        click.echo('Changed archive directory to: {}'.format(tools.get_config_value('log_dir')))


@config.command()
@click.option('--dbdir', is_flag=True, help='Path to directory where you store your personal db files.')
@click.option('--archivedir', is_flag=True, help='Path to directory where you store your personal archive files.')
@click.option('--logdir', is_flag=True, help='Path to directory where you store your log files.')
@click.pass_obj
def showpath(tracker, dbdir, archivedir, logdir):
    """
    Shows the path to the directory you asked for.
    """

    if dbdir:
        click.echo('Path to DB directory: \n\t{}'.format(
            tools.get_config_value(
                'db_dir',
                config_storage=tracker.config_storage
            )
        ))
    if archivedir:
        click.echo('Path to ARCHIVE directory: \n\t{}'.format(
            tools.get_config_value(
                'archive_dir',
                config_storage=tracker.config_storage
            )
        ))
    if logdir:
        click.echo('Path to LOG directory: \n\t{}'.format(
            tools.get_config_value(
                'log_dir',
                config_storage=tracker.config_storage
            )
        ))
