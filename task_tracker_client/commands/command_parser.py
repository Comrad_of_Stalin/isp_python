import click
from app_tools.print_tools import (handle_exception,
                                   add_exception_help_page)
from commands import config_commands
from commands import group_commands
from commands import planner_commands
from commands import remind_commands
from commands import tag_commands
from commands import task_commands
from commands import user_commands
from task_tracker.instances.tracker import Tracker


@click.group()
@click.pass_context
def root(ctx):
    """
    Welcome to task_tracker console application.\n
    This application helps to manage tasks for user on local PC.\n
    USAGE:     task_tracker <OBJECT> <COMMAND> [OPTIONS].\n
    Objects represent different instances that can perform the main actions.
    """
    main_tracker = Tracker('/home/deadcode/db', '/home/deadcode/db/archive')
    main_tracker.init_new_configuration('/home/deadcode/example/folder', 'config_file.ini')
    ctx.obj = main_tracker


@handle_exception
def entry_point():
    root.add_command(user_commands.user)
    root.add_command(task_commands.task)
    root.add_command(tag_commands.tag)
    root.add_command(planner_commands.plan)
    root.add_command(group_commands.group)
    root.add_command(remind_commands.remind)
    root.add_command(config_commands.config)

    add_exception_help_page(root)

    root()
