from datetime import datetime, timedelta

import click
from app_tools.print_tools import print_task_info
from app_tools.validation_tools import (check_authentication,
                                        check_if_task_exist,
                                        auth_user)
from task_tracker import datetime_parser
from task_tracker import tools


@click.group()
@click.pass_context
def group(ctx):
    """
    Makes operations with groups. Type task_tracker group --help to see more information.
    add - adds new group,
    delete - deletes group,
    sharetask - adds task to group,
    deltask - removes task from group,
    show - shows the information about the specified group, or about all groups
    """


@group.command()
@click.option('--groupname', '-g', type=str,
              help='The name of group you want to create.')
@click.option('--member', '-m', required=False, multiple=True,
              help='Member of group you want to create.')
@click.option('--task', '-t', required=False, multiple=True,
              help='Task that you want to share wth your group.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def add(tracker, groupname, member, task, level):
    """
    Adds new group to the user, adding new members and allowing them to share tasks.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if not member:
        member = ()
    if not task:
        task = ()
    tracker.manager.add_group(user=u, group_name=groupname, users_list=member, task_list=task, save=True)
    click.echo('Added new group with name {0}. '
               '\nMembers: {1}. '
               '\nTask list: {2}'.format(groupname, member, task))


@group.command()
@click.option('--groupname', '-g', type=str,
              help='The name of group you want to create.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delete(tracker, groupname, level):
    """
    Deletes group with name groupname.
    All of the shared tasks will become unshared.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    tracker.manager.del_group(user=u, group_name=groupname)
    click.echo('Deleted group with name {0}.'.format(groupname))


@group.command()
@click.option('--groupname', '-g', default=None, type=str,
              help='The name of group you want to look at.')
@click.option('--more', '-m', is_flag=True,
              help='Show more info about each task')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def show(tracker, groupname, more, level):
    """
    Shows the information about the group with name groupname.
    If no group name is specified, it shows the short information about all groups.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    tracker.manager.clean_group_list(user=u, save=True)
    if groupname is not None:
        g = tracker.manager.get_group(user=u, group_name=groupname)
        g.clean_group_tasks(path_to_db_dir=tracker.path_to_db, save=True)
        click.echo('Group name: {0}'
                   '\nGroup members: {1}'
                   '\nGroup admin: {2}'.format(g.name, g.members, g.admin_id))
        if more:
            click.echo('TASK LIST:\n')
            for _id in g.task_list:
                t = None
                if tools.object_is_storing(_id, path_to_db_dir=tracker.path_to_db):
                    t = tracker.manager.get_task_object_from_db(_id)
                elif tools.object_is_in_archive(_id, path_to_archive_dir=tracker.path_to_archive):
                    t = tracker.manager.get_task_object_from_archive(_id)
                if t is None:
                    continue
                if not g.has_task(t.parent_task_id):
                    print_task_info(t.id, tracker.manager, more, '')
                    click.echo()
    else:
        click.echo('GROUPS:\n')
        if not len(u.groups):
            click.echo('No groups for this user yet!')
            return
        for group_id in u.groups:
            g = tracker.manager.get_group(user=u, group_name=group_id)
            g.clean_group_tasks(path_to_db_dir=tracker.path_to_db, save=True)
            click.echo('Group name: {0}'
                       '\nGroup members: {1}'
                       '\nGroup tasks: {2}'
                       '\nGroup admin: {3}'
                       '\n\n'.format(g.name, g.members, g.task_list, g.admin_id))


@group.command()
@click.option('--taskid', '-t', type=str, default=None,
              help='Task id or short alias.')
@click.option('--groupname', '-g', type=str,
              help='The name of group you want to create.')
@click.option('--starttime', '-s', default=datetime_parser.datetime_to_str(datetime.now()),
              help='Start time for this task')
@click.option('--finishtime', '-f',
              default=datetime_parser.datetime_to_str(datetime.now()+timedelta(hours=1)),
              help='Finish time for this task')
@click.option('--msg', '-m', default=None,
              help='Message for this task')
@click.option('--priority', '-p', default=1,
              help='Priority for this task')
@click.option('--alias', '-a', default='', type=str,
              help='Alias for new task.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def sharetask(tracker, taskid, groupname, starttime, finishtime, msg, priority, alias, level):
    """
    Shares users task to all users of the specified group.
    It means that each user of this group will be able to edit or finish task.
    If tasks id is specified, an existing task will be added to group, else the task will be created
    and then added to the group.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if taskid is None:
        taskid = tracker.manager.create_new_task(
            user=u,
            finish_time=finishtime,
            msg=msg,
            start_time=starttime,
            users_owners_list=[],
            priority=priority,
            save=True
        )
        tracker.manager.add_alias(user=u, alias=alias, value=taskid)
    else:
        taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)
    tracker.manager.share_task_with_group(
        user=u,
        task_id=taskid,
        group_name=groupname,
        save=True
    )
    click.echo('Task with id {0} has been shared to group {1}'.format(taskid, groupname))


@group.command()
@click.option('--taskid', '-t', type=str,
              help='Task id or short alias.')
@click.option('--groupname', '-g', type=str,
              help='The name of group you want to create.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def deltask(tracker, taskid, groupname, level):
    """
    Removes task from the group.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    tracker.manager.del_task_from_group(
        user=u,
        task_id=taskid,
        group_name=groupname,
        save=True
    )
    click.echo('Task with id {0} has been removed from group {1}'.format(taskid, groupname))


@group.command()
@click.option('--groupname', '-g', type=str,
              help='The name of group you want to create.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delgroup(tracker, groupname, level):
    """
    Deletes the group and unshares all of the tasks that have been shared to it.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    tracker.manager.del_group(user=u, group_name=groupname)
    click.echo('Group with name {0} was deleted.'.format(groupname))


@group.command()
@click.option('--groupname', '-g', type=str,
              help='The name of group in which you want to invite users.')
@click.option('--member', '-m', required=False, multiple=True,
              help='User, whom you want to invite into group. '
                   'You can provide multiply values (id-s)')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def invite(tracker, groupname, member, level):
    """
    Invites somebody in the group. Required parameters: id of the user (somebody).\n
    Example: User_exampleuser.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if not u.has_group(groupname):
        click.echo('User {} doesn\'t have such group!')
        return
    for m in member:
        tracker.manager.invite_to_group(user=u, user_id=m, group_name=groupname, save=True)
        click.echo('Invited user {0} into group {1}'.format(m, groupname))


@group.command()
@click.option('--groupname', '-g', type=str,
              help='The name of group you want to create.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def quit(tracker, groupname, level):
    """
    Quits from the group with name groupname. Throws exception if there is no such group.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if not u.has_group(groupname):
        click.echo('User {} doesn\'t have such group!')
        return
    tracker.manager.quit_from_group(user=u, group_name=groupname, save=True)


@group.command()
@click.option('--groupname', '-g', type=str,
              help='The name of group in which you want to invite users.')
@click.option('--member', '-m', required=False, multiple=True,
              help='Member of group whom you want to kick from group. '
                   'You can provide multiply values (id-s)')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def deluser(tracker, groupname, member, level):
    """
    Deletes user that is present in the group now. Requires user id (example: User_exampleuser).
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if not u.has_group(groupname):
        click.echo('User {} doesn\'t have such group!'.format(u.name))
        return
    for m in member:
        tracker.manager.kick_user_from_group(
            user=u,
            group_name=groupname,
            user_id=m,
            save=True
        )
        click.echo('Deleted user {} from group.'.format(m))
