from datetime import datetime

import click
from app_tools.print_tools import print_task_info
from app_tools.validation_tools import (check_authentication,
                                        check_if_task_exist,
                                        auth_user)
from task_tracker import datetime_parser
from task_tracker import tools


@click.group()
@click.pass_context
def task(ctx):
    """
    Makes operations with tasks. Type task_tracker task --help to see more information.
    Main commands:
    add - adds new task,
    addalias - adds short name for task,
    attach - adds child task for task,
    aliases: show all the aliases of current user,
    archive: moves task and all it's active subtasks to archive,
    delalias - deletes short description of task,
    delarchived - deletes archived task from archive,
    delete - deletes task,
    edit - changes any options of task,
    fail - fails the task,
    finish - finishes task,
    info - shows the information about task,
    update - update single task or task list
    """


@task.command()
@click.option('--starttime', '-s',
              default=datetime_parser.datetime_to_str(datetime.now()),
              help='Start time for this task in format %Y-%m-%d %H:%M:%S.')
@click.option('--finishtime', '-f',
              default=datetime_parser.datetime_to_str(datetime(year=3000, month=1, day=1)),
              help='Finish time for this task in format %Y-%m-%d %H:%M:%S.')
@click.option('--msg', '-m', default=None,
              help='Message for this task')
@click.option('--priority', '-p', default=1,
              help='Priority for this task')
@click.option('--level', '-l', default='INFO', help='Set logging level. '
                                                    'Default = INFO. Also available: DEBUG')
@click.option('--alias', '-a', default='', type=str,
              help='Alias for new task.')
@click.option('--parenttask', '-P', default=None, help='Parent task id or alias')
@click.pass_obj
def add(tracker, starttime, finishtime, msg, priority, level, alias, parenttask):
    """
    Adds new task to current user. If parent task option is set,
    than the newly created task becomes a subtask of parent task.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if starttime is None:
        starttime = datetime.now()
    if parenttask is None:
        taskid = tracker.manager.create_new_task(
            user=u,
            finish_time=finishtime,
            msg=msg,
            start_time=starttime,
            priority=priority,
            save=True
        )
    else:
        taskid = tracker.manager.add_new_subtask_to_task(
            user=u,
            parent_task=parenttask,
            finish_time=finishtime,
            msg=msg,
            start_time=starttime,
            priority=priority,
            save=True
        )

    if alias:
        tracker.manager.add_alias(user=u, alias=alias, value=taskid, save=True)

    t = tracker.manager.get_task_object_from_db(taskid)
    click.echo('Added task with id {0}. \nTask message: {1}. \nTask DEADLINE: {2}.\nParent task: {3}.'
               .format(t.id, t.msg, t.finish_time, t.parent_task_id))


@task.command()
@click.option('--taskid', '-t',
              help='ID or alias of task you want to delete')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delete(tracker, taskid, level):
    """
    Deltes task from current user. Define task id or alias f target task.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)
    tracker.manager.del_task(u, taskid, save=True)
    click.echo('Task with id {0} was deleted.'.format(taskid))


@task.command()
@click.option('--parenttask', '-p', help='Parent task id or alias')
@click.option('--sontask', '-s', help='Son task id or alias')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def attach(tracker, parenttask, sontask, level):
    """
    Makes son_task a subtask of parent_task
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    tracker.manager.add_subtask_to_task(user=u, parent_task=parenttask, son_task=sontask, save=True)
    click.echo('Task with id {0} became a subtask of task with id {1}'.format(sontask, sontask))


@task.command()
@click.option('--sontask', '-s', help='Son task id or alias')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def deattach(tracker, sontask, level):
    """
    Re-weighs the child task of parent task (son task), making this two tasks independent.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(usr=u, taskid=sontask, path_to_db_dir=tracker.path_to_db)
    task_obj = tracker.manager.get_task_object_from_db(taskid)
    parent_task_id = task_obj.parent_task_id
    tracker.manager.deattach_subtask_from_task(user=u, parent_task=parent_task_id, son_task=sontask, save=True)
    click.echo('Task with id {0} is not a subtask of task with id {1} any more.'.format(sontask, parent_task_id))


@task.command()
@click.option('--taskid', '-t',
              help='ID or alias of task you want to finish')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.option('--archive', '-a', is_flag=True,
              help='Type this flag if you want to move the completed task into the archive')
@click.pass_obj
def finish(tracker, taskid, level, archive):
    """
    Finishes target task. Also it can move it o archive if the --archive flag is set.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)

    tracker.manager.finish_task(user=u, task=taskid, save=True, archive=archive)
    click.echo('Finished task with id {0}'.format(taskid))


@task.command()
@click.option('--taskid', '-t',
              help='ID or alias of task you want to fail')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.option('--archive', '-a', is_flag=True,
              help='Type this flag if you want to move the completed task into the archive')
@click.pass_obj
def fail(tracker, taskid, level, archive):
    """
    Fails the target task. Also it can move it o archive if the --archive flag is set.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)

    tracker.manager.fail_task(user=u, task=taskid, archive=archive, save=True)
    click.echo('Failed task with id {0}.'.format(taskid))


@task.command()
@click.option('--taskid', '-t', required=True,
              help='ID or alias of te required task.')
@click.option('--more', '-m', is_flag=True,
              help='Show more about this task.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def info(tracker, taskid, more, level):
    """
    Prints detail information about the task and it's subtasks.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)

    print_task_info(taskid, tracker.manager, more, '')


@task.command()
@click.option('--alias', '-a', type=str,
              help='Alias for task.')
@click.option('--taskid', '-t', type=str,
              help='ID of task.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def addalias(tracker, alias, taskid, level):
    """
    Adds alias 'alias' for the task with id taskid
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    tracker.manager.add_alias(user=u, alias=alias, value=taskid, save=True)
    click.echo('Added alias {0} with value {1} to user {2}.'.format(alias, taskid, u.name))


@task.command()
@click.option('--alias', '-a', type=str,
              help='Show more about this task.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delalias(tracker, alias, level):
    """
    Deletes alias.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    tracker.manager.del_alias(user=u, val=alias, save=True)
    click.echo('Deleted alias {0}.'.format(alias))


@task.command()
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def aliases(tracker, level):
    """
    Shows all the available aliases for tasks.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    for key, val in u.aliases.items():
        click.echo('{0} : {1}'.format(key, val))
    if not u.aliases:
        click.echo('No aliases was set for this user yet.')


@task.command()
@click.option('--all', '-a', is_flag=True,
              help='Type this flag if you want to dop all archived tasks')
@click.option('--taskid', '-t', type=str,
              help='Task id or alias. Myst have type str.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def archive(tracker, taskid, all, level):
    """
    Move task to archive if such task exists in archive.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if all:
        tracker.manager.archive_all_tasks(user=u)
        click.echo('All the active tasks were moved to archive. '
                   '\nType task_tracker user tasks -m to see them.')
    else:
        taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)

        tracker.manager.archive_task(user=u, task=taskid)
        click.echo('Task with id {0} has been moved to archive. '
                   '\nType task_tracker user tasks -m to see it'.format(taskid))


def del_archived_tasks(tracker, level):
    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    del_list = []
    for task in u.archived_tasks:
        del_list.append(task)
    for task in del_list:
        tracker.manager.del_archived_task(user=u, taskid=task)


@task.command()
@click.option('--taskid', '-t', default='', help='ID or alias of task')
@click.option('--all', is_flag=True,
              help='Type this flag if you want to dop all archived tasks')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delarchived(tracker, taskid, all, level):
    """
    Moves the specified task to the archive. If --all flag is set,
    than moves all the current tasks to the archive.
    """

    if all:
        del_archived_tasks(tracker, level)
    else:
        u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
        check_authentication(u)
        tracker.manager.del_archived_task(user=u, taskid=taskid)
        click.echo('Task with id {0} was removed from archive. It doesn\'texist any more.'.format(taskid))


@task.command()
@click.option('--archive', '-a', is_flag=True,
              help='Type this flag if you want to move the completed or failed task into the archive')
@click.option('--all', is_flag=True,
              help='Type this flag if you want to update all tasks')
@click.option('--taskid', '-t', type=str, help='Task id or alias. Myst have type str.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def update(tracker, archive, all, taskid, level):
    """
    Updates the current task list, by performing all of the
    required operations (i.e. setting planned tasks, deleting failed and so on.).
    If --archive option is passed than it moves dropped tasks to the archive.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if all:
        tracker.manager.update_task_list(user=u, save=True, archive=archive)
        click.echo('Updated all tasks. '
                   '\nTo see more, type task_tracker user tasks -m --archived.')
    else:
        tracker.manager.update_task(user=u, task_id=taskid, save=True, archive=archive)
        click.echo('Updated status for task with id {0}'.format(taskid))


@task.command()
@click.option('--taskid', '-t', help='Task id or short alias.')
@click.option('--finishtime', '-f', default=None, help='Finish time for this task')
@click.option('--msg', '-m', default=None, help='Message for this task')
@click.option('--priority', '-p', type=int, default=None, help='New priority for this task')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def edit(tracker, taskid, finishtime, msg, priority, level):
    """
    Changes options of task. Available for changing options: deadline time, message or priority.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)
    t = tracker.manager.get_task_object_from_db(taskid)
    old_priority, old_msg, old_deadline = t.priority, t.msg, t.finish_time
    tracker.manager.edit_task(
        user_id=u,
        task_id=taskid,
        new_finish_time=finishtime,
        new_msg=msg,
        new_priority=priority,
        save=True
    )
    click.echo('Edited ask with id {0}. Changed options:\n'.format(taskid))
    if msg is not None:
        click.echo('MESSAGE: {0} -> {1}'.format(old_msg, msg))
    if finishtime is not None:
        click.echo('DEADLINE: {0} -> {1}'.format(old_deadline, finishtime))
    if priority is not None:
        click.echo('PRIORITY: {0} -> {1}'.format(old_priority, priority))


if __name__ == '__main__':
    from task_tracker.instances.tracker import Tracker
    main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')

    info(main_tracker, 'Task_8c0f7d56-7031-11e8-90ab-b8819811d24e', Tracker, 'INFO')

