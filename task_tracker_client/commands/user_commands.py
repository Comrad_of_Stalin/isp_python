import click
from app_tools.print_tools import print_task_info
from app_tools.validation_tools import check_authentication, auth_user
from task_tracker import tools
from task_tracker.instances.user import User


@click.group()
@click.pass_context
def user(ctx):
    """
    Makes operations with users.
    Main commands:
    add - adding new user,
    auth - authenticating user, current - show short description about current user,
    list - show all users,
    tasks - show current users tasks
    """


@user.command()
@click.option('--name', '-n', default='', help='Login of new user. '
                                         '\nThis option should have type str'
                                         'and shouldn\'t contain spaces.')
@click.option('--level', '-l', default='INFO', help='Set logging level. '
                                                    'Default = INFO. Also available: DEBUG, None')
@click.pass_obj
def add(tracker, name, level):
    """
    Adds new use into DB. If succeeds, user is being changed.
    """

    tracker.set_logging_level(level)
    u = User(name=name)
    if name == '':
        click.echo('You should define name of user!')
        return
    if tools.object_is_storing(u.id, path_to_db_dir=tracker.path_to_db):
        click.echo('User with such login already exists in db!')
        return
    tools.set_config_values({'current_user_id': u.id}, config_storage=tracker.config_storage)
    tracker.manager.save_object(u)
    click.echo('Added user with name {0} and id {1}.'.format(u.name, u.id))


@user.command()
@click.option('--name', '-n', default='', help='Login of new user. '
                                         '\nThis option should have type str'
                                         'and shouldn\'t contain spaces.')
@click.option('--level', '-l', default='INFO', help='Set logging level. '
                                                    'Default = INFO. '
                                                    'Also available: DEBUG, None')
@click.pass_obj
def auth(tracker, name, level):
    """
    Authenticates user in system, if succeeds, user is being changed.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(name)
    check_authentication(u)
    tools.set_config_values({'current_user_id': u.id}, config_storage=tracker.config_storage)
    click.echo('Changed user to {0}\n'.format(u.name))


@user.command()
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG, None')
@click.pass_obj
def list(tracker, level):
    """
    Lists all users in db. If there are many db files, it
    """

    tracker.set_logging_level(level)
    users = tools.list_instance_objects(User, path_to_db_dir=tracker.path_to_db)
    if not users:
        click.echo('No users are registered now!')
    for u in users:
        click.echo('\nUser {0}\nID: {1}\nAmount of active tasks: {2}\nAmount of archived tasks: {3}\n'
                       .format(
                            u.name,
                            u.id,
                            len(u.task_queue),
                            len(u.archived_tasks)
                        )
            )


@user.command()
@click.option('--n', default=-1, help='Show n nearest tasks.')
@click.option('--reversed', '-r', default=False, help='Show the latest tasks fist.')
@click.option('--more', '-m', is_flag=True, help='Show more info about each task')
@click.option('--archived', is_flag=True, help='Show archived tasks.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG, None')
@click.pass_obj
def tasks(tracker, n, reversed, more, archived, level):
    """
    Shows the tasks for the current user.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    if n == -1:
        n = len(u.task_queue)

    u.clean_task_list(path_to_db_dir=tracker.path_to_db, save=True)

    if not n:
        click.echo('You haven\'t set any task yet.')

    if not more:
        u.task_queue.sort()
        if reversed:
            arr = u.task_queue[-n:]
        else:
            arr = u.task_queue[:n]
        for item in arr:
            click.echo('Finish time: {0} id: {1}'.format(str(item[0]), item[1]))

        if archived:
            click.echo('ARCHIVE: ')
            for _time, _id in u.archived_tasks:
                if tools.object_is_in_archive(_id, path_to_archive_dir=tracker.path_to_db):
                    click.echo('Finish time: {0} id: {1}'.format(_time, _id))
    else:
        for _time, _id in u.task_queue:
            t = None
            if tools.object_is_storing(_id, path_to_db_dir=tracker.path_to_db):
                t = tracker.manager.get_task_object_from_db(_id)
            elif tools.object_is_in_archive(_id):
                t = tools.get_object_from_archive(_id, path_to_archive_dir=tracker.path_to_archive)
            if t is None:
                continue
            if t.parent_task_id is None:
                print_task_info(t.id, tracker.manager, more, '')
                click.echo()

        if archived:
            if not u.archived_tasks:
                click.echo('No tasks are stored in archive.')
                return
            click.echo('ARCHIVE: ')
            for _time, _id in u.archived_tasks:
                if tools.object_is_in_archive(_id, path_to_archive_dir=tracker.path_to_archive):
                    t = tools.get_object_from_archive(_id, path_to_archive_dir=tracker.path_to_archive)
                    if t is None:
                        continue
                    if t.parent_task_id is None:
                        print_task_info(_id, tracker.manager, more, '')
                        click.echo()


@user.command()
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG, None')
@click.pass_obj
def current(tracker, level):
    """
    Shows information about the current user.
    """

    tracker.set_logging_level(level)
    conf_data = tools.get_config_values(config_storage=tracker.config_storage)
    u = tracker.manager.get_user_object_from_db(conf_data['current_user_id'])
    check_authentication(u)
    click.echo('\nLogin: {0} '
               '\nLast time logged in: {1}'
               '\nAmount of active tasks: {2}'
               '\nAmount of archived tasks: {3}'
               '\nAmount of groups: {4}\n'
               .format(u.name, u.last_time_logged_in, len(u.task_queue), len(u.archived_tasks), len(u.groups)))


@user.command()
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG, None')
@click.pass_obj
def logout(tracker, level):
    """
    Deauthenticates current user.
    """

    tracker.set_logging_level(level)
    conf_data = tools.get_config_values(config_storage=tracker.config_storage)
    u = tracker.manager.get_user_object_from_db(conf_data['current_user_id'])
    check_authentication(u)
    tools.set_config_values({'current_user_id': ''}, config_storage=tracker.config_storage)
    click.echo('Logged out!')


if __name__ == '__main__':
    from task_tracker.tracker import Tracker
    main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')
    tasks(main_tracker, -1, False, True, True, 'INFO')
