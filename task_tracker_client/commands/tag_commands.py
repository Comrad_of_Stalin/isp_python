import click
from app_tools.validation_tools import (check_authentication,
                                        check_if_task_exist,
                                        auth_user)
from task_tracker import tools
from task_tracker.instances.tag import Tag


@click.group()
@click.pass_context
def tag(ctx):
    """
    Makes operations with tags. Type task_tracker tag --help to see more information.
    add - adds tag to task,
    delete - deletes tag from all tasks that have it,
    make - creates new tag,
    show - shows information about tag
    """


@tag.command()
@click.option('--tagname', '-t', help='Tag name you want to attach to your task.')
@click.option('--taskid', '-i', help='Task id or short alias.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def add(tracker, tagname, taskid, level):
    """
    Adds tag with name tagname to the target task.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)
    tracker.manager.add_tag_to_task(user=u, task=taskid, tag=tagname, save=True)
    click.echo('Added tag {0} to task with ID {1}'.format(tagname, taskid))


@tag.command()
@click.option('--tagname', '-t', help='Tag name you want to create.')
@click.option('--description', '-d', help='Description of tag.')
@click.option('--level', '-l', default='INFO', help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def make(tracker, tagname, description, level):
    """
    Makes new tag without attaching it to any task.
    """
    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if tools.object_is_storing(tagname, path_to_db_dir=tracker.path_to_db):
        click.echo('Tag with such name already exists')
        return
    t = Tag(
        tag_name=tagname,
        user_owner_id=u.id,
        description=description
    )
    tracker.manager.save_object(t)
    click.echo('Created new tag with name {0}'.format(tagname))


@tag.command()
@click.option('--tagname', '-t', help='Tag name you want to attach to your task.')
@click.option('--taskid', '-i', help='Task id or short alias.')
@click.option('--level', '-l', default='INFO', help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def remove(tracker, tagname, taskid, level):
    """
    Removes tag from the specified task.
    """
    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(taskid=taskid, usr=u, path_to_db_dir=tracker.path_to_db)
    tracker.manager.del_tag_from_task(user=u, task=taskid, tag=tagname, save=True)
    click.echo('Tag with name {0} was removed from task with id {1}'.format(tagname, taskid))


@tag.command()
@click.option('--tagname', '-t', default='', help='Tag name of task group.')
@click.option('--more', '-m', is_flag=True, help='Show more info about each task')
@click.option('--all', is_flag=True,
              help='Type this flag if you want to dop all archived tasks')
@click.option('--level', '-l', default='INFO', help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def show(tracker, tagname, more, all, level):
    """
    Shows the detailed information about tag (i.e. tasks with such tag, description and so on)
    """
    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    if all:
        for tag_name in u.tags:
            tag = tracker.manager.get_tag_object(user=u, tagname=tag_name)
            tag.clean_task_list(path_to_db_dir=tracker.path_to_db, save=True)
            click.echo('Tagname: {0} '
                       '\nDescription: {1} '
                       '\nAmount of tasks with such tag: {2}'
                .format(
                tag.name,
                tag.description,
                len(tag.tasks))
            )
            if more:
                click.echo('Task list:\n')
                for task in tag.tasks:
                    t = tracker.manager.get_task_object_from_db(task)
                    click.echo('Task id: {0} '
                               '\nTask msg: {1} '
                               '\nTask time appeared: {2} '
                               '\nTask deadline: {3}\n'.format(
                        t.id,
                        t.msg,
                        str(t.start_time),
                        str(t.finish_time))
                    )
        return

    check_authentication(u)

    if not u.has_tag(tagname):
        click.echo('User {0} doesn\'t have such tag!'.format(u.name))
        return
    tag = tracker.manager.get_tag_object(user=u, tagname=tagname)
    click.echo('Tagname: {0} '
               '\nDescription: {1} '
               '\nAmount of tasks with such tag: {2}'
               .format(
        tag.name,
        tag.description,
        len(tag.tasks))
    )
    if more:
        click.echo('Task list:\n')
        for task in tag.tasks:
            t = tracker.manager.get_task_object_from_db(task)
            click.echo('Task id: {0} '
                       '\nTask msg: {1} '
                       '\nTask time appeared: {2} '
                       '\nTask deadline: {3}\n'.format(
                                                t.id,
                                                t.msg,
                                                str(t.start_time),
                                                str(t.finish_time))
                                            )


@tag.command()
@click.option('--tagname', '-t', default='', help='Tag name of task group.')
@click.option('--level', '-l', default='INFO', help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delete(tracker, tagname, level):
    """
    Removes tag with name tagname from all of the tasks that have it.
    """
    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    if not u.has_tag(tagname):
        click.echo('User {0} doesn\'t have such tag!'.format(u.name))
        return
    tracker.manager.remove_tag(user=u, tag_name=tagname, save=True)
    click.echo('Tag {0} was deleted from all tasks to which it has been set.'.format(tagname))
