from datetime import datetime

import click
from app_tools.validation_tools import (check_authentication,
                                        auth_user)
from task_tracker import datetime_parser
from task_tracker.instances.task_planner import TaskPlanner


@click.group()
@click.pass_context
def plan(ctx):
    """
    Makes operations with plans. Type task_tracker plan --help to see more information.
    add - adds new periodic task (plan),
    delete - deletes periodic task (plan),
    show - shows the info about plan
    """


@plan.command()
@click.option('--inittime', '-i', default=datetime_parser.datetime_to_str(datetime.now()),
              help='The time you want this planner to start working in format %Y-%m-%d %H:%M:%S.')
@click.option('--appearingperiod', '-a', default='1 day',
              help='Time period by which this task is going to be set. Example: \'1 week and 1 day\'')
@click.option('--workingperiod', '-w', default='1 day',
              help='Time period in wich this task will be active. Example: \'1 day and 1 hour\'')
@click.option('--msg', '-m', default='Default message!', help='Message for task')
@click.option('--priority', '-p', default=1, help='Priority of task.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def add(tracker, inittime, appearingperiod, workingperiod, msg, priority, level):
    """
    Adds new plan, which will be able to create tasks with defined period and defined properties.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    init_time = datetime_parser.parse_str_to_datetime(inittime)

    appearingperiod = datetime_parser.parse_str_to_relativedelta(appearingperiod)

    workingperiod = datetime_parser.parse_str_to_relativedelta(workingperiod)

    planner = TaskPlanner(
        initial_moment=init_time,
        period_time=appearingperiod,
        working_period=workingperiod,
        task_msg=msg,
        task_priority=priority
    )
    tracker.manager.add_periodic_task(user=u, planner=planner, save=True)

    click.echo('Plan with id {0} was added. \nNext task is planned on {1}'
               .format(planner.id, TaskPlanner.get_next_creation_time(init_moment=planner.initial_moment,
                                                                      appearing_period=planner.period_time,
                                                                      working_period=planner.working_period,
                                                                      last_time_created=None)))


@plan.command()
@click.option('--plannerid', '-p', type=str, help='Planner id')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delete(tracker, plannerid, level):
    """
    Deletes plan with id planner_id
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    tracker.manager.delete_periodic_task(user=u, periodic_task_id=plannerid, save=True)
    click.echo('Plan with id {0} was deleted.'.format(plannerid))


@plan.command()
@click.option('--plannerid', '-p', default=None, type=str, help='Planner id')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def show(tracker, plannerid, level):
    """
    Shows the information about the specified plan (specified by plan id).
    If no plan is specified shows information about all plans.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    if not u.planner_list:
        click.echo('No plans were set for this user.')
        return

    def print_plan_info(plan):
        initial_moment, appearing_period, msg, working_period, priority, _id, last_time_created = plan
        next_creation_time = TaskPlanner.get_next_creation_time(
            initial_moment,
            appearing_period,
            working_period,
            last_time_created
        )
        click.echo('PLAN ID: {0}'
                   '\nStarted: {1}'
                   '\nCreation period: {2}'
                   '\nNext creation time: {3} ({4})'
                   '\nPRODUCED TASK PROPERTIES:'
                   '\n\tMessage: {5}'
                   '\n\tTime active: {6}\n'.format(
                                            _id,
                                            initial_moment,
                                            appearing_period,
                                            next_creation_time,
                                            datetime_parser.get_week_day_by_index(next_creation_time.weekday()),
                                            msg,
                                            working_period
                                        ))

    if plannerid is not None:
        plan = tracker.manager.get_planner_by_id(user=u, planner_id=plannerid)
        print_plan_info(plan)
    else:
        for plan in u.planner_list:
            print_plan_info(plan)


