import heapq
from datetime import datetime

import click
from app_tools.validation_tools import (check_authentication,
                                        check_if_task_exist,
                                        auth_user)
from dateutil.relativedelta import relativedelta
from task_tracker import datetime_parser
from task_tracker import tools


@click.group()
@click.pass_context
def remind(ctx):
    """
    Makes operations with reminds. Type task_tracker remind --help to see more information.
    add - adds remind to task,
    show - shows reminds for specified task, or all reminds for user
    """


@remind.command()
@click.option('--taskid', '-t', help='Task id or short alias.')
@click.option('--remindtime', '-r', multiple=True, default=None,
              help='The time when the remind will appear. Required Format %Y-%m-%d %H:%M:%S.')
@click.option('--prevperiod', '-p', multiple=True, default=None,
              help='Time period in before task deadline. Example: \'1 day and 1 hour\'')
@click.option('--msg', '-m', default='Default message!', help='Message for task')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def add(tracker, taskid, remindtime, prevperiod, msg, level):
    """
    Adds remind to the tasks with id or alias task_id.
    """
    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    taskid = check_if_task_exist(u, taskid)
    t = tracker.manager.get_task_object_from_db(taskid)
    if remindtime is not None:
        if not isinstance(remindtime, tuple):
            remindtime = (remindtime,)
        for rt in remindtime:
            period = relativedelta(seconds=(t.finish_time -
                                            datetime_parser.parse_str_to_datetime(rt)).total_seconds())

            assert t.finish_time - period < t.finish_time, 'Can\'t set remind time into the past.'

            tracker.manager.add_remind(u, t.finish_time, period, msg, t.id, save=True)
            click.echo('Added remind at time {0} for task with id {1}'.format(rt, taskid))

    if prevperiod is not None:
        if not isinstance(prevperiod, tuple):
            prevperiod = (prevperiod,)
        for period in prevperiod:
            tracker.manager.add_remind(u, t.finish_time, period, msg, t.id, save=True)
            click.echo('Added remind at time {0} for task with id {1}'
                       .format(t.finish_time - datetime_parser.parse_str_to_relativedelta(period), taskid))


@remind.command()
@click.option('--taskid', '-t', help='Task id or short alias.')
@click.option('--initmoment', '-i',
              default=datetime_parser.datetime_to_str(datetime.now()),
              help='Time moment. Reminds won\'t start appearing before this moment. '
                   'Required Format %Y-%m-%d %H:%M:%S.')
@click.option('--appearingperiod', '-p', default='1 hour',
              help='Time period by which the reminds would be appearing. Example: \'1 day and 1 hour\'')
@click.option('--msg', '-m', default='Dafault message!', help='Message for task')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def addperiodic(tracker, taskid, initmoment=datetime_parser.datetime_to_str(datetime.now()),
                appearingperiod='1 hour', msg='Test periodic remind', level='INFO'):
    """
    Adds periodic remind for task with id = task_id.
    The remind will be appearing once in a distinct period of time.
    """
    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)

    taskid = check_if_task_exist(u, taskid, path_to_db_dir=tracker.path_to_db)
    t = tracker.manager.get_task_object_from_db(taskid)

    initmoment = datetime_parser.parse_str_to_datetime(initmoment)
    time_period = datetime_parser.parse_str_to_relativedelta(appearingperiod)
    deadline = t.finish_time

    current_period = time_period
    remind_moment = deadline - current_period
    while remind_moment >= initmoment:
        remind_moment = deadline - current_period
        tracker.manager.add_remind(u, deadline, current_period, msg, t.id, save=True)
        current_period += time_period
        click.echo('Added remind at time {0} for task with id {1}'
                       .format(t.finish_time - current_period, taskid))


@remind.command()
@click.option('--taskid', '-t', default=None, help='Task id or short alias.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def delreminds(tracker, taskid, level):
    """
    Deletes reminds from the specified task or deletes them all (if task id is not specified).
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if taskid is None:
        u.remind_list.clear()
        tracker.manager.save_object(u)
        click.echo('Skipped all reminds')
    else:
        taskid = check_if_task_exist(u, taskid)
        reminds = tracker.manager.get_reminds_for_task(user=u, task_id=taskid)
        for item in reminds:
            u.remind_list.remove(item)
        heapq.heapify(u.remind_list)
        tracker.manager.save_object(u)
        click.echo('Deleted reminds for task with id {}'.format(taskid))


@remind.command()
@click.option('--taskid', '-t', default='', help='Task id or short alias.')
@click.option('--level', '-l', default='INFO',
              help='Set logging level. Default = INFO. Also available: DEBUG')
@click.pass_obj
def show(tracker, taskid, level):
    """
    Shows all reminds for specified task.
    If no task is specified, shows all the reminds that are set for current user.
    """

    tracker.set_logging_level(level)
    u = tracker.manager.get_user_object_from_db(tracker.config_storage.get_current_user_id())
    check_authentication(u)
    if taskid:
        taskid = check_if_task_exist(u, taskid)
        remind_list = tracker.manager.get_reminds_for_task(user=u, task_id=taskid)
        if not remind_list:
            click.echo('No reminds for this task yet!')
        for item in sorted(remind_list):
            click.echo(item)
    else:
        if not u.remind_list:
            click.echo('No reminds have been set yet.')
        for remind in u.remind_list:
            click.echo(remind)
