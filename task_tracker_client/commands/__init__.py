"""
This package contains the main commands that are available in user console application.
There are 8 files.
The main file is commands_parser.py, where all the commands are being parsed into parts.
The command_parser.py contains the entry point of the application and passes the commands to other modules.
The other modules contain exactly the type of commands which are described by their names.
Modules in this package:

* command_parser.py
* user_commands.py
* task_commands.py
* tag_commands.py
* planner_commands.py
* group_commands.py
* remind_commands.py
* config_commands.py

The developing process is still in progress, so I still continue adding new commands to my project.

"""