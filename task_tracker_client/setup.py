from setuptools import setup, find_packages

if __name__ == '__main__':
    setup(
        name='task_tracker_commands',
        version='0.4',
        packages=find_packages(),
        short_description="Task Tracker Commands!!",
        install_requires=['dbus-python', 'notify2', 'python-daemon', 'click', 'jsonpickle', 'python-dateutil'],
        entry_points={
            'console_scripts':
                [
                 'task_tracker = commands.command_parser:entry_point',
                 ]
        }
    )


