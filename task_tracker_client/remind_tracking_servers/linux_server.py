#!/usr/bin/python3
import datetime
import os
import subprocess
from threading import Timer
import sys

import notify2
from task_tracker import tools
import daemon


def _edit_rc_local(server_file):
    if not server_file.endswith('\n'):
        server_file += '\n'
    with open('/etc/rc.local') as f:
        lines = list(f.readlines())
        while server_file in lines:
            lines.remove(server_file)
        index = 0
        for i in range(len(lines)):
            if lines[i].strip().count('exit 0') != 0:
                index = i
        lines = lines[:index] + [server_file, ] + lines[index:]

    with open('/etc/rc.local', 'w') as f:
        f.writelines(lines)


def get_path_to_script():
    config_dir = tools.get_config_value('config_dir')
    server_file = os.path.join(config_dir, 'linux_server.py')
    return server_file


def push_server_script_to_auto_run():
    tools.initialize_main_settings(None)
    server_file = get_path_to_script()
    subprocess.call([
        'sudo',
        'cp',
        sys.modules[__name__].__file__,
        server_file, ])
    subprocess.call([
        'sudo', 'chmod', '777', server_file,
    ])
    print('SERVER FILE', server_file)
    _edit_rc_local(server_file)


def not_later_than_an_interval(a: datetime.datetime, b: datetime.datetime, interval):
    return a <= b <= a + datetime.timedelta(seconds=interval) >= b


def send_notification(_msg, _task_id):
    task_obj = tools.get_object_by_id(_task_id)
    n = notify2.Notification(_msg, 'Task DEADLINE: {0}'
                                   '\nTask MESSAGE: {1}'.format(task_obj.finish_time, task_obj.msg))
    n.show()


def check_for_reminds(interval=6):
    main_timer = Timer(interval, check_for_reminds)
    main_timer.start()
    cur_user_id = tools.get_current_user_id()
    cur_user = tools.get_object_from_db(cur_user_id)
    if cur_user is not None:
        cur_user.drop_reminds_before_moment(datetime.datetime.now()-datetime.timedelta(seconds=interval+1), save=True)
        while cur_user.get_closest_remind() is not None and not_later_than_an_interval(
                cur_user.get_closest_remind()[0],
                datetime.datetime.now(),
                interval
        ):
            rem = cur_user.del_closest_remind(save=True)
            _time, _msg, _task_id = rem
            send_notification(_msg, _task_id)


def check_if_process_is_running():
    pdump = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
    parch = subprocess.Popen(['grep', 'linux_server.py'], stdin=pdump.stdout, stdout=subprocess.PIPE)
    pdump.stdout.close()
    return parch.stdout.read().count(b'linux_server.py') <= 1


def run_server():
    tools.initialize_database_settings()
    notify2.init('task_tracker')
    check_for_reminds()


if __name__ == '__main__':
    push_server_script_to_auto_run()
    with daemon.DaemonContext():
        if not check_if_process_is_running():
            tools.initialize_main_settings('INFO')
            run_server()
