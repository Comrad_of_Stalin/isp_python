import click
from task_tracker import logger
from task_tracker import tools
from click._compat import get_text_stderr
import sys


@logger.logging_function
def print_task_info(_id,
                    manager,
                    more=False,
                    inserter=''):
    """
    Prints information about the task
    :param _id: task id
    :param more: boolean. If true, shows nice description about the task.
    :param inserter: string that makes indentations. used to indent child tasks.
    :return: None
    """
    t = manager.get_task_object_from_db(_id)
    if t is None:
        t = manager.get_task_object_from_archive(_id)
        if t is None:
            click.echo('{}Task was DELETED!\n'.format(inserter))
            return
    click.echo('{0}ID: {1}\n{2}Message: {3}'
               '\n{4}Time appeared: {5}'
               '\n{6}Finish time: {7}'
               '\n{8}Status: {9}'
               '\n{10}Priority: {11}'
               '\n{12}progress (%): {13} '
               '\n{14}Tags: {15}'.format(
                            inserter,
                            t.id,
                            inserter,
                            t.msg,
                            inserter,
                            t.start_time,
                            inserter,
                            t.finish_time,
                            inserter,
                            t.status,
                            inserter,
                            t.priority,
                            inserter,
                            t.percent_ready,
                            inserter,
                            t.tag_list if t.tag_list else 'No tags for this task yet'
                        ))
    if more and len(t.child_task_list):
        click.echo('{0}Child tasks:'.format(inserter))
        for child_data in t.child_task_list:
            print_task_info(child_data['id'], manager, more, inserter+'\t')
            click.echo()


@logger.logging_function
def handle_exception(func):
    """
    Decorates function to print information about the occured exception
    :param func: function to decorate
    :return: decorated function
    """
    def wrapped(*args, **kwargs):
        """
        Decorator (wrapping function)
        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: same return value, as the func has
        """
        try:
            ret = func(*args, **kwargs)
            return ret
        except Exception as ex_obj:
            click.echo('AN ERROR OCCUPIED: \n')
            click.echo(ex_obj)
            click.echo('\ntype task_tracker <OPTION> <COMMAND> --help to see more information')
    return wrapped


@logger.logging_function
def show_error(message, file, color):
    click.echo('Error: %s\n' % message, file=file, color=color)


@logger.logging_function
def print_help(group):
    """
    Shows group help menu.
    """
    with click.Context(group) as ctx:
        click.echo(group.get_help(ctx))


@logger.logging_function
def add_exception_help_page(commands_group):
    """
    Method that shows help menu when command has been incorrectly.
    """
    def show(self, file=None):
        if file is None:
            file = get_text_stderr()
        color = None
        show_error(self.format_message(), file, color)
        sys.argv = [sys.argv[0]]
        commands_group()

    click.exceptions.UsageError.show = show


if __name__ == '__main__':
    print_task_info('Task_8c0f7d56-7031-11e8-90ab-b8819811d24e', True, '',
                    path_to_db_dir='/home/deadcode/example/folder/db_files',
                    path_to_archive_dir='/home/deadcode/example/folder/archive_files')
