import click
from task_tracker import logger
from task_tracker import configuration
from task_tracker import database_manager
from task_tracker import tools


@logger.logging_function
def check_authentication(usr):
    """
    Checks if user is authenticated. If no, quits.
    :param usr: user object
    :return: None
    """
    if usr is None:
        click.echo('Authentication failure!')
        exit(0)


@logger.logging_function
def check_if_task_exist(usr, taskid, path_to_db_dir=None):
    """
    Checks i user has such task. If no, quits
    :param usr: user object
    :param taskid: id of task (str)
    :return: valid task id (could be retrieved from alias)
    """

    if taskid in usr.aliases:
        taskid = usr.aliases[taskid]
    if not usr.has_task(taskid, path_to_db_dir=path_to_db_dir):
        click.echo('User {0} has not task with id {1}'.format(usr.name, taskid))
        exit()
    return taskid


@logger.logging_function
def auth_user(username='', config_storage=None):
    """
    Returns current user logged in the system.
    :param username:
    :return: user which is logged now or None
    """

    if username == '':
        _id = tools.get_current_user_id(config_storage=config_storage)
    else:
        if database_manager.object_exists_in_db('User_' + username, config_storage=config_storage):
            _id = 'User_' + username
        else:
            _id = None
    if _id is None:
        return None
    u = tools.get_object_by_id(_id, config_storage=config_storage)
    return u
