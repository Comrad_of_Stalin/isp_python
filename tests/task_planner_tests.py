import datetime
import unittest
import time
from dateutil import relativedelta
from task_tracker import tools
from task_tracker import database_manager
from task_tracker.instances.task import Task
from task_tracker.instances.user import User
from task_tracker.instances.task_planner import TaskPlanner
from task_tracker.instances.tracker import Tracker


class TaskPlannerTestClass(unittest.TestCase):

    def setUp(self):
        self.main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')
        self.u = User('testuser',
                        path_to_db_dir=self.main_tracker.path_to_db,
                        path_to_archive_dir=self.main_tracker.pat_to_archive
                        )
        self.u.delete_all_periodic_tasks(save=True)
        self.u.delete_all_tasks(save=True)

    def tearDown(self):
        self.u.delete_all_periodic_tasks(save=True)
        self.u.delete_all_tasks(save=True)

    def test_creating_tasks_by_task_planner(self):
        tp = TaskPlanner(
            datetime.datetime.now(),
            relativedelta.relativedelta(seconds=2),
            'Periodic task',
            relativedelta.relativedelta(seconds=1),
            3)
        self.u.add_periodic_task(tp, save=True)

        self.u.update_task_list(save=True)

        self.assertEqual(len(self.u.task_queue), 1)
        self.assertEqual(self.u.task_queue[0][0].toordinal(), tp.initial_moment.toordinal())

        time.sleep(2)

        self.u.update_task_list(save=True)

        self.assertEqual(len(self.u.task_queue), 1)
        self.assertEqual(self.u.task_queue[0][0].toordinal(), (tp.initial_moment+tp.period_time).toordinal())

    def test_removing_task_planner(self):
        tp = TaskPlanner(
            datetime.datetime.now(),
            relativedelta.relativedelta(seconds=2),
            'Periodic task',
            relativedelta.relativedelta(seconds=1),
            3)
        self.u.add_periodic_task(tp, save=True)

        self.u.update_task_list(save=True)

        self.assertEqual(len(self.u.task_queue), 1)
        self.assertEqual(self.u.task_queue[0][0].toordinal(), tp.initial_moment.toordinal())

        self.u.delete_periodic_task(tp.id, save=True)

        time.sleep(2)

        self.u.update_task_list(save=True)

        self.assertEqual(len(self.u.task_queue), 0)
        self.assertEqual(len(self.u.planner_list), 0)

    def test_adding_task_planner(self):
        tp = TaskPlanner(
            datetime.datetime.now(),
            relativedelta.relativedelta(seconds=2),
            'Periodic task',
            relativedelta.relativedelta(seconds=1),
            3)
        self.u.add_periodic_task(tp, save=True)
        self.assertEqual(len(self.u.planner_list), 1)
        self.assertIsInstance(self.u.planner_list, list)
        self.assertEqual(self.u.planner_list[0][-2], tp.id)

    def test_creating_new_task_planner(self):
        planner_id = self.u.create_periodic_task('Message for task planner',
                               datetime.datetime.now(),
                                                 relativedelta.relativedelta(seconds=10),
                                                 relativedelta.relativedelta(seconds=20),
                                                 3,
                                                 save=True)
        self.assertEqual(len(self.u.planner_list), 1)
        self.assertIsInstance(self.u.planner_list, list)
        self.assertEqual(self.u.planner_list[0][-2], planner_id)

    def test_using_newly_created_task_planner(self):
        planner_id = self.u.create_periodic_task('Message for task planner',
                                                 datetime.datetime.now(),
                                                 relativedelta.relativedelta(seconds=2),
                                                 relativedelta.relativedelta(seconds=1),
                                                 3,
                                                 save=True)
        tp = tools.get_object_by_id(planner_id)

        self.u.update_task_list(save=True)

        self.assertEqual(len(self.u.task_queue), 1)
        self.assertEqual(self.u.task_queue[0][0].toordinal(), tp.initial_moment.toordinal())

        time.sleep(2)

        self.u.update_task_list(save=True)

        self.assertEqual(len(self.u.task_queue), 1)
        self.assertEqual(self.u.task_queue[0][0].toordinal(),
                         (tp.initial_moment + tp.period_time).toordinal())


if __name__ == '__main__':
    unittest.main()
