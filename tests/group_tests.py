import datetime
import unittest

from task_tracker.instances.group import Group
from task_tracker import tools
from task_tracker import database_manager
from task_tracker.instances.user import User
from task_tracker.instances.tracker import Tracker


class UserTestClass(unittest.TestCase):

    def setUp(self):
        self.main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')
        self.usr = User('testuser',
                        path_to_db_dir=self.main_tracker.path_to_db,
                        path_to_archive_dir=self.main_tracker.pat_to_archive
                        )

        self.r = User('testuser2',
                      path_to_db_dir=self.usr.path_to_db_dir,
                      path_to_archive_dir=self.usr.path_to_archive_dir
                      )
        self.r.save()
        self.usr.clean_group_list(save=True)
        self.usr.delete_all_tasks(save=True)
        self.usr.delete_all_archived_tasks(save=True)
        self.usr.save()

    def tearDown(self):
        try:
            self.usr.del_group('TESTGROUP')
        except:
            pass
        try:
            self.usr.del_group('ANOTHER TEST GROUP')
        except:
            pass
        self.usr.delete_all_tasks(save=True)
        self.usr.delete_all_archived_tasks(save=True)

    def test_adding_group(self):
        group_id = self.usr.add_group('TESTGROUP', (self.r.id,), (), save=True)
        group = Group('TESTGROUP', self.usr.id, (), (), save=True)
        self.assertTrue(self.usr.id in group.members)
        self.assertTrue(self.r.id in group.members)
        self.usr.update_self_state()
        self.r.update_self_state()
        self.assertTrue(group.id in self.usr.groups)
        self.assertTrue(group.id in self.r.groups)
        self.assertTrue(len(group.members) == 2)

    def test_deleting_group(self):

        group_id = self.usr.add_group('TESTGROUP', (self.r.id,), (), save=True)

        self.usr.del_group('TESTGROUP')

        self.assertFalse(database_manager.object_exists_in_db('Group_TESTGROUP'))

        self.usr.update_self_state()
        self.r.update_self_state()

        self.assertFalse('Group_TESTGROUP' in self.usr.groups)
        self.assertFalse('Group_TESTGROUP' in self.r.groups)

    def test_adding_task_to_group(self):
        group_id = self.usr.add_group('ANOTHER TEST GROUP', (self.r.id,), (), save=True)
        task_id = self.usr.create_new_task(
            finish_time='2018-6-19 10:0:0',
            msg='Some message',
            start_time=datetime.datetime.now(),
            users_owners_list=[],
            priority=1,
            save=True
        )
        self.usr.share_task_with_group(task_id, 'ANOTHER TEST GROUP', save=True)
        group = tools.get_object_by_id(group_id, path_to_db_dir=self.usr.path_to_db_dir)
        self.assertTrue(database_manager.object_exists_in_db(group_id))
        self.assertIsNotNone(group)
        self.assertTrue(len(group.task_list)==1)
        self.assertTrue(task_id in group.task_list)
        self.r.update_self_state()
        self.assertTrue(self.r.has_task(task_id))
        self.usr.del_group('ANOTHER TEST GROUP')

    def test_finishing_task_in_group(self):
        group_id = self.usr.add_group('ANOTHER TEST GROUP', (self.r.id,), (), save=True)

        task_id = self.usr.create_new_task('2018-6-19 10:0:0', 'Some message', datetime.datetime.now(), [], 1, save=True)
        self.usr.share_task_with_group(task_id, 'ANOTHER TEST GROUP', save=True)

        self.r.update_self_state()

        self.r.finish_task(task_id, save=True, archive=True)

        t = tools.get_object_from_archive(task_id, path_to_archive_dir=self.usr.path_to_archive_dir)
        self.usr.update_self_state()

        self.assertTrue(t.user_successor == self.r.name)
        self.assertTrue((t.finish_time, t.id) in self.usr.archived_tasks)

        group = tools.get_object_by_id(group_id, path_to_db_dir=self.usr.path_to_db_dir)
        group.clean_group_tasks(save=True)

        self.assertFalse(task_id in group.task_list)
        self.assertTrue(tools.object_is_in_archive(task_id))

    def test_quit_from_group(self):
        group_id = self.usr.add_group('ANOTHER TEST GROUP', (self.r.id,), (), save=True)

        self.r.update_self_state()

        self.assertTrue(group_id in self.r.groups)

        self.r.quit_from_group('ANOTHER TEST GROUP', save=True)

        self.assertFalse(group_id in self.r.groups)

        group = self.usr.get_group('ANOTHER TEST GROUP')

        self.assertFalse(self.r.id in group.members)

    def test_failing_quit_admin_from_group(self):
        group_id = self.usr.add_group('ANOTHER TEST GROUP', (self.r.id,), (), save=True)

        self.r.update_self_state()

        self.assertTrue(group_id in self.r.groups)

        with self.assertRaises(AssertionError) as exc_obj:
            self.usr.quit_from_group('ANOTHER TEST GROUP', save=True)
            self.assertTrue(exc_obj.msg == 'Can not kick admin out of group')

        group = self.usr.get_group('ANOTHER TEST GROUP')
        self.assertTrue(self.usr.id in group.members)

    def test_kicking_user_from_group(self):
        group_id = self.usr.add_group('ANOTHER TEST GROUP', (self.r.id,), (), save=True)

        self.r.update_self_state()

        self.assertTrue(group_id in self.r.groups)

        self.usr.kick_user_from_group(self.r.id, 'ANOTHER TEST GROUP', save=True)

        group = self.usr.get_group('ANOTHER TEST GROUP')

        self.assertTrue(self.usr.id in group.members)
        self.assertFalse(self.r.id in group.members)

        self.r.update_self_state()

        self.assertFalse(group_id in self.r.groups)

    def test_failing_kicking_admin_from_group(self):
        group_id = self.usr.add_group('ANOTHER TEST GROUP', (self.r.id,), (), save=True)

        self.r.update_self_state()

        self.assertTrue(group_id in self.r.groups)

        with self.assertRaises(AssertionError) as exc_obj:
            self.r.kick_user_from_group(self.usr.id, 'ANOTHER TEST GROUP', save=True)
            self.assertTrue(exc_obj.msg == 'Can not kick admin out of group')

        group = self.usr.get_group('ANOTHER TEST GROUP')

        self.assertTrue(self.usr.id in group.members)
        self.assertTrue(self.r.id in group.members)

        self.r.update_self_state()

        self.assertTrue(group_id in self.r.groups)


if __name__ == '__main__':
    unittest.main()
