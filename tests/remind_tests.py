import unittest
import datetime
from task_tracker.instances.user import User
from task_tracker.instances.task import Task
from task_tracker.tools import init_db
from task_tracker.instances.tracker import Tracker


class RemindTestClass(unittest.TestCase):

    def setUp(self):
        self.main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')
        self.u = User('testuser',
                        path_to_db_dir=self.main_tracker.path_to_db,
                        path_to_archive_dir=self.main_tracker.pat_to_archive
                        )
        self.u.delete_all_tasks(save=True)
        self.u.remove_all_tags(save=True)
        while self.u.remind_list:
            self.u.del_closest_remind(save=True)

    def tearDown(self):
        self.u.del_closest_remind(save=True)

    def test_adding_remind_to_task(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_remind(t.finish_time, datetime.timedelta(seconds=30), 'REMIND', t.id, save=True)
        remind_tuple = self.u.get_closest_remind()
        self.assertEqual(len(remind_tuple), 3)
        self.assertTrue((remind_tuple[0] - (t.finish_time-datetime.timedelta(seconds=30))).total_seconds() < 1)
        self.assertEqual(remind_tuple[1], 'REMIND')
        self.assertEqual(remind_tuple[-1], t.id)

    def test_deleting_closest_remind(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.assertIsNone(self.u.get_closest_remind())

        self.u.add_remind(t.finish_time, datetime.timedelta(seconds=30), 'REMIND', t.id, save=True)
        self.u.add_remind(t.finish_time, datetime.timedelta(seconds=50), 'REMIND', t.id, save=True)
        self.u.add_remind(t.finish_time, datetime.timedelta(seconds=20), 'REMIND', t.id, save=True)

        self.u.update_self_state()

        remind_tuple = self.u.del_closest_remind()

        self.assertTrue((remind_tuple[0] -
                               (datetime.datetime.now() +
                               datetime.timedelta(minutes=1) -
                               datetime.timedelta(seconds=50))).total_seconds() < 1)

    def test_dropping_reminds_till_moment(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.assertIsNone(self.u.get_closest_remind())

        self.u.add_remind(t.finish_time, datetime.timedelta(seconds=30), 'REMIND', t.id, save=True)
        self.u.add_remind(t.finish_time, datetime.timedelta(seconds=50), 'REMIND', t.id, save=True)
        self.u.add_remind(t.finish_time, datetime.timedelta(seconds=20), 'REMIND', t.id, save=True)

        self.u.update_self_state()

        self.u.drop_reminds_before_moment(datetime.datetime.now()+datetime.timedelta(seconds=35), save=True)

        remind_tuple = self.u.get_closest_remind()

        self.assertTrue((remind_tuple[0] -
                         (datetime.datetime.now() +
                          datetime.timedelta(minutes=1) -
                          datetime.timedelta(seconds=20))).total_seconds() < 1)


if __name__ == '__main__':
    unittest.main()
