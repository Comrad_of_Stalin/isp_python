import datetime
import unittest

from task_tracker.instances.group import Group
from task_tracker import tools
from task_tracker import database_manager
from task_tracker.instances.user import User
from task_tracker.instances.tracker import Tracker


class UserTestClass(unittest.TestCase):

    def setUp(self):
        self.main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')
        self.usr = User('testuser',
                        path_to_db_dir=self.main_tracker.path_to_db,
                        path_to_archive_dir=self.main_tracker.pat_to_archive
                        )
        self.usr.delete_all_tasks(save=True)
        self.usr.save()

    def tearDown(self):
        self.usr.delete()

    def test_user_creation(self):
        self.assertTrue(tools.object_is_storing(self.usr.id, path_to_db_dir=self.usr.path_to_db_dir))
        cp_u = database_manager.get_object_from_db_(self.usr.id, path_to_db_dir=self.usr.path_to_db_dir)
        self.assertEqual(self.usr.id, cp_u.id)

    def test_user_deletion(self):
        self.usr.delete()
        self.assertFalse(database_manager.object_exists_in_db(self.usr.id, path_to_db_dir=self.usr.path_to_db_dir))


if __name__ == '__main__':
    unittest.main()
