import datetime
import unittest

from task_tracker import tools
from task_tracker.instances.tag import Tag
from task_tracker.instances.task import Task
from task_tracker.instances.tracker import Tracker
from task_tracker.instances.user import User


class TagTestClass(unittest.TestCase):

    def setUp(self):
        self.main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')
        self.u = User('testuser',
                        path_to_db_dir=self.main_tracker.path_to_db,
                        path_to_archive_dir=self.main_tracker.pat_to_archive
                        )
        self.u.delete_all_tasks(save=True)
        self.u.remove_all_tags(save=True)

    def tearDown(self):
        self.u.delete_all_tasks(save=True)
        self.u.delete_all_archived_tasks(save=True)
        self.u.remove_all_tags(save=True)

    def test_adding_tag_to_task(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_tag_to_task(t.id, 'Test tag', save=True)
        tg = self.u.get_tag_object('Test tag')
        self.assertIsInstance(tg, Tag)
        self.assertEqual(tg.user_owner, self.u.name)
        self.assertEqual(len(tg.tasks), 1)
        task_to_check = self.u.get_task_object_from_db(t.id)
        self.assertTrue(tg.name in task_to_check.tag_list)

    def test_adding_tag_to_many_tasks(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_task(q, save=True)
        self.u.add_tag_to_task(t.id, 'Test tag', save=True)
        self.u.add_tag_to_task(q.id, 'Test tag', save=True)
        tg = Tag('Test tag',
                 self.u.id,
                 path_to_db_dir=self.u.path_to_db_dir,
                 path_to_archive_dir=self.u.path_to_archive_dir)
        self.assertEqual(len(tg.tasks), 2)
        self.assertTrue(t.id in tg.tasks)
        self.assertTrue(q.id in tg.tasks)

    def test_removing_tag_from_task(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_task(q, save=True)
        self.u.add_tag_to_task(t.id, 'Test tag', save=True)
        self.u.add_tag_to_task(q.id, 'Test tag', save=True)

        tg = Tag('Test tag',
                 self.u.id,
                 path_to_db_dir=self.u.path_to_db_dir,
                 path_to_archive_dir=self.u.path_to_archive_dir)
        self.assertEqual(len(tg.tasks), 2)
        self.assertTrue(t.id in tg.tasks)
        self.assertTrue(q.id in tg.tasks)

        self.u.del_tag_from_task(t.id, tg.name, save=True)

        tg = self.u.get_tag_object('Test tag')
        self.assertEqual(len(tg.tasks), 1)
        self.assertTrue(t.id not in tg.tasks)
        self.assertTrue(q.id in tg.tasks)

    def test_removing_tag_from_user(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_task(q, save=True)
        self.u.add_tag_to_task(t.id, 'Test tag', save=True)
        self.u.add_tag_to_task(q.id, 'Test tag', save=True)

        tg = Tag('Test tag',
                 self.u.id,
                 path_to_db_dir=self.u.path_to_db_dir,
                 path_to_archive_dir=self.u.path_to_archive_dir)
        self.assertEqual(len(tg.tasks), 2)
        self.assertTrue(t.id in tg.tasks)
        self.assertTrue(q.id in tg.tasks)

        self.u.remove_tag(tg.name, save=True)

        self.assertFalse(tools.object_is_storing(tg.id, path_to_db_dir=self.u.path_to_db_dir))
        self.assertFalse(tg.name in t.tag_list)
        self.assertFalse(tg.name in q.tag_list)

    def test_edit_tag(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_tag_to_task(t.id, 'Test tag', save=True)
        tg = self.u.get_tag_object('Test tag')
        old_description = tg.description
        self.u.edit_tag_description(tg.name, 'New description', save=True)
        tg = tools.get_object_by_id(tg.id, path_to_db_dir=self.u.path_to_db_dir)
        self.assertNotEqual(old_description, tg.description)
        self.assertEqual(tg.description, 'New description')


if __name__ == '__main__':
    unittest.main()
