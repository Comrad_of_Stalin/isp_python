import datetime
import unittest

from task_tracker.exceptions import WrongDataException
from task_tracker import tools
from task_tracker import database_manager
from task_tracker.instances.task import Task
from task_tracker.instances.user import User
from task_tracker.instances.tracker import Tracker


class TaskTestClass(unittest.TestCase):

    def setUp(self):
        self.main_tracker = Tracker('/home/deadcode/example/folder', 'config_file.ini')
        self.u = User('testuser',
                        path_to_db_dir=self.main_tracker.path_to_db,
                        path_to_archive_dir=self.main_tracker.pat_to_archive
                        )
        self.u.delete_all_tasks(save=True)
        self.u.delete_all_archived_tasks(save=True)
        self.u.clean_archived_task_list(save=True)

    def tearDown(self):
        self.u.delete_all_tasks(save=True)
        self.u.delete_all_archived_tasks(save=True)
        self.u.clean_archived_task_list(save=True)

    def test_adding_task(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.assertEqual(len(self.u.task_queue), 1)
        self.assertEqual(self.u.task_queue[0], (t.finish_time, t.id))
        self.assertTrue(t.user_admin_id == self.u.id)

    def test_deleting_task(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        task_id = self.u.task_queue[0][1]
        prev_len = len(self.u.task_queue)
        self.u.del_task(task_id, save=True)
        after_len = len(self.u.task_queue)
        self.assertEqual(prev_len, after_len+1)
        with self.assertRaises(Exception):
            self.u.has_task(task_id)

    def test_adding_subtask_to_task(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        task_id = self.u.task_queue[0][1]
        prev_complexity = t.total_complexity
        self.u.add_subtask_to_task(t, q, save=True)
        self.assertEqual(len(t.child_task_list), 1)
        self.assertEqual(t.total_complexity, prev_complexity + q.total_complexity)
        self.assertTrue(len(self.u.task_queue) == 2)
        self.assertTrue(database_manager.object_exists_in_db(t.id))
        self.assertTrue(database_manager.object_exists_in_db(q.id))

    def test_deleting_subtask_from_task(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        task_id = self.u.task_queue[0][1]
        prev_complexity = t.total_complexity
        self.u.add_subtask_to_task(parent_task=t, son_task=q, save=True)
        self.u.del_task(q.id, save=True)
        t = self.u.get_task_object_from_db(t.id)
        t.clean_child_list()
        self.assertEqual(len(t.child_task_list), 0)
        self.assertEqual(t.total_complexity, prev_complexity + q.total_complexity)
        self.assertTrue(len(self.u.task_queue) == 1)
        self.assertTrue(database_manager.object_exists_in_db(t.id, path_to_db_dir=self.u.path_to_db_dir))
        self.assertFalse(database_manager.object_exists_in_db(q.id, path_to_db_dir=self.u.path_to_db_dir))

    def test_failing_adding_unexisting_subtask(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir,
            save=True
        )
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t.id, save=True)
        with self.assertRaises(Exception):
            self.u.add_subtask_to_task(t.id, q.id, save=True)
        with self.assertRaises(Exception):
            self.u.has_task(q.id)
        t = self.u.get_task_object_from_db(t.id)
        self.assertFalse((q.finish_time, q.id) in t.child_task_list)

    def test_creating_new_task(self):
        task_id = self.u.create_new_task(datetime.datetime.now()+datetime.timedelta(minutes=1),
                          'NoMSG',
                          datetime.datetime.now(),
                          [],
                          2,
                          save=True)
        t = self.u.get_task_object_from_db(task_id)
        self.assertEqual(len(self.u.task_queue), 1)
        self.assertIsNotNone(t)
        self.assertEqual(self.u.task_queue[0], (t.finish_time, t.id))
        self.assertTrue(database_manager.object_exists_in_db(t.id, path_to_db_dir=self.u.path_to_db_dir))

    def test_failing_creating_new_task(self):
        task_id = None
        with self.assertRaises(AssertionError):
            task_id = self.u.create_new_task(datetime.datetime.now() - datetime.timedelta(minutes=1),
                                        'NoMSG',
                                        datetime.datetime.now(),
                                        [],
                                        2,
                                        save=True)
        self.assertIsNone(task_id)
        self.assertEqual(len(self.u.task_queue), 0)

    def test_finishing_task_without_archiving(self):
        task_id = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        self.u.finish_task(task_id, save=True)
        self.assertEqual(len(self.u.task_queue), 0)
        self.assertFalse(database_manager.object_exists_in_db(task_id, path_to_db_dir=self.u.path_to_db_dir))
        self.assertFalse(tools.object_is_in_archive(task_id, path_to_archive_dir=self.u.path_to_archive_dir))

    def test_finishing_task_with_archiving(self):
        task_id = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        self.u.finish_task(task_id, save=True, archive=True)
        self.assertEqual(len(self.u.task_queue), 0)
        self.assertFalse(database_manager.object_exists_in_db(task_id, path_to_db_dir=self.u.path_to_db_dir))
        self.assertTrue(tools.object_is_in_archive(task_id, path_to_archive_dir=self.u.path_to_archive_dir))

    def test_deleting_achived_tasks(self):
        task_id = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        self.u.archive_task(task_id)
        pre_len = len(self.u.archived_tasks)
        self.u.del_archived_task(task_id)
        after_len = len(self.u.archived_tasks)
        self.assertFalse(tools.object_is_in_archive(task_id, path_to_archive_dir=self.u.path_to_archive_dir))
        self.assertFalse(database_manager.object_exists_in_db(task_id, path_to_db_dir=self.u.path_to_db_dir))
        self.assertEqual(pre_len, after_len+1)

    def test_finishing_subtask_with_recalculating(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            1,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_subtask_to_task(t, q, save=True)
        prev_readiness = t.percent_ready
        self.u.finish_task(q.id, save=True, archive=True)
        t = self.u.get_task_object_from_db(t.id)
        q = tools.get_object_from_archive(q.id, path_to_archive_dir=self.u.path_to_archive_dir)
        after_readiness = t.percent_ready
        self.assertEqual(q.status, Task.COMPLETED)
        self.assertNotEqual(prev_readiness, after_readiness)
        self.assertAlmostEqual(after_readiness, 100 / 3)

    def test_archiving_subtask_with_recalculating(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        prev_complexity = t.total_complexity
        q = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            1,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir
        )
        self.u.add_task(t, save=True)
        self.u.add_task(q, save=True)
        self.u.add_subtask_to_task(t, q, save=True)
        after_complexity = t.total_complexity
        prev_readiness = t.percent_ready
        self.u.archive_task(q.id)
        t_task_to_check = self.u.get_task_object_from_db(t.id)
        after_readiness = t_task_to_check.percent_ready
        q_task_to_check = tools.get_object_from_archive(q.id, path_to_archive_dir=self.u.path_to_archive_dir)
        self.assertEqual(q_task_to_check.status, Task.ARCHIVED)
        self.assertEqual(prev_readiness, after_readiness)
        self.assertEqual(after_readiness, 0)

    def test_creating_new_subtask(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir,
            save=True
        )
        self.u.add_task(t.id, save=True)
        task_id = self.u.add_new_subtask_to_task(t.id,
                                  datetime.datetime.now()+datetime.timedelta(minutes=1),
                                  'NoMSG',
                                  datetime.datetime.now(), 3, save=True)
        t = self.u.get_task_object_from_db(t.id)
        self.assertEqual(len(self.u.task_queue), 2)
        self.assertTrue((t.finish_time, t.id) in self.u.task_queue)
        self.assertTrue(self.u.has_task(task_id))
        self.assertEqual(len(t.child_task_list), 1)
        self.assertEqual(t.total_complexity, 5)

    def test_failing_subtask_with_archiving(self):
        t = Task(
            datetime.datetime.now() + datetime.timedelta(minutes=1),
            'NoMSG',
            [],
            self.u.id,
            2,
            path_to_db_dir=self.u.path_to_db_dir,
            path_to_archive_dir=self.u.path_to_archive_dir,
            save=True
        )
        self.u.add_task(t.id, save=True)
        task_id = self.u.add_new_subtask_to_task(t.id,
                                            datetime.datetime.now() + datetime.timedelta(minutes=1),
                                            'NoMSG',
                                            datetime.datetime.now(), 3, save=True)
        self.u.fail_task(task_id, archive=True, save=True)
        self.assertFalse(database_manager.object_exists_in_db(t.id))
        self.assertTrue(tools.object_is_in_archive(t.id))
        self.assertTrue(tools.object_is_in_archive(task_id))
        t = tools.get_object_from_archive(t.id, path_to_archive_dir=self.u.path_to_archive_dir)
        self.assertEqual(t.status, Task.FAILED)
        q = tools.get_object_from_archive(task_id, path_to_archive_dir=self.u.path_to_archive_dir)
        self.assertEqual(q.status, Task.FAILED)

    def test_adding_alias_to_task(self):
        task_id = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        self.u.add_alias('foo', task_id, save=True)
        self.assertTrue('foo' in self.u.aliases)
        self.assertEqual(self.u.aliases['foo'], task_id)

    def test_deleting_alias_from_task(self):
        task_id = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        self.u.add_alias('foo', task_id, save=True)
        self.assertTrue('foo' in self.u.aliases)
        self.assertEqual(self.u.aliases['foo'], task_id)
        self.u.del_alias('foo')
        self.assertFalse('foo' in self.u.aliases)
        self.assertEqual(len(self.u.aliases), 0)

    def test_failing_adding_alias(self):
        task_id = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        wrong_task_id = 'Task_UNEXISTING'
        with self.assertRaises(AssertionError):
            self.u.add_alias('foo', wrong_task_id)
        self.assertFalse('foo' in self.u.aliases)
        self.assertEqual(len(self.u.aliases), 0)

    def test_deleting_all_tasks(self):
        self.assertTrue(len(self.u.task_queue) == 0)

    def test_getting_tasks_in_period(self):
        task_id_1 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        task_id_2 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=3),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        task_id_3 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=4),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        task_id_4 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=6),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        task_list = self.u.get_tasks_in_time_range(datetime.datetime.now() + datetime.timedelta(minutes=2),
                                              datetime.datetime.now() + datetime.timedelta(minutes=4))
        self.assertEqual(len(task_list), 2)
        task_list.sort(key=lambda x:x.finish_time)
        self.assertEqual(task_list[0].id, task_id_2)
        self.assertEqual(task_list[1].id, task_id_3)

    def test_clearing_archived_tasks(self):
        task_id_1 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                      'NoMSG',
                                      datetime.datetime.now(),
                                      [],
                                      2,
                                      save=True)
        task_id_2 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=3),
                                      'NoMSG',
                                      datetime.datetime.now(),
                                      [],
                                      2,
                                      save=True)
        task_id_3 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=4),
                                      'NoMSG',
                                      datetime.datetime.now(),
                                      [],
                                      2,
                                      save=True)
        task_id_4 = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=6),
                                      'NoMSG',
                                      datetime.datetime.now(),
                                      [],
                                      2,
                                      save=True)
        for item in self.u.task_queue[:]:
            self.u.archive_task(item[1])
        self.assertEqual(len(self.u.archived_tasks), 4)
        self.u.delete_all_archived_tasks(save=True)
        self.assertEqual(len(self.u.archived_tasks), 0)

    def test_editing_task(self):
        task_id = self.u.create_new_task(datetime.datetime.now() + datetime.timedelta(minutes=1),
                                    'NoMSG',
                                    datetime.datetime.now(),
                                    [],
                                    2,
                                    save=True)
        t = self.u.get_task_object_from_db(task_id)

        self.assertTrue(t.msg == 'NoMSG')
        self.assertEqual(t.priority, 2)

        self.u.edit_task(
            task_id=task_id,
            new_msg='Edited MSG',
            new_priority=3,
            save=True
        )

        t = self.u.get_task_object_from_db(task_id)

        self.assertTrue(t.msg == 'Edited MSG')
        self.assertEqual(t.priority, 3)


if __name__ == '__main__':
    unittest.main()
