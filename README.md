# ISP_Python

## Installing

The project consists of two main parts: the library and the client side application.

First you should install the library. Go into directory isp_python/task_tracker_lib.
To install the library type the following commands from the terminal:

    sudo python3 setup.py install

After that go into directory isp_python/task_tracker_client. Here lies the client side of the project.
To install it use the following commands in from the terminal:
    
    sudo python3 setup.py install

If you want to run a remind tracker, got into directory isp_python/task_tracker_client/remind_tracking_servers.
Then type the following commands in he terminal:

    sudo python3 linux_server.py

Accepted setup.py commands:

* install: installs the library to your computer.

## Configuration

You can change configuration using '~/.task_tracker_conf/config.ini' file.
You should only use the section CURRENT_OPTIONS. You can configure path to your db or archive files,
by configuring db_dir and archive_dir options.
Settings example:

    [CURRENT_OPTIONS]
    
    host = localhost
    
    current_user_id = User_a
    
    config_dir = ~/.task_tracker_conf
    
    db_dir = ~/.task_tracker_conf/db_files
    
    archive_dir = ~/.task_tracker_conf/archive_files
    
    port = 50007
    
    config_file = ~/.task_tracker_conf/config.ini
    
    log_dir = ~/.task_tracker_conf/log
    
    main_db_file = ~/.task_tracker_conf/main_db.json

## Usage

The console app can be started using the task_tracker command in terminal.
After that type --help to see the main options.

The list of main options;

* user
* task
* group
* tag
* remind
* plan
* config


## Examples of usage:

    task_tracker user current    # that will output the information about current user
    
    task_tracker task add --msg '3-rd lab' -f '2018-6-12 20:0:0' -a 3dlab   # that command will create a task
    
    task_tracker remind addperiodic -p '1 hour' -t 3dlab   # that will create a periodic reminds for task created before  
    
    task_tracker    # that will output the help page

### Have pleasure, using this app ))